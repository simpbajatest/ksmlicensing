﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Lambda.Core;
using Amazon.Runtime;
using DatabaseLayer;
using Lambda.Entities;
using Lambda.Logic;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            LicenseResponse.Rawlicense licenseResponse = new LicenseResponse.Rawlicense();
            licenseResponse = FulfillmentLogic.GetAPI("1137996");
            Console.ReadLine();
            //byte[] data = Convert.FromBase64String(System.IO.File.ReadAllText(@"E:/file1.bin"));
            //string decodedString = Encoding.GetEncoding("iso-8859-1").GetString(data);

            //List<string> lines = new List<string>();
            //using (System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(data))
            //{
            //    lines = fileUploadRequestParser(streamBitmap);
            //}

            //string displayString = string.Empty;

            //if (lines.Count > 0)
            //{
            //    string fileContent = string.Empty;
            //    fileContent = lines[0];

            //    for (int i = 1; i < lines.Count; i++)
            //    {
            //        fileContent = fileContent + Environment.NewLine + lines[i];
            //    }
            //    displayString = FulfillmentLogic.DecryptFNE(GenerateStreamFromString(fileContent), null);
            //    Console.WriteLine(displayString);
            //    Console.ReadLine();
            //}
            //string lam = FulfillmentLogic.EncryptString(System.IO.File.ReadAllText(@"E:/file1.bin"));
            //Console.WriteLine(lam);
            //Console.ReadLine();
            // string requestBody = System.IO.File.ReadAllText(@"E:/syncSubscribeNetReq.bin");

            // Dictionary<string, string> parameters = new Dictionary<string, string>();

            //// parameters.Add("FileType", "FNE");

            // HttpClient client = new HttpClient();

            // MultipartFormDataContent form = new MultipartFormDataContent();
            // HttpContent content = new StringContent("fileToUpload");
            // HttpContent dictionaryItems = new FormUrlEncodedContent(parameters);         

            // byte[] data = Convert.FromBase64String(requestBody);
            // content = new StreamContent(new System.IO.MemoryStream(data));
            // content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            // {
            //     FileName = "subscribenet.bin"
            // };

            // form.Add(content);

            // HttpResponseMessage response = null;

            // try
            // {
            //     response = client.PostAsync("https://ksmlicensingtest.keysight.com/control/ixia/deviceservices", form).Result;
            // }
            // catch (Exception ex)
            // {
            //     throw ex;
            // }

            // var k = response.Content.ReadAsStringAsync().Result;

            // Console.ReadLine();
            //return Convert.ToString(k);

            ConfigSettings.Connection = "Data Source=(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = ksmdb1t.cos.is.keysight.com)(PORT = 1521)))(CONNECT_DATA = (SERVICE_NAME = GSDCTEST))); Pooling=True; Persist Security Info = True; User Id = license; Password = re10ister;";
            //LambdaResponse lam = FulfillmentLogic.readActivationCode("1371-6965-C5AD-E137", "", true, null, "ecommerce");
            //Console.ReadLine();
            //string message = "eyJtZXNzYWdlU2lnbmF0dXJlIjoiZTcyNFhDYloyYkJveE1yMzFuajNMTUl6Yk5PVTNtb0x6MWtFTnArSkxzRXoxZTlhd3J5c3h0Y1licWhVQnA3ZzRkL2NaWEJvUU4vb2xZZEEvMkJCZk1BY3NxT21haDc1R2RISkVhdWtFcTZXYjBNRU8rVXBTNHdCdStvQlZKWGJ0K21OWFVGaTRhTk5kVlFWaytjeG1SRjRITHBuM2I0L2Mrdk9WMitiOEVJL21ScUhQcDJhTVZWckYvTUExV1R1KzlrY2lNcW4xZ1RaRVU1N2liazg1MnY3RjRrZllua0llWTNhQmJTRDlpTHU1OG4wbGkvSmpGVkxNSy9uZXpkU1RKZ2szUWMrVStEaFF6OVlybjRrM25UNGlvT0dvZExqcVlaSUtzaGt0MHZ2SFIwVWNqSkc3VFZEUHlTWHU0TXMxTmhuNTBGNDRPZjVMWGVuSkxmeXVrbjNRdkt5NlA2QTVwZzVCNGVPK2NMdXVaU3dRTVhKU0IwSzdhZHdkRlNsZDVxb3lla2U1TngrNW1OV1dUelNCY2E1WUdDUVJIclZvZ1pRd1pXUWpWaWxFK3VhVTN4TGpsM2lFaVBRZGdqYnpQVSszTGE4eVF5Q1g5RVQvNklUeTAzUEpCallNNDl5TXR2ejZTUlhkRDFGTFVVYTErM1RSRUlTeFphdkgrY05qM0NRUGYvR0l2T3JvZkF3Y1RvSWlqZWZMME8zUUplUm13STlrMEVQQlI4cERPTWF3aVJ4cXBwQ3JKOU9YWnNicnJxSFdrUmorM2g4Ujl3QXkyYzFJbEZPMmFHc2ZCNFhlMnVzSGhOSVp2OGxvL1FncjI1d1I4TFRPcjhCMll5cWVIdFZ2RHVXVndsWWdWRTc3Vmd1OVVYeXZ0UElrUm1Da0NoS0JqTS80aWM9IiwibWVzc2FnZSI6IjQzOFJGWnkyZisvd0toMDRLeUNtQW5SclpkeEg0Q29LeHlhSzhxOVoyemRIdG5vVjV2NDVYSjUvSXJLcUtaL25yVDJrYnpNWUJrK0E2UDZTR3J4MFBRPT0iLCJpdiI6ImVhVHBpajdQVHhuUUpaYUpZN1pvc3U2N3ZsN0FkUzZxeUo0aUQwVkJnY0dPT3ZRZ29rd3Y1QncxbjZSOEluQ3JSdFZFWERVVFdNM0dRbGJTd053OHljTHFnLzVhY0VMWnl6cmEzNGZCWnRzNHNkb2VzU2FVUXFxUDFCQ0JtKzBIdXRqZlJVdFdFUkZrVVV5MzEvaU9lVHkvRFRFNTJsRGFmU3RhdWlSbzFIWVdsOHdXNktjMnlEMjhENVpVMGQxeklOSkc0bWNObGxXZEpIbnZ4K1BPeUJSTHlaUTdpYmpMb0NUUmZFZkpBUmlLeGNFZUZnY2RyTEhZODE5ZjQvNWt4ZWJXZk95RFNrWWw2VUJUdUR0MncyQ0xCV2h5UTBXOVpmQU5QUzhMNUd4bWs2UFpRbExnSXV3RDV1Ym04THlob3J6K1FRZzQrSDVGa3R3b3VlNVRnR1g1WVpuVldoMDNiT2JlMXlocXRMN1g1NHhlMVozMjFrUUtmWEhISW9GSnRqY0pTVThaZDZZOE0ybjkreTVOa243anVVeFBnUGVQdzBDWWxsVjlGanBmTGlLRE1LQkpXaUpNMkJKV2RsU1pTaWM5VDVobHFBYVU4TWhIL2RUeWZkQ003d1NRMWNDMSsrd3BpQkRYd1grV2t4UWNUWVBWdWlaNll5M1g4MERLTElGYllYNUUvTENqTnFTMW5MczJxK3ZhSm9nbUtmMUwzWFpvUzJFamNsU01sam1HaW9INzZEMmR5NWcrZVc2eXpuenViUEUrNW9SSGJMM1Eva2ZrcXd5L1k0bWFHYzMreEdTb2FSR1pjT3Z3SndzVlIyV3BOWUNKZTFlU3ovWVo4blJlelF5UW9pV1NVSjZzQU5mUWF6RmZYOXdTeDUySk9Namh6MHI3MjRvPSIsImtleSI6Ik9tN1VaOW1CblB6U0x0bFJvb3UrakIxRUdmVmlORnY0RGE2dzU5OUJqaUw5U09yQ01IOU5XTUlaWnViZVpYM3Z3SmZDWXNSb0twYTJlZ2lWcXptbmVLSTEwMEpqUmxvT2RJMmpmVEZTbVpjMURPK3FtSHlGRDZWTE9YQUlyOENqbDY4cmI2dmVlU1huNk1mREptb3pPaFNxYmF3dlFXa1h1TmxrdXVEQTMxaTUyc3pEU1Y3bnRlZ2hKcndSZVJwTHFrZGhoc3daMlEwT1oxRi9uS1RyL29jV2ljczJwOXpNRGJFOC9QRGNNa3pUazhiWDhFejlOTWRUSHN4dFVwOVo1WnA5WUQ5SFNNd0xONFZLTDczWnFZQm5qMVI2RndXRS9JRjFYbkJKTUhhVFNsMTZJZ3FDcWlQbnhvcXJjTjNRMHlBZUVLUzB3OEhFWkZ2RUliSTVadUpCYUJvc2NoWW9JV3duQ1lYd1BjWUtJRGN2UUZ0bHVKUjNoN0t0ZDdSTFkzV2VjSWNHNTBIS3lyR3Q4S21tZitJbldGSHBaUEp6THQ3VTFpZk9qUWd0QnY5aFgxOFlwYnI3cFhPUkdQVGxESy9lSmY0QWFUcWxuVHU5Y21kMWpiQXFLQWZhVGZ0NHpyQ2Q1bW5ic21FRW5RTi9lU1pKbnZiTHhSSmZqS0VVNmlPMmVkK0JabGlYVEZiVmR3dGhBVGQ0bWpIL3N1MGYvbGVrNVA3UkFXVjBUSWk0NHdDWEVIb2NOMHlQcHRuU3pmbGxwTFVaL0U5bGl1T0dzbDh6RFN2Q3U3OXJwbTAwVUo5MnNwVjlUcnZjc1JFcDVrOGg4dTVKeXFsU3FqWGNLT2orL1RCcE52SGc5SGRQam5uckkrb3czbVhwaHU5UnBVNXNia3lHbnU0PSJ9";
            //DecryptResponse.StringContent instance = FulfillmentLogic.DecryptString(message, null);
            // LambdaResponse lam = FulfillmentLogic.getOrderLine("156F-9692-90E6-091D", "1002377317", true, null);
            // SyncLicenseDb.CreateConnection();
            //var credentials = new BasicAWSCredentials("AKIAVUV3SF2DMMEZYF53", "+jNNRACn3qVE+Z8Kc5eT/zBZLgPYhu7yjHAAug3w");
            //var client = new AmazonDynamoDBClient(credentials, RegionEndpoint.USWest2);
            //var context = new DynamoDBContext(client);
            //Dictionary<string, AttributeValue> key = new Dictionary<string, AttributeValue>
            //{
            //    { "HOST_ID", new AttributeValue { S = "GFFG27-E1D1G9-7QNC37-NQF4" } },
            //    { "LICENSE_ID", new AttributeValue { S = "" } }
            //};

            //// Create GetItem request
            //GetItemRequest request = new GetItemRequest
            //{
            //    TableName = "KSM_ISG_SYNC",
            //    Key = key,
            //};

            //// Issue request
            //GetItemResponse result = await client.GetItemAsync(request);

            //// View response
            //Console.WriteLine("Item:");
            //Dictionary<string, AttributeValue> item = result.Item;
            //// (new System.Collections.Generic.IDictionaryDebugView<string, Amazon.DynamoDBv2.Model.AttributeValue>(result.Item).Items[0]).Value.S

            //Console.WriteLine("Exp_date := {0}", item["LIC_EXP_DATE"].S);
            //Console.WriteLine("Host Id := {0}", item["HOST_ID"].S);
            //Console.WriteLine("License Id:= {0}", item["LICENSE_ID"].S);
            //Console.WriteLine("Qty := {0}", item["QTY"].N);
            //Console.WriteLine("Subs_End_Date := {0}", item["SUS_END_DATE"].S);
            //Console.WriteLine("License type := {0}", item["LIC_TYPE"].S);
            //Console.ReadLine();

            //AttributeValue hashKey = new AttributeValue { S = "GFFG27-E1D1G9-7QNC37-NQF2" };

            //// Define query condition to search for range-keys that begin with the string "4"
            //Condition condition = new Condition
            //{
            //    ComparisonOperator = "BEGINS_WITH",
            //    AttributeValueList = new List<AttributeValue>
            //    {
            //        new AttributeValue { S = "4" }
            //    }
            //};

            //// Create the key conditions from hashKey and condition
            //Dictionary<string, Condition> keyConditions = new Dictionary<string, Condition>
            //{
            //    // Hash key condition. ComparisonOperator must be "EQ".
            //    {
            //        "HOST_ID",
            //        new Condition
            //        {
            //            ComparisonOperator = "EQ",
            //            AttributeValueList = new List<AttributeValue> { hashKey }
            //        }
            //    },
            //    // Range key condition
            //    {
            //        "LICENSE_ID",
            //        condition
            //    }
            //};

            //// Define marker variable
            //Dictionary<string, AttributeValue> startKey = null;
            //List<LicenseSync> licCol = new List<LicenseSync>();
            //do
            //{
            //    // Create Query request
            //    QueryRequest request = new QueryRequest
            //    {
            //        TableName = "KSM_ISG_SYNC",
            //        ExclusiveStartKey = startKey,
            //        KeyConditions = keyConditions
            //    };

            //    // Issue request
            //    var result = await client.QueryAsync(request);

            //    // View all returned items
            //    List<Dictionary<string, AttributeValue>> items = result.Items;
            //    foreach (Dictionary<string, AttributeValue> item in items)
            //    {
            //        LicenseSync lic = new LicenseSync();
            //        foreach (var keyValuePair in item)
            //        {

            //            if (keyValuePair.Key == "LIC_EXP_DATE")
            //            {
            //                lic.Exp_Date = keyValuePair.Value.S;
            //            }
            //            else if (keyValuePair.Key == "SUS_END_DATE")
            //            {
            //                lic.End_Date = keyValuePair.Value.S;
            //            }
            //            else if (keyValuePair.Key == "LIC_TYPE")
            //            {
            //                lic.Lic_Type = keyValuePair.Value.S;
            //            }
            //            else if (keyValuePair.Key == "HOST_ID")
            //            {
            //                lic.Host_Id = keyValuePair.Value.S;
            //            }
            //            else if (keyValuePair.Key == "Qty")
            //            {
            //                lic.Qty = keyValuePair.Value.N;
            //            }
            //            else if (keyValuePair.Key == "ACTIVATION_CODE")
            //            {
            //                lic.Activation_Code = keyValuePair.Value.S;
            //            }


            //            //Console.WriteLine("{0} : S={1}, N={2}, SS=[{3}], NS=[{4}]",
            //            //    keyValuePair.Key,
            //            //    keyValuePair.Value.S,
            //            //    keyValuePair.Value.N,
            //            //    string.Join(", ", keyValuePair.Value.SS ?? new List<string>()),
            //            //    string.Join(", ", keyValuePair.Value.NS ?? new List<string>()));
            //        }
            //        licCol.Add(lic);
            //    }

            //    // Set marker variable
            //    startKey = result.LastEvaluatedKey;
            //} while (startKey != null && startKey.Count > 0);

            //Console.ReadLine();
            //Table books = Table.LoadTable(client, "License-Sync");
            //var search = books.Scan(new ScanOperationConfig()
            //{
            //    ConsistentRead = true
            //});
            //Console.WriteLine("ScanAsync: printing scan response");
            //var documents = await search.GetRemainingAsync();
            //documents.ForEach((d) => {
            //    Console.WriteLine(d);
            //});

            //    Table StudentTable = Table.LoadTable(client, "License-Sync");

            //    ScanFilter scanFilter = new ScanFilter();
            //    scanFilter.AddCondition("SyncId", ScanOperator.Equal, 1);
            //    List<Document> documentList = new List<Document>();

            //    Search search = StudentTable.Scan(scanFilter);
            //    documentList = await search.GetNextSetAsync();

            //    foreach (var document in documentList)
            //    {

            //        foreach (var attribute in document.GetAttributeNames())
            //        {
            //            string stringValue = null;
            //            var value = document[attribute];
            //            if (value is Primitive)
            //                stringValue = value.AsPrimitive().Value.ToString();
            //            else if (value is PrimitiveList)
            //                stringValue = string.Join(",", (from primitive
            //                                in value.AsPrimitiveList().Entries
            //                                                select primitive.Value).ToArray());
            //            if (attribute == "studentId")
            //            {
            //                Console.WriteLine(stringValue);
            //            }
            //            else if (attribute == "studentName")
            //            {
            //                row.Cells[1].Value = stringValue;
            //            }
            //            else if (attribute == "collegeName")
            //            {
            //                row.Cells[2].Value = stringValue;
            //            }
            //            else if (attribute == "className")
            //            {
            //                row.Cells[3].Value = stringValue;
            //            }
            //        }
            //        DGV.Rows.Add(row);


            //    }
            //} while (!search.IsDone);  


            //    Console.ReadLine();
            //string licenseFile = FulfillmentLogic.sqlgetlicensefile();
            //byte[] data = Convert.FromBase64String(licenseFile);

            //string decodedString = Encoding.ASCII.GetString(data);
            //Console.WriteLine();
            //  ConfigSettings.Connection = "Data Source=(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST =ksmperlp.sgp.is.keysight.com)(PORT = 1523)))(CONNECT_DATA = (SERVICE_NAME = GSDC))); Persist Security Info = True; User Id = license; Password = re11ister; ";
            // string s =  Test.checkConnection(ConfigSettings.Connection);
            //  string d = string.Empty;
            //byte[] byteArray = Encoding.ASCII.GetBytes(@"  f  €®                VERIWAVE     M0013e9001315     \ ]O    ]        C j    l        S         ’        kC¯×¾Ò5¼ª¦5WîúÞæ2¨6    z         €IxVeriWave    4 –   - —    ˜54C1-B294-958E-F93F     ™        ¦        §WB1000 Management Blade     ¨WB1000    ( ­    ®        ¯        °         Ê        Î        Ò     ");
            //MemoryStream stream = new MemoryStream(byteArray);
            //FulfillmentLogic.DecryptFNE(stream);
            //  LambdaResponse lam = FulfillmentLogic.getProductFeatures("N9030AK-C35");

            //LicenseResponse.Rawlicense licenseResponse = new LicenseResponse.Rawlicense();
            //licenseResponse = FulfillmentLogic.GetAPI("1095145");


            // LambdaResponse lam = FulfillmentLogic.getSubscribeNetDetails(System.IO.File.ReadAllText(@"E:/file1.bin"), null);

            //////file.Close();

            //////// Suspend the screen.
            ////Console.ReadLine();
            //LicenseResponse.Rawlicense licenseResponse = new LicenseResponse.Rawlicense();
            //licenseResponse = FulfillmentLogic.GetAPI("1116081");

            //byte[] buffer5 = Encoding.GetEncoding("iso-8859-1").GetBytes(licenseResponse.License[0]);
            //MemoryStream ms5 = new MemoryStream(buffer5);
            ////write to file
            //FileStream file5 = new FileStream(@"E:\\file5.bin", FileMode.Create, FileAccess.Write);
            //ms5.WriteTo(file5);
            //file5.Close();
            //ms5.Close();

            //Console.WriteLine(licenseResponse.License[0]);
            //Console.ReadLine();
            List<ActivationCode> actCodes = new List<ActivationCode>();
            ActivationCode act = new ActivationCode();
            act.activationID = "B09A-FC75-C9DA-7E40";
            act.quantity = "1";
            actCodes.Add(act);
            //        ActivationCode act1 = new ActivationCode();
            //        act1.activationID = "959D-F71F-0848-66EA";
            //        act1.quantity = "2";
            //        actCodes.Add(act1);

            //        ActivationCode act3 = new ActivationCode();
            //        act3.activationID = "859D-F71F-0848-66EA";
            //        act3.quantity = "2";
            //        actCodes.Add(act3);

            //        var query = actCodes
            //.GroupBy(f => new { f.activationID , f.quantity})
            //.Select(group => new { quantity = group.Sum(f => Convert.ToInt64(f.quantity)) });

            //        var newList = actCodes
            //        .GroupBy(x => new { x.activationID })
            //        .Select(y => new ActivationCode()
            //        {
            //            activationID = y.Key.activationID,
            //            quantity = Convert.ToString(y.Sum(f => Convert.ToInt64(f.quantity)) )           
            //        }
            //        ).ToList();

            LambdaResponse lam = FulfillmentLogic.fulfillment(actCodes, "01c044-4d41f8-c7306e-d510", new System.Text.StringBuilder("Test"), null);
            Console.ReadLine();
            //InstalledFulfillment iam = new InstalledFulfillment();
            //iam.activationCode = "02C5-3278-C3DD-5048";
            //iam.expirationDate = "02-FEB-22"; ;
            //iam.maintenanceEndDate = "02-FEB-22";
            //iam.quantity = "2";
            //List<InstalledFulfillment> lstIns = new List<InstalledFulfillment>();
            //lstIns.Add(iam);
            //InstalledFulfillment iam1 = new InstalledFulfillment();
            //iam1.activationCode = "235E-A15F-BB05-FE24";
            //iam1.expirationDate = "permanent"; ;
            //iam1.maintenanceEndDate = "10-Dec-2024";
            //iam1.quantity = "1";
            //lstIns.Add(iam1);
            //InstalledFulfillment iam4 = new InstalledFulfillment();
            //iam4.activationCode = "9582-A18B-6F10-54A6";
            //iam4.expirationDate = "23-Oct-2020"; ;
            //iam4.maintenanceEndDate = "23-Oct-2020";
            //iam4.quantity = "2";
            //lstIns.Add(iam4);
            //InstalledFulfillment iam2 = new InstalledFulfillment();
            //iam2.activationCode = "959D-F71F-0848-66EA";
            //iam2.expirationDate = "permanent"; ;
            //iam2.maintenanceEndDate = "23-Oct-2020";
            //iam2.quantity = "4";
            //lstIns.Add(iam);

            // ILambdaContext contextLog = (ILambdaContext)context;
            //LambdaResponse lam1 = await FulfillmentLogic.SyncHost(lstIns, "false", "01c044-4d41f8-c7306e-d511", string.Empty, new System.Text.StringBuilder("Test"), null);
            // LambdaResponse lam = FulfillmentLogic.Sync(lstIns, "false", "BypassDuo99001", string.Empty, new System.Text.StringBuilder("Test"), null);
            // Console.WriteLine(lam.body);

            //string filecontent1 = string.Empty;



            //filecontent1 = GetFileContent("fullname", "FNE");

            // string requestbody = "LS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLTg5MDk0NjcwMTg5NzEzNjExMTcxMDMxMg0KQ29udGVudC1EaXNwb3NpdGlvbjogZm9ybS1kYXRhOyBuYW1lPSIiOyBmaWxlbmFtZT0icGhvLmJpbiINCkNvbnRlbnQtVHlwZTogYXBwbGljYXRpb24vb2N0ZXQtc3RyZWFtDQoNCgAAAdMAABIQAAAAAAAAAAsAAQAAAAACAAAAEAAHAlZFUklXQVZFAAAAABQATQIxMTIyMzM0NDU1NjYAAAAACwBcAAAAAAAAAAALAF0AAAAAAAAAAEMAagUAAAALAGwAAAAABQAAAAsAUwAAAAAAAAAACwCSAAAAAAIAAAAbAGsLWNIZcYEW9wEAuiQ3ltlNo+R9uHwAAAALAHoAAAAAAAAAABQAgAJJeFZlcmlXYXZlTE0AAAAANACWBQAAAC0AlwUAAAAbAJgCRDhBMC0xNDg3LTAwRTUtNjhCMQAAAAALAJkAAAAAAQAAAAsApgAAAAAIAAAAJgCnAldBVkVMSUMgV2luZG93cyBMaWNlbnNlIFNlcnZlcgAAAAAOAKgCV2F2ZUxNAAAAACgArQUAAAALAK4AAAAH4wAAAAsArwAAAAAEAAAACwCwAAAAAAAAAAAoAL8FAAAACwDBAF4odPcAAAALAMIAAAAAAAAAAAsAwwAAAAACAAAACwDKAAAAAAYAAAALAM4AAAAAAQAAAAsA0gAAAAAAAAAAJgDwBQAAAAsARAAAAAAIAAAAFABFAjExMjIzMzQ0NTU2NgAAAAALAQEAAAAAAQAAAAsBGgAAAAABDQotLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tODkwOTQ2NzAxODk3MTM2MTExNzEwMzEyLS0NCg==";
            // //string requestbody = "LS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLTA4NjM1MTg5MjQ3MjAyMDEzNTYzNTM2Mg0KQ29udGVudC1EaXNwb3NpdGlvbjogZm9ybS1kYXRhOyBuYW1lPSIiOyBmaWxlbmFtZT0icmVxLmJpbiINCkNvbnRlbnQtVHlwZTogYXBwbGljYXRpb24vb2N0ZXQtc3RyZWFtDQoNCgAAAXUAAO0X/////wAAAAsAAQAAAAACAAAAKACtBQAAAAsArgAAAAfhAAAACwCvAAAAAAsAAAALALAAAAAAAAAAAAsAygAAAAAGAAAAEAAHAlZFUklXQVZFAAAAACgAvwUAAAALAMIAAAABbwAAAAsAwQDRhe4zAAAACwDDAAAAAAEAAAALAF0AAAAAAAAAAAsAXAAAAAAAAAAAJgDwBQAAAAsARAAAAAAIAAAAFABFAjAwMTNlMTIzMjAyMAAAAAALAKYAAAAACAAAABQATQIwMDEzZTEyMzIwMjAAAAAACwDOAAAAAAEAAAAUAKcCV2F2ZUxNIEJsYWRlAAAAAA4AqAJXYXZlTE0AAAAACwDSAAAAAAAAAAASAIACSXhWZXJpV2F2ZQAAAAALAQEAAAAAAQAAAEMAagUAAAALAGwAAAAABQAAAAsAkgAAAAACAAAACwBTAAAAAAAAAAAbAGsLAgxaBrd17BLej9Xl2rt+kkkNN1wNCi0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0wODYzNTE4OTI0NzIwMjAxMzU2MzUzNjItLQ0K";
            // byte[] data = Convert.FromBase64String(requestbody);

            //string decodedString = Encoding.GetEncoding("iso-8859-1").GetString(data);
            //MemoryStream ms2 = new MemoryStream(data);
            ////write to file
            //FileStream file2 = new FileStream(@"E:\\file1.bin", FileMode.Create, FileAccess.Write);
            //ms2.WriteTo(file2);
            //file2.Close();
            //ms2.Close();

            //  byte[] buffer1 = Encoding.GetEncoding("iso-8859-1").GetBytes(decodedString);

            // List<string> lines = new List<string>();
            // using (System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(data))
            // {
            //     lines = fileUploadRequestParser(streamBitmap);
            //     //StreamReader sr = new StreamReader(streamBitmap, Encoding.UTF8);
            //     //sr.ReadLine
            // }

            // string fileContent = string.Empty;

            // if (lines.Count > 0)
            // {
            //     fileContent = lines[0].TrimEnd();
            // }



            //// string[] lines1 = File.ReadAllLines("E:/file1.bin", System.Text.Encoding.GetEncoding("iso-8859-1"));

            // byte[] buffer33 = Encoding.Default.GetBytes(fileContent);
            // MemoryStream ms3 = new MemoryStream(buffer33);
            // //write to file
            // FileStream file3 = new FileStream(@"E:\\file2.bin", FileMode.Create, FileAccess.Write);
            // ms3.WriteTo(file3);
            // file3.Close();
            // ms3.Close();

            // // //fileContent = @"  Ӡ                 VERIWAVE     M112233445566     \         ]        C j    l        S                 kXҙq÷ º$7ٍ£佸|    z         IxVeriWaveLM    4    -     D8A0-1487-00E5-68B1             ¦       & §WAVELIC Windows License Server     ¨WaveLM    ( ­    ®   㠠  ¯        °        ( ¿    Á ^(t÷             à       ʠ       Π       Ҡ       & ퟖ�  D        E112233445566               ";
            // // // byte[] convertedByte = Encoding.Unicode.GetBytes(stringToConvert);
            // // byte[] buffer = Encoding.ASCII.GetBytes(fileContent);

            // //// byte[] arr = StringToBytesArray(str);

            // // // string conv = Convert.ToBase64String(buffer);
            // byte[] buffer22 = Encoding.GetEncoding("iso-8859-1").GetBytes(fileContent);
            // MemoryStream ms1 = new MemoryStream(buffer22);
            // //write to file
            // FileStream file1 = new FileStream(@"E:\\file.bin", FileMode.Create, FileAccess.Write);
            // ms1.WriteTo(file1);
            // file1.Close();
            // ms1.Close();

            // string filecontent = string.Empty;
            // // //var n = DateTime.Now;
            // // ////string filename = string.Format("{0}_{1:00}_{2:00}_{3:00}_{4:00}{5:00}{6:00}{7}", hpf.FileName.Replace(".", "_"), n.Year - 2000, n.Month, n.Day, n.Hour, n.Minute, n.Second, Path.GetExtension(hpf.FileName));
            // // //string fullname =  @"E:\KSMRepository\TestApp\Decrypt\file.bin";
            // // //if (!File.Exists(fullname))
            // // //{

            //      filecontent = GetFileContent("fullname", "FNE");
            // //// }

            //  string s = filecontent;
        }



        private static List<string> fileUploadRequestParser(Stream stream)
        {
            List<String> lstLines = new List<string>();
            StreamReader textReader = new StreamReader(stream, Encoding.GetEncoding("iso-8859-1"));
            string sLine = textReader.ReadLine();
            Regex regex = new Regex("(^-+)|(^content-)|(^$)|(^submit)", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline);
            //contextLog.Logger.Log("SubscribeNet textReader line :" + sLine);
            while (sLine != null)
            {
                if (!regex.Match(sLine).Success)
                {
                    lstLines.Add(sLine);
                }
                sLine = textReader.ReadLine();
            }

            textReader.Dispose();
            stream.Flush();
            stream.Dispose();
            return lstLines;

        }

        public static Stream GenerateStreamFromString(string s)
        {
            byte[] buffer = Encoding.GetEncoding("iso-8859-1").GetBytes(s);
            MemoryStream ms = new MemoryStream(buffer);
            return ms;
        }

        public static string GetFileContent(string fullname, string filetype)
        {
            try
            {
                string strArguments = string.Empty;
                strArguments = Environment.CurrentDirectory;
                Process processJar = new Process();
                //if (string.IsNullOrEmpty(filetype))
                //{
                //    strArguments = " -jar " + @"decrypt.jar -i " + fullname;
                //}
                //else
                //{
                    strArguments = " -jar " + @"decrypt.jar -i " + @"E:\file.bin" + " " + filetype;
              //  }

                processJar.StartInfo.FileName = "\"" + @"java" + "\"";
                processJar.StartInfo.Arguments = strArguments;
                processJar.StartInfo.WorkingDirectory = @"E:\KSMRepository\TestApp\Decrypt\";
                //@"E:\Anup\FileDecoder\FileDecoder\bin\Debug\Decrypt\";

                processJar.StartInfo.UseShellExecute = false;
                processJar.StartInfo.RedirectStandardOutput = true;

                processJar.Start();
                string returncontent = processJar.StandardOutput.ReadToEnd();
                processJar.WaitForExit(2000);
                return returncontent;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

