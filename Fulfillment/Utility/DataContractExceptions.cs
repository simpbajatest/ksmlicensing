﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IxiaFulfillment.Utility
{
    public static class DataContractExceptions
    {
        public static string getErrorMessage(string errorCode)
        {
            string value = string.Empty;
            Dictionary<string, string> getErrorMessage =
            new Dictionary<string, string>(){
                                {"AUTHENTICATION_FAILED", "Authentication failed"},
                                {"DATABASE_CONNECTION_ISSUE", "Database connection issue"},
                                {"ENTITLEMENT_CODE_NOT_FOUND", "Request format is invalid. Please provide an Entitlement Code." },
                                {"ACTIVATION_CODE_NOT_FOUND_ON_ENT", "No Activation Codes assigned on the received Entitlement Code." },
                                {"ACTIVATION_CODE_NOT_FOUND", "Activation Codenot found." },
                                {"INVALID_ACTIVATION_CODE", "The Activation Code received is not valid"} };

            getErrorMessage.TryGetValue(errorCode, out value);
            return value.ToString();
        }
    }
}
