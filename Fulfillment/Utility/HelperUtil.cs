﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Fulfillment.Controller;
using Lambda.Logic;

namespace Fulfillment.Utility
{
    public static class HelperUtil
    {       
        public static string getMethodName(string inputPath, string httpMethod)
        {
            InitializeConnection();
            httpMethod = httpMethod.ToLower();
            
            if (inputPath.ToLower().Contains("get") || inputPath.ToLower().Contains("post"))
            {
                httpMethod = string.Empty;
            }

            string value = string.Empty;
            Dictionary<string, string> getmethodNameDict =
            new Dictionary<string, string>(){
                                {"/api/testing", httpMethod + "Testing"},
                                 {"/", "lamdaWarmer"},
                                {"/api/dbtesting", "dbTesting"},
                                {"/api/get-activation-codes-by-entitlement-code", httpMethod + "getActivationsByEntitlementCode"},
                                {"/api/get-quantity-by-activation-codes", httpMethod + "getQuantityByActivationCodes"},
                                {"/api/get-product-info-by-activation-codes", httpMethod + "getProductInfoByActivationCodes" },
                                {"/api/read-activation-code", "readActivationCode"},
                                //{"/api/fulfillment", "fulfillmentNew"},
                                {"/api/fulfillment", "setFulfillment"},
                                {"/api/read-entitlement", "readActivationCode"},
                                {"/api/sync", "SyncHost"},
                                {"/api/misc/time", "getEpochTime"},
                                //{"/api/asset/features", "getProductFeatures"},
                                //{"/api/asset/customer", "getCustomerDetails"},
                                {"/control/ixia/deviceservices", "getSubscribeNetDetails"}};
            
            getmethodNameDict.TryGetValue(inputPath.ToLower(), out value);
            return Convert.ToString(value);
        }

        public static Type getClassType(string inputPath)
        {
            Type typeInstance = null;
            if (inputPath.Contains("/api/") || inputPath.Contains("/control/") || inputPath.Contains("/") || inputPath.Contains("/v1/"))
            {
                typeInstance = typeof(FulfillmentController);
            }

            return typeInstance;
        }     

        public static void InitializeConnection()
        {
            var envVariable = Environment.GetEnvironmentVariable("DbConnection");
            FulfillmentLogic.InitializeConnection(envVariable);
        }
    }
}
