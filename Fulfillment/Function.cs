using System;
using System.Collections.Generic;
using System.Linq;
using Amazon.Lambda.Core;
using Amazon.Lambda.ApplicationLoadBalancerEvents;
using Fulfillment.Utility;
using Lambda.Entities;
using System.Reflection;
using System.Net;
using System.Threading.Tasks;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Fulfillment
{
    public class Function
    {
        
        /// <summary>
        /// Lambda function handler to respond to events coming from an Application Load Balancer.
        /// 
        /// Note: If "Multi value headers" is disabled on the ELB Target Group then use the Headers and QueryStringParameters properties 
        /// on the ApplicationLoadBalancerRequest and ApplicationLoadBalancerResponse objects. If "Multi value headers" is enabled then
        /// use MultiValueHeaders and MultiValueQueryStringParameters properties.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task<ApplicationLoadBalancerResponse> FunctionHandler(ApplicationLoadBalancerRequest request, ILambdaContext context)
        {
            string pathName = request.Path;
            //string clientType = string.Empty;
            context.Logger.Log("path:" + pathName);            
           
            try
            {
                Type typeInstance = HelperUtil.getClassType(pathName);
                string methodName = HelperUtil.getMethodName(pathName, request.HttpMethod);
                context.Logger.Log("methodName: " + methodName);
                if (string.IsNullOrEmpty(methodName))
                {                   
                    var response1 = new ApplicationLoadBalancerResponse
                    {
                        StatusCode = Convert.ToInt32(HttpStatusCode.NotFound),
                        StatusDescription = "404 Not found",
                        IsBase64Encoded = false
                    };

                    response1.Headers = new Dictionary<string, string>
                        {
                            {"Content-Type", "application/json" }
                        };
                    return response1;
                }

                LambdaResponse lamResponse = null;

                if (typeInstance != null)
                {
                    context.Logger.Log("request: " + pathName + " - " + request.Body);
                    MethodInfo methodInfo = typeInstance.GetMethod(methodName);
                    ParameterInfo[] parameters = methodInfo.GetParameters();
                   
                    object classInstance = Activator.CreateInstance(typeInstance, null);                   
                    
                    if (pathName.ToLower().Contains("sync"))
                    {  
                        if (parameters.Length == 0)
                        {
                            var task = methodInfo.Invoke(classInstance, null) as Task<LambdaResponse>;
                            lamResponse = task.Result;
                            context.Logger.Log("Response: " + pathName + "-" + task.Result.body);
                        }
                        else
                        {
                            object[] parametersArray = new object[] { request, context };
                            var task = methodInfo.Invoke(classInstance, parametersArray) as Task<LambdaResponse>;
                            context.Logger.Log("Response: " + pathName + "-" + task.Result.body);
                            lamResponse = task.Result;
                        }
                    }
                    else
                    {
                        if (parameters.Length == 0)
                        {
                            lamResponse = methodInfo.Invoke(classInstance, null) as LambdaResponse;                           
                        }
                        else
                        {
                            object[] parametersArray = new object[] { request, context };
                            lamResponse = methodInfo.Invoke(classInstance, parametersArray) as LambdaResponse;                           
                        }
                    }
                    context.Logger.Log("Response: " + pathName + "-" + lamResponse.body);                    
                }                

                var response = new ApplicationLoadBalancerResponse
                {
                    StatusCode = Convert.ToInt32(lamResponse.statusCode),                   
                    StatusDescription = "200 OK",
                    IsBase64Encoded = false
                };               

                if (pathName.ToLower().Contains("control"))
                {
                    //byte[] data = System.Text.Encoding.UTF8.GetBytes(lamResponse.body);
                    response.Body = lamResponse.body;

                    response.IsBase64Encoded = true;
                    response.Headers = new Dictionary<string, string>
                    {
                        {"Content-Type", "application/octet-stream" },
                        {"connection", "Keep-Alive" },
                        {"x_frame_options", "SAMEORIGIN" },
                        {"Set_Cookie", "flexnet-http-cookie-123085=14b5a3d9f416a027c163c8d98cbbeae8caaba371afed363d6c546205786e5590076ac5f9;path=/;secure;httponly" }
                    };
                }
                else
                {
                    response.Headers = new Dictionary<string, string>
                    {
                        {"Content-Type", "application/json" }
                    };

                    //if (pathName.ToString().ToLower().Contains("read-activation-code") && !string.IsNullOrEmpty(lamResponse.body))
                    //{
                    //    byte[] byt = System.Text.Encoding.UTF8.GetBytes(lamResponse.body);
                    //    lamResponse.body = Convert.ToBase64String(byt);
                    //   // response.IsBase64Encoded = true;
                    //}

                    response.Body = lamResponse.body;                   
                }                

                return response;
            }
            catch
            {
                var response = new ApplicationLoadBalancerResponse
                {
                    StatusCode = Convert.ToInt32(HttpStatusCode.NotFound),
                    StatusDescription = "404 Not found",
                    IsBase64Encoded = false
                };

                response.Headers = new Dictionary<string, string>
            {
                {"Content-Type", "application/json" }
            };

                return response;
            }
        }
    }
}
