﻿using System;
using System.Net;
using IxiaFulfillment.Utility;
using Lambda.Entities;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Lambda.Logic;
using Amazon.Lambda.Core;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Amazon.Lambda.ApplicationLoadBalancerEvents;
using System.Linq;
using System.IO;
using System.Threading.Tasks;

namespace Fulfillment.Controller
{
    /// <summary>
    /// Contains data and logic for getting Autoactivation licenses.
    /// </summary>
    public class FulfillmentController
    {
        /// <summary>
        /// this function is only for testing purpose and has not been called by any client.
        /// </summary>
        public LambdaResponse getTesting(object albProxyRequest, object context)
        {
            LambdaResponse response;
            string hostID = string.Empty;
            string orderNumber = string.Empty;
            JObject jsonObject = new JObject(); ;

            ApplicationLoadBalancerRequest request = (ApplicationLoadBalancerRequest)albProxyRequest;

            if (request.Body != null)
            {
                //request.QueryStringParameters.TryGetValue("hostID", out hostID);
                //request.QueryStringParameters.TryGetValue("orderNumber", out orderNumber);                  
                string json = request.Body;
                RootObject instance = JsonConvert.DeserializeObject<RootObject>(json);
                hostID = instance.hostID;
                orderNumber = instance.orderNumber;

                if (!string.IsNullOrEmpty(hostID) && !string.IsNullOrEmpty(orderNumber))
                {
                    jsonObject["yourRequest"] = string.Format(
                                        "HostID: {0}; OrderNumber: {1}",
                                        hostID,
                                        orderNumber);
                    jsonObject["success"] = "true";

                    return response = new LambdaResponse
                    {
                        statusCode = HttpStatusCode.OK,
                        body = jsonObject.ToString()
                    };
                }
                else
                {
                    return response = new LambdaResponse
                    {
                        statusCode = HttpStatusCode.OK,
                        body = "OK"
                    };

                }
            }
            else
            {
                return response = new LambdaResponse
                {
                    statusCode = HttpStatusCode.OK,
                    body = "OK"
                };

            }
        }

        /// <summary>
        /// This function is only for testing purpose and has been called by any client  as first API.
        /// </summary>
        public LambdaResponse postTesting(object albProxyRequest, object context)
        {
            string clientType = string.Empty;
            ILambdaContext contextLog = (ILambdaContext)context;
            
            JObject jsonObject = new JObject();
            try { 
                LambdaResponse response;               
               
                jsonObject["testSuccessful"] = "true";
               
                return response = new LambdaResponse
                {
                    statusCode = HttpStatusCode.OK,
                    body = jsonObject.ToString()
                };
            }
            catch (Exception ex)
            {
                contextLog.Logger.Log("postTesting -" + ex.Message);
                jsonObject["error"] = "Error in testing";
                return errorResponse(jsonObject);
            }
        }

        /// <summary>
        /// This function is only for keeping lambda warm to avoid any cold start.
        /// </summary>
        public LambdaResponse lamdaWarmer(object albProxyRequest, object context)
        {
            ILambdaContext contextLog = (ILambdaContext)context;
            JObject jsonObject = new JObject();
            try
            {
                LambdaResponse response;

                jsonObject["lambdaWarmer"] = "running";

                return response = new LambdaResponse
                {
                    statusCode = HttpStatusCode.OK,
                    body = jsonObject.ToString()
                };
            }
            catch (Exception ex)
            {
                contextLog.Logger.Log("lamdaWarmer -" + ex.Message);
                jsonObject["error"] = "Error in lambda warmer";
                return errorResponse(jsonObject);
            }
        }

        /// <summary>
        /// This function is only for getting activations/orderlines by entitlement code.
        /// </summary>
        public LambdaResponse getActivationsByEntitlementCode(object albProxyRequest, object context)
        {
            LambdaResponse response;
            ILambdaContext contextLog = (ILambdaContext)context;
            JObject jsonObject = new JObject();

            try
            {
                ApplicationLoadBalancerRequest request = (ApplicationLoadBalancerRequest)albProxyRequest;
                if (request.Body != null)
                {
                    string json = request.Body;
                    RootObject instance = JsonConvert.DeserializeObject<RootObject>(json);

                    if (!string.IsNullOrEmpty(instance.entitlementCode))
                    {
                        response = FulfillmentLogic.getActivationsByEntitlementCode(instance.entitlementCode, instance.returnAvailableQty);
                        return response;
                    }
                    else
                    {
                        jsonObject["error"] = "The Entitlement Code received is not valid.";
                        return errorResponse(jsonObject);
                    }
                }
                else
                {
                    jsonObject["error"] = "The Entitlement Code received is not valid.";
                    return errorResponse(jsonObject);
                }
            }
            catch (Exception ex)
            {
                contextLog.Logger.Log("getActivationsByEntitlementCode -" + ex.Message);
                jsonObject["error"] = "Error in getting activations by Entitlement Code";
                return errorResponse(jsonObject);
            }
        }

        /// <summary>
        /// This function is only for providing qty information by activation code.
        /// </summary>
        public LambdaResponse getQuantityByActivationCodes(object albProxyRequest, object context)
        {
            LambdaResponse response;
            ILambdaContext contextLog = (ILambdaContext)context;
            JObject jsonObject = new JObject();
            bool returnAvailableQty = false;

            try
            {
                ApplicationLoadBalancerRequest request = (ApplicationLoadBalancerRequest)albProxyRequest;

                if (request.Body != null)
                {
                    string json = request.Body;
                    RootObject instance = JsonConvert.DeserializeObject<RootObject>(json);
                    returnAvailableQty = instance.returnAvailableQty;

                    if (instance.activationCodes == null)
                    {
                        jsonObject["error"] = DataContractExceptions.getErrorMessage("ACTIVATION_CODE_NOT_FOUND");
                        return response = new LambdaResponse
                        {
                            statusCode = HttpStatusCode.OK,
                            body = jsonObject.ToString()
                        };
                    }

                    if (instance.activationCodes.Count > 0)
                    {
                        JObject activationsWithQty = new JObject();

                        foreach (string actCode in instance.activationCodes)
                        {
                            string activationId = actCode;
                            OrderLine orderLine = null;

                            //get details on basis of activation id from db
                            try
                            {
                                orderLine = FulfillmentLogic.getOrderLineByActivationId(activationId);
                            }
                            catch (Exception ex)
                            {                                
                                orderLine = FulfillmentLogic.getOrderLineByActivationId(activationId);
                            }

                            JObject details = new JObject();

                            if (orderLine == null)
                            {
                                details["error"] = "Activation code not found";
                            }
                            else
                            {                               
                                details["totalQty"] = orderLine.QtyOrdered;

                                if (returnAvailableQty)
                                {
                                    details["availableQty"] = orderLine.QtyRedeemable - orderLine.QtyRedeemed;
                                }
                            }

                            activationsWithQty[activationId] = details;
                        }

                        if (activationsWithQty.Count > 0)
                        {
                            jsonObject["activations"] = activationsWithQty;
                        }
                        else
                        {
                            jsonObject["error"] = DataContractExceptions.getErrorMessage("ACTIVATION_CODE_NOT_FOUND_ON_ENT");
                        }

                        return response = new LambdaResponse
                        {
                            statusCode = HttpStatusCode.OK,
                            body = jsonObject.ToString()
                        };
                    }
                    else
                    {
                        jsonObject["error"] = DataContractExceptions.getErrorMessage("INVALID_ACTIVATION_CODE");
                        return errorResponse(jsonObject);
                    }
                }
                else
                {
                    jsonObject["error"] = DataContractExceptions.getErrorMessage("ENTITLEMENT_CODE_NOT_FOUND");
                    return errorResponse(jsonObject);
                }
            }
            catch (Exception ex)
            {
                contextLog.Logger.Log("getQuantityByActivationCodes -" + ex.Message);
                return FulfillmentLogic.fulfillmentMessage(402, "INVALID_REQUEST_PARAM", "Invalid input parameter", string.Empty);
            }
        }

        /// <summary>
        /// This function is for getting product information by activation code.
        /// </summary>
        public LambdaResponse getProductInfoByActivationCodes(object albProxyRequest, object context)
        {
            LambdaResponse response;
            ILambdaContext contextLog = (ILambdaContext)context;
            JObject jsonObject = new JObject();
            JArray errorsArray = new JArray();
            JObject activationsWithQty = new JObject();

            try
            {
                ApplicationLoadBalancerRequest request = (ApplicationLoadBalancerRequest)albProxyRequest;

                if (request.Body != null)
                {
                    string json = request.Body;
                    RootObject instance = JsonConvert.DeserializeObject<RootObject>(json);

                    if (instance.activationCodes == null)
                    {
                        jsonObject["error"] = DataContractExceptions.getErrorMessage("ACTIVATION_CODE_NOT_FOUND_ON_ENT");
                        return response = new LambdaResponse
                        {
                            statusCode = HttpStatusCode.OK,
                            body = jsonObject.ToString()
                        };
                    }

                    if (instance.activationCodes.Count > 0)
                    {
                        foreach (string actCode in instance.activationCodes)
                        {
                            string activationId = actCode;                            
                            OrderLine orderLine = null;

                            //get product details on basis of activation id from db
                            try
                            { 
                             orderLine = FulfillmentLogic.getOrderLineByActivationId(activationId);
                            }
                            catch (Exception ex)
                            {                              
                                orderLine = FulfillmentLogic.getOrderLineByActivationId(activationId);
                            }

                            if (orderLine != null)
                            {                                
                                Product prod = FulfillmentLogic.getProductByProdId(orderLine.ProductId);

                                if (prod != null)
                                {                                   
                                    JObject prodDetails = new JObject();

                                    prodDetails["partNumber"] = prod.ProductNum;
                                    prodDetails["productLine"] = prod.ProdLine;
                                    prodDetails["productDescription"] = prod.ProductDesc;
                                    activationsWithQty[activationId] = prodDetails;
                                }
                                else
                                {
                                    errorsArray.Add("Could not find Catalog Item for Part Number: " + orderLine.ProductNumber);
                                }
                            }
                            else
                            {
                                errorsArray.Add("Could not find Entitlement Line for Activation Code: " + activationId);
                            }
                        }
                    }
                    else
                    {
                        errorsArray.Add("No Activation Code sent in the request.");
                    }
                }

                if (activationsWithQty.Count > 0)
                {
                    jsonObject["activations"] = activationsWithQty;
                }

                if (errorsArray.Count > 0)
                {
                    jsonObject["errors"] = errorsArray;
                    contextLog.Logger.Log("getProductInfoByActivationCodes -" + errorsArray);
                }

                return response = new LambdaResponse
                {
                    statusCode = HttpStatusCode.OK,
                    body = jsonObject.ToString()
                };
            }
            catch (Exception ex)
            {
                contextLog.Logger.Log("getProductInfoByActivationCodes -" + ex.Message);
                return FulfillmentLogic.fulfillmentMessage(402, "INVALID_REQUEST_PARAM", "Invalid input parameter", string.Empty);
            }
        }

        public LambdaResponse getProductInfoByACs(object albProxyRequest, object context)
        {
            LambdaResponse response;
            ILambdaContext contextLog = (ILambdaContext)context;
            JObject jsonObject = new JObject();
            JArray errorsArray = new JArray();
            JObject activationsWithQty = new JObject();

            try
            {
                ApplicationLoadBalancerRequest request = (ApplicationLoadBalancerRequest)albProxyRequest;

                if (request.Body != null)
                {
                    string json = request.Body;
                    RootObject instance = JsonConvert.DeserializeObject<RootObject>(json);

                    if (instance.activationCodes == null)
                    {
                        jsonObject["error"] = DataContractExceptions.getErrorMessage("ACTIVATION_CODE_NOT_FOUND_ON_ENT");
                        return response = new LambdaResponse
                        {
                            statusCode = HttpStatusCode.OK,
                            body = jsonObject.ToString()
                        };
                    }

                    if (instance.activationCodes.Count > 0)
                    {
                        foreach (string actCode in instance.activationCodes)
                        {
                            string activationId = actCode;
                            
                            OrderLine orderLine = null;

                            //get product details on basis of activation id from db
                            try
                            {
                                orderLine = FulfillmentLogic.getOrderLineByActivationId(activationId);
                            }
                            catch (Exception ex)
                            {                                
                                orderLine = FulfillmentLogic.getOrderLineByActivationId(activationId);
                            }

                            if (orderLine != null)
                            {                                
                                Product prod = FulfillmentLogic.getProductByProdId(orderLine.ProductId);

                                if (prod != null)
                                {                                    
                                    JObject prodDetails = new JObject();

                                    prodDetails["partNumber"] = prod.ProductNum;
                                    prodDetails["productLine"] = prod.ProdLine;
                                    prodDetails["productDescription"] = prod.ProductDesc;
                                    activationsWithQty[activationId] = prodDetails;
                                }
                                else
                                {
                                    errorsArray.Add("Could not find Catalog Item for Part Number: " + orderLine.ProductNumber);
                                }
                            }
                            else
                            {
                                errorsArray.Add("Could not find Entitlement Line for Activation Code: " + activationId);
                            }
                        }
                    }
                    else
                    {
                        errorsArray.Add("No Activation Code sent in the request.");
                    }
                }

                if (activationsWithQty.Count > 0)
                {
                    jsonObject["activations"] = activationsWithQty;
                }

                if (errorsArray.Count > 0)
                {
                    jsonObject["errors"] = errorsArray;
                    contextLog.Logger.Log("getProductInfoByActivationCodes -" + errorsArray);
                }

                return response = new LambdaResponse
                {
                    statusCode = HttpStatusCode.OK,
                    body = jsonObject.ToString()
                };
            }
            catch (Exception ex)
            {
                contextLog.Logger.Log("getProductInfoByActivationCodes -" + ex.Message);
                return FulfillmentLogic.fulfillmentMessage(402, "INVALID_REQUEST_PARAM", "Invalid input parameter", string.Empty);
            }
        }

        /// <summary>
        /// This function is for getting activation code information information by activation code.
        /// </summary>
        public LambdaResponse readActivationCode(object albProxyRequest, object context)
        {
            LambdaResponse response = new LambdaResponse();
            ILambdaContext contextLog = (ILambdaContext)context;
            JObject jsonObject = new JObject();
            string clientType = string.Empty;
            //clientType = "ecommerce";
            try
            {
                ApplicationLoadBalancerRequest request = (ApplicationLoadBalancerRequest)albProxyRequest;               
                request.Headers.TryGetValue("ixia-client-id", out clientType);               

                if (string.IsNullOrEmpty(clientType))
                {
                    return FulfillmentLogic.fulfillmentMessage(
                        103,
                        "REQUEST_HEADER_CLIENT_ID_MISSING",
                        "Header \"Ixia-Client-Id\" is missing.",
                        string.Empty
                        );
                }
               
                if (request.Body != null)
                {
                    string json = request.Body;
                    string checkBase64 = json;
                    contextLog.Logger.Log("readActivationCode json " + json);
                    string base64str = string.Empty;

                    contextLog.Logger.Log("readActivationCode " + IsBase64String(checkBase64));
                    if (IsBase64String(checkBase64))
                    {
                        contextLog.Logger.Log("readActivationCode IsBase64 true");
                        try
                        {
                            byte[] data = Convert.FromBase64String(checkBase64.Replace("%3D", "=").Replace("%2B", "+").Replace("%2F", "/"));
                            string decodedString = Encoding.UTF8.GetString(data);
                            if (IsBase64String(decodedString))
                            {
                                json = decodedString;
                            }
                            
                            contextLog.Logger.Log("readActivationCode double base64");
                        }
                        catch (Exception ex)
                        {
                            contextLog.Logger.Log("readActivationCode double not base64");
                        }

                        base64str = json;
                    }
                    else
                    {
                        contextLog.Logger.Log("readActivationCode IsBase64 false");
                        byte[] byt = System.Text.Encoding.UTF8.GetBytes(json);
                        base64str = Convert.ToBase64String(byt);
                    }
                   
                    contextLog.Logger.Log("readActivationCode base64str " + base64str);
                    DecryptResponse.StringContent instance = FulfillmentLogic.DecryptString(base64str, context, clientType.ToLower());
                    contextLog.Logger.Log("readActivationCode AC " + instance.activationCode);
                    bool getHostInfo = (instance.getHostInfo == null) || (instance.getHostInfo != null && Convert.ToBoolean(instance.getHostInfo));

                    if (!string.IsNullOrEmpty(instance.activationCode) || !string.IsNullOrEmpty(instance.entitlementID))
                    {
                        response = FulfillmentLogic.readActivationCode(instance.activationCode, instance.entitlementID, getHostInfo, context, clientType.ToLower());
                    }
                    else
                    {
                        //return errorMsgReadAC();
                        response = FulfillmentLogic.fulfillmentMessage(403, "INVALID_SIGNATURE", "Invalid signature", string.Empty);
                        string encryt = FulfillmentLogic.EncryptString(response.body, clientType);
                        return response = new LambdaResponse
                        {
                            statusCode = HttpStatusCode.BadRequest,
                            body = encryt.ToString()
                        };
                    }

                    //response = FulfillmentLogic.getOrderLine();
                    return response;
                }
                else
                {
                   // return errorMsgReadAC();
                    response  = FulfillmentLogic.fulfillmentMessage(403, "INVALID_SIGNATURE", "Invalid signature", string.Empty);
                    string encryt = FulfillmentLogic.EncryptString(response.body, clientType);
                    return response = new LambdaResponse
                    {
                        statusCode = HttpStatusCode.BadRequest,
                        body = encryt.ToString()
                    };
                }
            }
            catch (Exception ex)
            {
                contextLog.Logger.Log("readActivationCode -" + ex.Message);
                //return errorMsgReadAC();
                //string responseBody = System.IO.File.ReadAllText(@".. /Fulfillment/ReadACErrorResponse.txt");
                response = FulfillmentLogic.fulfillmentMessage(402, "INVALID_REQUEST_PARAM", "Invalid input parameter", string.Empty);
                string encryt = FulfillmentLogic.EncryptString(response.body, clientType);
                return response = new LambdaResponse
                {
                    statusCode = HttpStatusCode.BadRequest,
                    body = encryt.ToString()
                };
            }
        }

        /// <summary>
        /// This function is for checking if request is base64 string.
        /// </summary>
        public static bool IsBase64String(string base64)
        {
            Span<byte> buffer = new Span<byte>(new byte[base64.Length]);
            return Convert.TryFromBase64String(base64, buffer, out int bytesParsed);
        }

        /// <summary>
        /// This function is for activating a license.
        /// </summary>
        public LambdaResponse setFulfillment(object albProxyRequest, object context)
        {
            string clientType = string.Empty;
            ILambdaContext contextLog = (ILambdaContext)context;
            LambdaResponse response = new LambdaResponse();
            JObject jsonObject = new JObject();
            StringBuilder metaData = new StringBuilder();

            try
            {
                ApplicationLoadBalancerRequest request = (ApplicationLoadBalancerRequest)albProxyRequest;
                request.QueryStringParameters.TryGetValue("client-type", out clientType);

                if (string.IsNullOrEmpty(clientType))
                {
                    return response = new LambdaResponse
                    {
                        statusCode = HttpStatusCode.Unauthorized
                    };
                }

                if (request.Body != null)
                {
                    string json = request.Body;

                    RootObject instance = JsonConvert.DeserializeObject<RootObject>(json);

                    if (instance.hostDetails != null)
                    {
                        metaData.Append("hostDetails:" + instance.hostDetails.ToString());
                    }

                    if (!string.IsNullOrEmpty(instance.alias))
                    {
                        metaData.Append("alias:" + instance.alias.ToString());
                    }

                    if (!string.IsNullOrEmpty(instance.dnsName))
                    {
                        metaData.Append("dnsName:" + instance.dnsName.ToString());
                    }

                    if (instance.activations == null || instance.activations.Count == 0)
                    {                        
                        ActivationCode actCodes = new ActivationCode();
                        actCodes.activationID = instance.activationID;
                        actCodes.quantity = instance.quantity;
                        actCodes.nodeID = instance.hostID;
                        instance.activations.Add(actCodes);                       
                    }
                    
                    var newList = instance.activations
                        .GroupBy(x => new { x.activationID })
                        .Select(y => new ActivationCode()
                        {
                            activationID = y.Key.activationID,
                            quantity = Convert.ToString(y.Sum(f => Convert.ToInt64(f.quantity)))
                        }
                        ).ToList();

                    response = FulfillmentLogic.fulfillment(instance.activations, instance.nodeID, metaData, contextLog);
                    
                    return response;                 
                }

                jsonObject["errorType"] = "NullPointerException";
                return new LambdaResponse
                {
                    statusCode = HttpStatusCode.InternalServerError,
                    body = jsonObject.ToString()
                };
            } 
            catch (Exception ex)
            {
                contextLog.Logger.Log("setFulfillment -" + ex.Message);
                return FulfillmentLogic.fulfillmentMessage(402, "INVALID_REQUEST_PARAM", "Invalid input parameter", string.Empty);
            }
        }

        public LambdaResponse fulfillmentNew(object albProxyRequest, object context)
        {
            string clientType = string.Empty;
            ILambdaContext contextLog = (ILambdaContext)context;
            LambdaResponse response = new LambdaResponse();
            JObject jsonObject = new JObject();
            StringBuilder metaData = new StringBuilder();

            try
            {
                ApplicationLoadBalancerRequest request = (ApplicationLoadBalancerRequest)albProxyRequest;
                request.QueryStringParameters.TryGetValue("client-type", out clientType);

                if (string.IsNullOrEmpty(clientType))
                {
                    return response = new LambdaResponse
                    {
                        statusCode = HttpStatusCode.Unauthorized
                    };
                }

                if (request.Body != null)
                {
                    string json = request.Body;

                    RootObject instance = JsonConvert.DeserializeObject<RootObject>(json);

                    if (instance.hostDetails != null)
                    {
                        metaData.Append("hostDetails:" + instance.hostDetails.ToString());
                    }

                    if (!string.IsNullOrEmpty(instance.alias))
                    {
                        metaData.Append("alias:" + instance.alias.ToString());
                    }

                    if (!string.IsNullOrEmpty(instance.dnsName))
                    {
                        metaData.Append("dnsName:" + instance.dnsName.ToString());
                    }

                    if (instance.activations == null || instance.activations.Count == 0)
                    {                       
                        ActivationCode actCodes = new ActivationCode();
                        actCodes.activationID = instance.activationID;
                        actCodes.quantity = instance.quantity;
                        actCodes.nodeID = instance.hostID;
                        instance.activations.Add(actCodes);
                    }
                   
                    var newList = instance.activations
                        .GroupBy(x => new { x.activationID })
                        .Select(y => new ActivationCode()
                        {
                            activationID = y.Key.activationID,
                            quantity = Convert.ToString(y.Sum(f => Convert.ToInt64(f.quantity)))
                        }
                        ).ToList();

                    response = FulfillmentLogic.fulfillmentNew(instance.activations, instance.nodeID, metaData, contextLog);
                    
                    return response;
                }

                jsonObject["errorType"] = "NullPointerException";
                return new LambdaResponse
                {
                    statusCode = HttpStatusCode.InternalServerError,
                    body = jsonObject.ToString()
                };
            }
            catch (Exception ex)
            {
                contextLog.Logger.Log("setFulfillment -" + ex.Message);
                return FulfillmentLogic.fulfillmentMessage(402, "INVALID_REQUEST_PARAM", "Invalid input parameter", string.Empty);
            }
        }

        /// <summary>
        /// This function is for activating or syncing a subscribenet license.
        /// </summary>
        public LambdaResponse getSubscribeNetDetails(object albProxyRequest, object context)
        { 
            ILambdaContext contextLog = (ILambdaContext)context;
            LambdaResponse response = new LambdaResponse();
            JObject jsonObject = new JObject();
            
            try
            {
                ApplicationLoadBalancerRequest request = (ApplicationLoadBalancerRequest)albProxyRequest;
               
                if (request.Body != null)
                {
                    //contextLog.Logger.Log("SubscribeNet encrypt Body :" + request.Body);                  
                    byte[] data = Convert.FromBase64String(request.Body);
                    string decodedString = Encoding.GetEncoding("iso-8859-1").GetString(data);                   
                    
                    contextLog.Logger.Log("SubscribeNet decode Body :" + decodedString);
                    List<string> lines = new List<string>();
                    using (System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(data))
                    { 
                        lines = fileUploadRequestParser(streamBitmap, contextLog);                       
                    }

                    string displayString = string.Empty;

                    if (lines.Count > 0)
                    {
                        string fileContent = string.Empty;
                        fileContent = lines[0]; 
                        
                        for (int i=1; i<lines.Count; i++)
                        {
                         fileContent = fileContent + Environment.NewLine + lines[i];                            
                        }
                       
                        //contextLog.Logger.Log("SubscribeNet lines :" + fileContent);                                               
                        displayString = FulfillmentLogic.DecryptFNE(GenerateStreamFromString(fileContent), contextLog);
                        //contextLog.Logger.Log("displayString " + displayString);
                    }
                    
                    DecryptFNEFile objroot = new DecryptFNEFile();

                    if (!string.IsNullOrEmpty(displayString))
                    {
                        var deserialized = JsonConvert.DeserializeObject<DecryptFNEFile>(displayString);
                        objroot = deserialized;                       
                    }
                    else
                    {
                        return FulfillmentLogic.fulfillmentMessage(403, "INVALID_SIGNATURE", "Invalid signature", string.Empty);
                    }                                      
                  
                    //contextLog.Logger.Log("SubscribeNet request-  " + objroot.Data.FileContent);
                    
                    if (!string.IsNullOrEmpty(objroot.Data.FileContent))
                    { 
                        return  response = FulfillmentLogic.getSubscribeNetDetails(objroot.Data.FileContent, context);
                    }
                    else
                    {
                        return FulfillmentLogic.fulfillmentMessage(403, "INVALID_SIGNATURE", "Invalid signature", string.Empty);
                    }                  
                }
                else
                {
                    return FulfillmentLogic.fulfillmentMessage(402, "INVALID_REQUEST_PARAM", "Invalid input parameter", string.Empty);
                }
            }
            catch (Exception ex)
            {
                contextLog.Logger.Log("subscribeNet -" + ex.Message);
                return FulfillmentLogic.fulfillmentMessage(402, "INVALID_REQUEST_PARAM", "Invalid input parameter", string.Empty);
            }
        }

        /// <summary>
        /// This function is for parsing decrypted subscribenet request.
        /// </summary>
        private static List<string> fileUploadRequestParser(Stream stream, ILambdaContext contextLog)
        {           
            List<String> lstLines = new List<string>();
            StreamReader textReader = new StreamReader(stream, Encoding.GetEncoding("iso-8859-1"));
            string sLine = textReader.ReadLine();
            Regex regex = new Regex("(^-+)|(^content-)|(^$)|(^submit)", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline);
            //contextLog.Logger.Log("SubscribeNet textReader line :" + sLine);
            while (sLine != null)
            {
                if (!regex.Match(sLine).Success)
                {
                    lstLines.Add(sLine);
                }
                sLine = textReader.ReadLine();
            }

            textReader.Dispose();
            stream.Flush();
            stream.Dispose();
            return lstLines;
        }

        /// <summary>
        /// This function is for convert a string into stream.
        /// </summary>
        public static Stream GenerateStreamFromString(string s)
        {
            byte[] buffer = Encoding.GetEncoding("iso-8859-1").GetBytes(s);
            MemoryStream ms = new MemoryStream(buffer);
            return ms;           
        }

        /// <summary>
        /// This function is for syncing licenses per host.
        /// </summary>
        public async Task<LambdaResponse> SyncHost(object albProxyRequest, object context)
        {
            string clientType = string.Empty;
            ILambdaContext contextLog = (ILambdaContext)context;
            LambdaResponse response = new LambdaResponse();
            JObject jsonObject = new JObject();
            StringBuilder metaData = new StringBuilder();

            try
            {
                ApplicationLoadBalancerRequest request = (ApplicationLoadBalancerRequest)albProxyRequest;
                request.QueryStringParameters.TryGetValue("client-type", out clientType);

                if (string.IsNullOrEmpty(clientType) || !(clientType == "ILU" || clientType == "GENERIC"))
                {
                    return response = new LambdaResponse
                    {
                        statusCode = HttpStatusCode.Unauthorized
                    };
                }

                if (request.Body != null)
                {
                    string json = request.Body;                  

                    List<InstalledFulfillment> insFul = new List<InstalledFulfillment>();

                    RootObject instance = JsonConvert.DeserializeObject<RootObject>(json);

                    if (instance.hostDetails != null)
                    {
                        metaData.Append("hostDetails:" + instance.hostDetails.ToString());
                    }

                    if (!string.IsNullOrEmpty(instance.alias))
                    {
                        metaData.Append("alias:" + instance.alias.ToString());
                    }

                    if (!string.IsNullOrEmpty(instance.dnsName))
                    {
                        metaData.Append("dnsName:" + instance.dnsName.ToString());                        
                    }

                    if (instance.installedFulfillments != null)
                    {
                        insFul = instance.installedFulfillments;
                    }

                    if (string.IsNullOrEmpty(instance.forceFnpLicense))
                    {
                        instance.forceFnpLicense = "false";
                    }
                    
                    response = await FulfillmentLogic.SyncHost(insFul, instance.forceFnpLicense, instance.hostID, instance.nodeID, metaData, context);

                    return response;
                }
                else
                {
                    return FulfillmentLogic.fulfillmentMessage(402, "INVALID_REQUEST_PARAM", "Invalid input parameter", string.Empty);
                }
            }
            catch (Exception ex)
            {
                contextLog.Logger.Log("SyncHost -" + ex.Message);
                return FulfillmentLogic.fulfillmentMessage(402, "INVALID_REQUEST_PARAM", "Invalid input parameter", string.Empty);
            }
        }

        /// <summary>
        /// This function is for getting epoch time for misc api.
        /// </summary>
        public LambdaResponse getEpochTime(object albProxyRequest, object context)
        {
            JObject jsonObject = new JObject();
            jsonObject["success"] = "true";
            jsonObject["epoch"] = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            return new LambdaResponse
            {
                statusCode = HttpStatusCode.OK,
                body = jsonObject.ToString()
            };
        }

        public LambdaResponse getProductFeatures(object albProxyRequest, object context)
        {
            ILambdaContext contextLog = (ILambdaContext)context;
            JObject jsonObject = new JObject();
            ApplicationLoadBalancerRequest request = (ApplicationLoadBalancerRequest)albProxyRequest;
            string query = string.Empty;
            string partNumber = string.Empty;

            try
            {
              
               request.QueryStringParameters.TryGetValue("query", out query);
                if (!string.IsNullOrEmpty(query))
                {                    
                    byte[] data = Convert.FromBase64String(query.Replace("%3D", "=").Replace("%2B", "+").Replace("%2F", "/"));
                    string decodedString = Encoding.UTF8.GetString(data);
                    
                    if (!string.IsNullOrEmpty(decodedString))
                    {
                        RootObject instance = JsonConvert.DeserializeObject<RootObject>(decodedString);
                        partNumber = instance.partNumber;

                        if (!string.IsNullOrEmpty(partNumber))
                        {
                            return FulfillmentLogic.getProductFeatures(partNumber);
                        }
                    }
                }

                return new LambdaResponse
                {
                    statusCode = HttpStatusCode.OK,
                    body = jsonObject.ToString()
                };
            }
            catch (Exception ex)
            {
                contextLog.Logger.Log("getProductFeatures -" + ex.Message);
                return FulfillmentLogic.fulfillmentMessage(402, "INVALID_REQUEST_PARAM", "Invalid input parameter", string.Empty);
            }
        }

        public LambdaResponse getCustomerDetails(object albProxyRequest, object context)
        {
            ILambdaContext contextLog = (ILambdaContext)context;
            JObject jsonObject = new JObject();
            ApplicationLoadBalancerRequest request = (ApplicationLoadBalancerRequest)albProxyRequest;
            string query = string.Empty;
            string partNumber = string.Empty;
            JObject jAssets = new JObject();

            try
            {
                request.QueryStringParameters.TryGetValue("query", out query);
                JArray jsonOrderDetails = new JArray();

                if (!string.IsNullOrEmpty(query))
                {                    
                    byte[] data = Convert.FromBase64String(query.Replace("%3D","=").Replace("%2B", "+").Replace("%2F", "/"));
                    string decodedString = Encoding.UTF8.GetString(data);
                   
                    if (!string.IsNullOrEmpty(decodedString))
                    {
                        RootObject instance = JsonConvert.DeserializeObject<RootObject>(decodedString);

                        if (instance.assets.Count > 0)
                        {
                            foreach (AssetDetails assetDet in instance.assets)
                            {
                                if (assetDet.type.ToUpperInvariant() == "ACTIVATIONCODE" && !string.IsNullOrEmpty(assetDet.id))
                                {
                                    JObject orderDetails = new JObject();
                                    orderDetails = FulfillmentLogic.getCustomerDetails(assetDet.id);

                                    if (orderDetails != null)
                                    {
                                        jsonOrderDetails.Add(orderDetails);
                                    }
                                }
                            }                          

                            if (jsonOrderDetails != null)
                            {
                                jAssets["assets"] = jsonOrderDetails;
                                jAssets["success"] = "true";
                            }
                            else
                            {
                                jAssets["success"] = "false";
                            }
                        }
                        else
                        {
                            jAssets["success"] = "false";
                        }

                    }
                }

                return new LambdaResponse
                {
                    statusCode = HttpStatusCode.OK,
                    body = jAssets.ToString()
                };
            }
            catch (Exception ex)
            {
                contextLog.Logger.Log("getCustomerDetails -" + ex.Message);
                return FulfillmentLogic.fulfillmentMessage(402, "INVALID_REQUEST_PARAM", "Invalid input parameter", string.Empty);
            }
        }

        /// <summary>
        /// This function is for checking db connectivity.
        /// </summary>
        public LambdaResponse dbTesting(object albProxyRequest, object context)
        {
            LambdaResponse response;
            JObject jsonObject = new JObject();
            ApplicationLoadBalancerRequest request = (ApplicationLoadBalancerRequest)albProxyRequest; 
            var envVariable = Environment.GetEnvironmentVariable("DbConnection"); 
            response = FulfillmentLogic.devdbTesting(envVariable.ToString());
            return response; 
        }

        /// <summary>
        /// This function is for returning error in specified format.
        /// </summary>
        public LambdaResponse errorResponse(JObject jsonObject)
        {
            LambdaResponse response;
            return response = new LambdaResponse
            {
                statusCode = HttpStatusCode.BadRequest,
                body = jsonObject.ToString()
            };
        }        

        }
}
