﻿// -----------------------------------------------------------------------
// <copyright file="BusinessManagersException.cs" company="Keysight Technologies, Inc.">
//      Copyright (C) Keysight Technologies, Inc.
// </copyright>
// ---------------------------------------------------------------------
namespace Lambda.Managers
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A base class of an exception thrown from the BusinessManagers project
    /// </summary>
    [Serializable]
    public class BusinessManagersException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessManagersException"/> class.
        /// </summary>
        public BusinessManagersException() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessManagersException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public BusinessManagersException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessManagersException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public BusinessManagersException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessManagersException"/> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization info.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected BusinessManagersException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }
    }
}