﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Lambda.Entities;
using Newtonsoft.Json.Linq;
using DatabaseLayer;
using System.Text.RegularExpressions;
using System.Linq;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net.Http;
using System.IO;
using Amazon.Lambda.Core;
using System.Data;
using System.Threading.Tasks;

namespace Lambda.Logic
{
    public static class FulfillmentLogic
    {
        public static string DecryptUrl = Environment.GetEnvironmentVariable("DecryptUrl");

        public static LambdaResponse readActivationCode1(string activationCode, string orderNum, bool getHostInfo, object context, string clientType)
        {
            ILambdaContext contextLog = (ILambdaContext)context;
            LambdaResponse response;
            JObject jsonObject = new JObject();
            ReadEntitlement orderLine = new ReadEntitlement();
            JArray partsJSONArray = new JArray();
            JObject orderLineJsonObj = new JObject();

            if (!string.IsNullOrEmpty(activationCode))
            {
                try
                {
                    orderLine = OrderLineDb.FindOrderInfoByActivationCode(activationCode);
                }
                catch (Exception ex)
                {
                    orderLine = OrderLineDb.FindOrderInfoByActivationCode(activationCode);
                }

                if (orderLine == null)
                {
                    jsonObject["error"] = "Activation Code not found.";
                    return errorResponse(jsonObject);
                }
            }
            else if (!string.IsNullOrEmpty(orderNum))
            {
                try
                {
                    orderLine = OrderLineDb.FindOrderLineByOrderNumber(orderNum);
                }
                catch (Exception ex)
                {
                    orderLine = OrderLineDb.FindOrderLineByOrderNumber(orderNum);
                }

                if (orderLine == null)
                {
                    jsonObject["error"] = "Entitlement Id not found.";
                    return errorResponse(jsonObject);
                }
            }

            if (orderLine != null)
            {
                jsonObject["entitlementCode"] = orderLine.EntitlementCode.ToUpperInvariant();
                jsonObject["success"] = "true";
                orderLineJsonObj["sku"] = orderLine.ProdNum;
                orderLineJsonObj["activationCode"] = orderLine.ActivationCode.ToUpperInvariant();
                orderLineJsonObj["status"] = "ACTIVE";
                orderLineJsonObj["endDate"] = orderLine.EndDate;
                orderLineJsonObj["quantity"] = orderLine.QtyRedeemable - orderLine.QtyRedeemed;

                if (getHostInfo)
                {
                    LicenseHostCollection licCol = SyncLicenseDb.FindLicenseHost(orderLine.OrderId, orderLine.LineId);

                    JArray orderLineHostIDs = new JArray();

                    if (licCol.Count > 0)
                    {
                        foreach (LicenseHost license in licCol)
                        {
                            JObject hostIDJSON = new JObject();
                            hostIDJSON["hostID"] = license.HostId;
                            hostIDJSON["quantity"] = license.Qty;
                            orderLineHostIDs.Add(hostIDJSON);
                        }

                        orderLineJsonObj["hostIds"] = orderLineHostIDs;
                    }
                }
                partsJSONArray.Add(orderLineJsonObj);
                jsonObject["parts"] = partsJSONArray;
                jsonObject["entitlementCode"] = orderLine.EntitlementCode;
                jsonObject["accountName"] = orderLine.CompanyName;
                jsonObject["accountID"] = orderLine.OracleCustId;
                jsonObject["contactEmail"] = orderLine.CustomerEmail;
            }
            else
            {
                jsonObject["error"] = "order not found.";
                return errorResponse(jsonObject);
            }

            string encryptedResponse = EncryptString(jsonObject.ToString(), clientType);

            return response = new LambdaResponse
            {
                statusCode = HttpStatusCode.OK,
                body = encryptedResponse
            };
        }

        public static LambdaResponse readActivationCode(string activationCode, string orderNum, bool getHostInfo, object context, string clientType)
        {
            ILambdaContext contextLog = (ILambdaContext)context;
            LambdaResponse response;
            JObject jsonObject = new JObject();
            ReadEntitlement orderLine = new ReadEntitlement();            
            JArray partsJSONArray = new JArray();
            JObject orderLineJsonObj = new JObject();

            if (!string.IsNullOrEmpty(activationCode))
            {
                try
                {
                    orderLine = OrderLineDb.FindOrderInfoByActivationCode(activationCode);
                }
                catch (Exception ex)
                {                    
                    orderLine = OrderLineDb.FindOrderInfoByActivationCode(activationCode);
                }

                if (orderLine == null)
                {
                    response = fulfillmentMessage(100, "INVALID_ACTIVATION_CODE_PARAM", "Activation Code not found", string.Empty);
                    //return errorMsgReadAC(response.body); 
                    string encryt = EncryptString(response.body, clientType);
                    return response = new LambdaResponse
                    {
                        statusCode = HttpStatusCode.BadRequest,
                        body = encryt.ToString()
                    };
                }               
            }
            else if (!string.IsNullOrEmpty(orderNum))
            {
                try
                {
                    orderLine = OrderLineDb.FindOrderLineByOrderNumber(orderNum);
                }
                catch (Exception ex)
                {                    
                    orderLine = OrderLineDb.FindOrderLineByOrderNumber(orderNum);
                }

                if (orderLine == null)
                {
                    response = fulfillmentMessage(101, "INVALID_ENTITLEMENT_ID_PARAM", "Entitlement Id not found.", string.Empty);
                    // return errorMsgReadAC(response.body); 
                    string encryt = EncryptString(response.body, clientType);
                    return response = new LambdaResponse
                    {
                        statusCode = HttpStatusCode.BadRequest,
                        body = encryt.ToString()
                    };
                }                
            }

            if (orderLine != null)
            {
                jsonObject["entitlementCode"] = orderLine.EntitlementCode.ToUpperInvariant();
                jsonObject["success"] = true;
                orderLineJsonObj["sku"] = orderLine.ProdNum;
                orderLineJsonObj["activationCode"] = orderLine.ActivationCode.ToUpperInvariant();
                orderLineJsonObj["status"] = "ACTIVE";
                orderLineJsonObj["endDate"] = orderLine.EndDate;
                orderLineJsonObj["quantity"] = orderLine.QtyRedeemable - orderLine.QtyRedeemed;

                if (getHostInfo)
                {
                    LicenseHostCollection licCol = SyncLicenseDb.FindLicenseHost(orderLine.OrderId, orderLine.LineId);

                    JArray orderLineHostIDs = new JArray();

                    if (licCol.Count > 0)
                    {
                        foreach (LicenseHost license in licCol)
                        {
                            JObject hostIDJSON = new JObject();
                            hostIDJSON["hostID"] = license.HostId;
                            hostIDJSON["quantity"] = license.Qty;
                            orderLineHostIDs.Add(hostIDJSON);
                        }

                        orderLineJsonObj["hostIds"] = orderLineHostIDs;
                    }
                }
                partsJSONArray.Add(orderLineJsonObj);
                jsonObject["parts"] = partsJSONArray;
                jsonObject["entitlementCode"] = orderLine.EntitlementCode;
                jsonObject["accountName"] = orderLine.CompanyName;
                jsonObject["accountID"] = orderLine.OracleCustId;
                jsonObject["contactEmail"] = orderLine.CustomerEmail;
            }
            else
            {
                response = fulfillmentMessage(102, "ORDER_NOT_FOUND", "order not found.", string.Empty);
                // return errorMsgReadAC(response.body);
                string encryt = EncryptString(response.body, clientType);
                return response = new LambdaResponse
                {
                    statusCode = HttpStatusCode.BadRequest,
                    body = encryt.ToString()
                };
            }

            string encryptedResponse = EncryptString(jsonObject.ToString(), clientType);
           
            return response = new LambdaResponse
            {
                statusCode = HttpStatusCode.OK,
                body = encryptedResponse
            };
        }

        public static LambdaResponse errorMsgReadAC(string errorMsg)
        {
            string base64str = string.Empty;
            byte[] byt = System.Text.Encoding.UTF8.GetBytes(errorMsg);
            base64str = Convert.ToBase64String(byt);

            JObject jsonObject = new JObject();
            jsonObject["message"] = base64str;
            jsonObject["messageSignature"] = "BGAUkjDc2lVMUJpYHS63rLVE9N8y8G1FVaqphonVOQbLcKOULdGSpMdoTKa+Kn3dO/5v/zpE1BH+3K3P0xrZM7xeKvonyFoh7uksgyk7Ez42lGv/8pI7+6QxIOH6SAucCp/t1vjvJpleBpsXnP+iZhsPXnZfdfGex0/v8Z5V4a0F+OUtGLXfaK0NOODLq63dlRoeLDNcD13NlbjV1bX7SYqoXzrNO71zU20ksdi03262F1qMYrKfOVWzajSwGh0dY3EMhKZFcf9Ft8cXM/XTnZqdLn2uDbn9txEs0VO5ZCQ5e9FAG/Im5ZN3diVGMa+kW3jF0mpPhM+s1UD/Eunlnk1BRVxgo4o3ZgiOqSQzY2oL4HZ2OvpUIm1RewvLsGtcT4C8ZAcKH0vUa2nObK7o2DV2u7G8zA694h5xAfwdTorBA9OHxDQPxYkO85aPjnSCzt76rYmlXjQojWqxrpUt2gxkx1I6QHPFu+b9eU7xZuEqQhTTzSBbRpw8MAx0dbeub/lFJO/BSlD8LDELI/iDxQz7wiq08Q7f4bkf2XGRq1lA6xtJbWMLlon2HT6L+76wm87gsr3SUnxnwGjY1ctYaAChLE2vkx71FTTmcbPGkf6ttXrTV+izd8mvt+1OQS3cISKAsieFykJVcRS/5dcy6daUINOb9tKihjUeKRCNJbE=";
            jsonObject["key"] = "quV6hMkXIayOHBDU3gDgP02f1G4RSrlSKB8ug+X8jgew0LD4jrOzGDgKXxA0y1ql67j6AXEE75r7M8vBzK+01SS5OzuJSDKdL/QC5/8utCyiUJoXwqTOSu7ROlSk+OyDh7ECYYwjO0cXyG5iWcq4IasbD86SoL6JMcSbyzHA07cxzgQw0+GeKcvz0fQYmH9mZ95K3LElA6w7jgVwbNYofXbH5vXWCyJKFqGD3ou7gub8GQVxeUvIzsKMl3kTtpZgcOX/qY5EJLkhMrJtUjiYnY9iWfSLiIsXCbVPaY2Ak9TXu3S8C55xFA07h8qm6RKGsAEaLZ7Kw/TD3c78MeS1UhnerpcM3etnnZWtZuL5RhOZSZs7D4sje/IrP8IFrpSFfYKox94DZKPz0tMF6vsQju1tB5wX7CoAeCfvpyQmNE+dMhdpsUeAZnj3PDg9OprYrooLmpZDzqaRzBTVrXFd4KI14JRP3u1ii14us8wXx7ARLPPJSKkJ5x8ERnk7R1E6pLfWlk6tzOZ+6Rjf4wd2JqoNxd7VX9u3etK0pCYxfGXA90Mw4iS8OuuYgC4LbH8Id4BnN3QRX64ahjDaDw7HyWMP1Zh3KVyFljyUtGxzY8QxtEKqWWziIfylpaMjFDhQtTDF2zJhBYs3bIFtYbzhbNtosrDNDwv2kSgawgQi8tQ=";
            jsonObject["iv"] = "KyvmxQ54+vFkkdS5YQ0dwCRGKtq7EAN3dEyzNqOpOsyFTaeS+kAVAR4I5Kvu2RZBb2Dl2bW7x+zVWxYgCWOPCCfdxsmJISVF/AKMh5t9ks/1QLFewp9LgPMtlVFolIZAe29VaXHJbDBG+jK8HfiPKjCFBTu0E4dU2odr0pCjQuZdn5qMeKm0We2pBOq/4yRIK0YSWwXJ5134Gy1NACp5ZC2GoR5/ERYLxxNKxglesuqbCLcfzDcwNzuaw/Bh82lh30WBkgrdlnRmCiB+plIz2HyyhG9EMFODdzOiNNt1E2laHQfsH1MT/s9Ur7rHo040Srqy7uU+ZegYbzixce3SCiPX1Qb9l1JvUD6aMt1aI33d3yf0GpN0RjG42SLnOomQxIqXkpSj/OBwuFokIo2QykIpgOu9bjbqt9wYFiEZ8prnJiBEbK+E5EKU4zJVFmXUzhXYQmnKo0LZrg4o8EHxBXz1J2WGABymPngHgsmUBtZsAgeyOhtBrNi8SFrUW9o3logZTFwkv33EJL4cYlgUpwYqOD7cwp6t7JNdZMVOxR6YswhFz16ateAgjgKOqDzFxeOK+AAfcCHURouBroeQEXSvUmpysteb46sF5pBfM7ektUXFwyvrUrJ/zF3WyYFvbeNsBjJto/1rKqCTYYa7sX3+pcI2Ed76+u71iNctxyg=";
            LambdaResponse response;
            response = new LambdaResponse
            {
                statusCode = HttpStatusCode.BadRequest,
                body = jsonObject.ToString()
            };
            return response;

        }

        public static LambdaResponse getActivationsByEntitlementCode(string entitlementCode, bool returnAvailableQty)
        {
            LambdaResponse response;
            JObject jsonObject = new JObject();
            OrderLineCollection orderLineCol = new OrderLineCollection();
            Order order = new Order();
            JArray partsJSONArray = new JArray();
            JObject orderLineJsonObj = new JObject();

            orderLineCol = OrderLineDb.FindOrderLinesByEntitlementCode(entitlementCode);

            if (orderLineCol == null)
            {
                jsonObject["error"] = "No Activation Codes assigned on the received Entitlement Code.";
            }
            else
            {
                JObject activationsWithQty = new JObject();

                foreach (OrderLine orderLine in orderLineCol)
                {

                    JObject details = new JObject();
                    details["totalQty"] = orderLine.QtyOrdered;

                    if (returnAvailableQty)
                    {
                        details["availableQty"] = orderLine.QtyRedeemable;
                    }

                    activationsWithQty[orderLine.ActivationCode.ToUpperInvariant()] = details;
                }

                if (activationsWithQty.Count > 0)
                {
                    jsonObject["activations"] = activationsWithQty;
                }
                else
                {
                    jsonObject["error"] = "No Activation Codes assigned on the received Entitlement Code.";
                }

                return response = new LambdaResponse
                {
                    statusCode = HttpStatusCode.OK,
                    body = jsonObject.ToString()
                };
            }

            return response = new LambdaResponse
            {
                statusCode = HttpStatusCode.OK,
                body = jsonObject.ToString()
            };
        }

        public static OrderLine getOrderLineByActivationId(string activationId)
        {
            OrderLine orderLine = OrderLineDb.FindOrderLineByActivationCode(activationId);
            return orderLine;
        }

        public static Product getProductByProdId(long prodId)
        {
            Product product = ProductDb.FindProductByProdId(prodId);
            return product;
        }

        public static LambdaResponse getProductFeatures(string productNumber)
        {
            JObject jsonObject = new JObject();
            JArray jsonFeatures = new JArray();
            ProductFeaturesCollection features = null;
            try
            {
                features = ProductDb.FindProductFeaturesByProdNumber(productNumber);
            }
            catch (Exception ex)
            {
                features = ProductDb.FindProductFeaturesByProdNumber(productNumber);
            }

            if (features != null)
            {
                // String[] featurelist = features.Feature.Split(" ");

                foreach (ProductFeatures feature in features)
                {
                    JObject jFeature = new JObject();
                    jFeature["count"] = feature.Quantity;
                    jFeature["featureID"] = feature.Feature;
                    jsonFeatures.Add(jFeature);
                }

                jsonObject["features"] = jsonFeatures;
                jsonObject["success"] = "true";
                jsonObject["partNumber"] = productNumber;
            }
            else
            {
                jsonObject["success"] = "false";
            }

            return new LambdaResponse
            {
                statusCode = HttpStatusCode.OK,
                body = jsonObject.ToString()
            };
        }

        public static JObject getCustomerDetails(string activationCode)
        {
            Order order = null;
            try
            {
                order = OrderDb.FindOrderByActivationCode(activationCode);
            }
            catch (Exception ex)
            {
                order = OrderDb.FindOrderByActivationCode(activationCode);
            }

            OrderLine orderLine = OrderLineDb.FindOrderLineByActivationCode(activationCode);
            JObject jOrder = new JObject();

            if (order != null && orderLine != null)
            {
                jOrder["success"] = "true";
                jOrder["companyName"] = order.CompanyName;
                jOrder["id"] = orderLine.ActivationCode.ToUpperInvariant();
                jOrder["oracleCustomerId"] = order.CustomerAccountId;
                jOrder["ownerEmail"] = orderLine.CustomerEmail;
                return jOrder;
            }
            else
            {
                jOrder["success"] = "false";
                jOrder["errorMessage"] = "Activation Code not found.";
                jOrder["errorCode"] = 200;
                jOrder["id"] = activationCode.ToUpperInvariant();
                return jOrder;
            }
        }

        public static LambdaResponse getSubscribeNetDetails(string fileContent, object context)
        {
            ILambdaContext contextLog = (ILambdaContext)context;
            List<ActivationCode> lstActCodes = new List<ActivationCode>();

            SubscribeNet subscribeNet = new SubscribeNet();
            subscribeNet.RequestHostID = getSubstring(fileContent, "RequestHostID=", "\r\n");
            
            int count = Regex.Matches(fileContent, "ActivationID").Count;

            if (count > 0)
            {
                fileContent = fileContent.Replace("ActivationID=", "\r\nActivationID=");
                string desiredRightsSub = getSubstring(fileContent, "DesiredRights=(", ")");
                string desiredRights = desiredRightsSub.Replace("ActivationID=", "\r\nActivationID=") + "\r\n";
                for (int i = 0; i < count; i++)
                {
                    string qtyContent = getSubstring(desiredRights + "&", "ActivationID=", "\r\n");
                    subscribeNet.ActivationID = getSubstring(fileContent, "ActivationID=", ":");                    

                    if (!string.IsNullOrEmpty(qtyContent))
                    {
                        subscribeNet.Qty = getSubstring(qtyContent + "$", ":", "$");
                    }

                    if (!string.IsNullOrEmpty(subscribeNet.ActivationID) && !string.IsNullOrEmpty(subscribeNet.Qty))
                    {
                        ActivationCode actCode = new ActivationCode();
                        actCode.activationID = subscribeNet.ActivationID;
                        actCode.quantity = subscribeNet.Qty.Trim();
                        lstActCodes.Add(actCode);
                        fileContent = fileContent.Replace("ActivationID=" + qtyContent, "");
                        desiredRights = desiredRights.Replace("ActivationID=" + qtyContent, "");
                    }
                }

                string desiredRightsSub1 = getSubstring(fileContent, "IdentityName=IxVeriWaveLM\r\n", "RequestHostIDType=String");

                if (string.IsNullOrEmpty(desiredRightsSub1))
                {
                    desiredRightsSub1 = getSubstring(fileContent, "IdentityName=IxVeriWave\r\n", "RequestHostIDType=String");
                }

                if (!string.IsNullOrEmpty(desiredRightsSub1))
                {
                    fileContent = fileContent.Replace(desiredRightsSub1, "");
                }
                if (lstActCodes.Count > 0)
                {
                    return fulfillment(lstActCodes, subscribeNet.RequestHostID, new StringBuilder(fileContent), contextLog);
                }
                else
                {
                    JObject jsonObject = new JObject();
                    jsonObject["error"] = "Activation code not added for activation.";
                    return errorResponse(jsonObject);
                }
            }
            else
            {                
                return SyncHost(new List<InstalledFulfillment>(), "true", subscribeNet.RequestHostID, string.Empty, new StringBuilder(fileContent), contextLog).Result;
            }
        }

        public static LambdaResponse fulfillment(List<ActivationCode> lstActCodes, string nodeId, StringBuilder metaData, object context)
        {
            string hostId1 = string.Empty;
            int[] qtys = new int[lstActCodes.Count];
            string[] hostIds = new string[lstActCodes.Count];
            string[] actCodeList = new string[lstActCodes.Count];
            string[] requests = new string[lstActCodes.Count];
            int count = 0;
            ILambdaContext contextLog = (ILambdaContext)context;
            
            foreach (ActivationCode actCode in lstActCodes)
            {
                if (string.IsNullOrEmpty(actCode.activationID))
                {
                    return fulfillmentMessage(100, "INVALID_ACTIVATION_CODE_PARAM", "Invalid Activation Code parameter", string.Empty);
                }
                else if (!Regex.IsMatch(actCode.activationID, "^[0-9A-Za-z]{4}-[0-9A-Za-z]{4}-[0-9A-Za-z]{4}-[0-9A-Za-z]{4}$"))
                {
                    return fulfillmentMessage(114, "INVALID_ACTIVATIONS", "Invalid activations", actCode.activationID);
                }
                else if (string.IsNullOrEmpty(actCode.quantity) || !Regex.IsMatch(actCode.quantity, @"^\d+$"))
                {
                    return fulfillmentMessage(113, "INVALID_QUANTITY_PARAM", "Invalid quantity parameter", string.Empty);
                }
                else if (string.IsNullOrEmpty(actCode.nodeID) && string.IsNullOrEmpty(nodeId))
                {
                    return fulfillmentMessage(102, "INVALID_NODE_ID_PARAM", "Invalid nodeID parameter", "JSONObject[\"nodeID\"] not found");
                }
                else if (!string.IsNullOrEmpty(actCode.nodeID) || !string.IsNullOrEmpty(nodeId))
                {
                    hostId1 = string.IsNullOrEmpty(nodeId) ? actCode.nodeID : nodeId;
                    // hostId1 = hostId1.ToUpper();

                    qtys[count] = Convert.ToInt32(actCode.quantity);
                    actCodeList[count] = actCode.activationID.ToUpperInvariant();
                    requests[count] = "FULFILLMENT";
                    hostIds[count] = hostId1;
                    count++;               
            }
        }         

            string qId = "0";
            
            try
            {
                SyncLicenseDb.InsertInstalledFulfillmentForFulfillment(qtys, actCodeList, hostIds, requests, lstActCodes.Count);
            }
            catch (Exception ex)
            {
                SyncLicenseDb.InsertInstalledFulfillmentForFulfillment(qtys, actCodeList, hostIds, requests, lstActCodes.Count);
            }

            try
            {
                qId = SyncLicenseDb.ActivateLicenses(hostId1, metaData.ToString());                
            }
            catch (Exception ex)
            {
                return fulfillmentMessage(114, "INVALID_QID", "Invalid license service call", qId);
            }

            if (qId != "0" && !qId.Contains("ERROR"))
            {
                contextLog.Logger.Log("fulfillment qid" + qId);
                LicenseResponse.Rawlicense licenseResponse = new LicenseResponse.Rawlicense();
                licenseResponse = FulfillmentLogic.GetAPI(qId);               

                if (licenseResponse.License[0] == "empty")
                {
                    return new LambdaResponse
                    {
                        statusCode = HttpStatusCode.InternalServerError,
                        body = string.Empty
                    };
                }
                else
                {
                    //as for now
                    return new LambdaResponse
                    {
                        statusCode = HttpStatusCode.OK,
                        body = licenseResponse.License[0]
                    };
                }

            }
            else
            {                
                return fulfillmentMessage(114, "INVALID_QID", "Invalid license generation call", qId);
            }
        }

        public static LambdaResponse fulfillmentNew(List<ActivationCode> lstActCodes, string nodeId, StringBuilder metaData, object context)
        {
            string hostId1 = string.Empty;
            int[] qtys = new int[lstActCodes.Count];
            string[] hostIds = new string[lstActCodes.Count];
            string[] actCodeList = new string[lstActCodes.Count];
            string[] requests = new string[lstActCodes.Count];
            int count = 0;
            ILambdaContext contextLog = (ILambdaContext)context;
            
            foreach (ActivationCode actCode in lstActCodes)
            {
                if (string.IsNullOrEmpty(actCode.activationID))
                {
                    return fulfillmentMessage(100, "INVALID_ACTIVATION_CODE_PARAM", "Invalid Activation Code parameter", string.Empty);
                }
                else if (!Regex.IsMatch(actCode.activationID, "^[0-9A-Za-z]{4}-[0-9A-Za-z]{4}-[0-9A-Za-z]{4}-[0-9A-Za-z]{4}$"))
                {
                    return fulfillmentMessage(114, "INVALID_ACTIVATIONS", "Invalid activations", actCode.activationID);
                }
                else if (string.IsNullOrEmpty(actCode.quantity) || !Regex.IsMatch(actCode.quantity, @"^\d+$"))
                {
                    return fulfillmentMessage(113, "INVALID_QUANTITY_PARAM", "Invalid quantity parameter", string.Empty);
                }
                else if (string.IsNullOrEmpty(actCode.nodeID) && string.IsNullOrEmpty(nodeId))
                {
                    return fulfillmentMessage(102, "INVALID_NODE_ID_PARAM", "Invalid nodeID parameter", "JSONObject[\"nodeID\"] not found");
                }
                else if (!string.IsNullOrEmpty(actCode.nodeID) || !string.IsNullOrEmpty(nodeId))
                {
                    hostId1 = string.IsNullOrEmpty(nodeId) ? actCode.nodeID : nodeId;
                    // hostId1 = hostId1.ToUpper();

                    qtys[count] = Convert.ToInt32(actCode.quantity);
                    actCodeList[count] = actCode.activationID.ToUpperInvariant();
                    requests[count] = "FULFILLMENT";
                    hostIds[count] = hostId1;
                    count++;
                }
            }

            string qId = "0";

            try
            {
                SyncLicenseDb.InsertInstalledFulfillmentForFulfillment(qtys, actCodeList, hostIds, requests, lstActCodes.Count);
            }
            catch (Exception ex)
            {
                SyncLicenseDb.InsertInstalledFulfillmentForFulfillment(qtys, actCodeList, hostIds, requests, lstActCodes.Count);
            }

            try
            {
                DataSet dataset = SyncLicenseDb.ActivateLicensesNew(hostId1, metaData.ToString());

                DataTable objDt = new DataTable();
                if (dataset.Tables[0].Rows.Count > 0)
                {
                    objDt = dataset.Tables[0];                    

                    var strQId = (from r in objDt.AsEnumerable()
                                  select r["Q_ID"]).Distinct().FirstOrDefault();

                    qId = Convert.ToString(strQId);                    
                }
            }
            catch (Exception ex)
            {
                return fulfillmentMessage(114, "INVALID_QID", "Invalid license geneation call", qId);
            }

            if (qId != "0" && !qId.Contains("ERROR"))
            {
                contextLog.Logger.Log("fulfillmentNew qid" + qId);
                LicenseResponse.Rawlicense licenseResponse = new LicenseResponse.Rawlicense();
                licenseResponse = FulfillmentLogic.GetAPI(qId);
               
                if (licenseResponse.License[0] == "empty")
                {
                    return new LambdaResponse
                    {
                        statusCode = HttpStatusCode.InternalServerError,
                        body = string.Empty
                    };
                }
                else
                {
                    //as for now
                    return new LambdaResponse
                    {
                        statusCode = HttpStatusCode.OK,
                        body = licenseResponse.License[0]
                    };
                }

            }
            else
            {
                contextLog.Logger.Log("Activate -" + qId);
                return fulfillmentMessage(114, "INVALID_QID", "Invalid license generation call", qId);
            }
        }        

        public static async Task<LambdaResponse> SyncHost(List<InstalledFulfillment> installedFulfillments, string forceFnpLicense, string hostId, string nodeId, StringBuilder metaData, object context)
        {
            ILambdaContext contextLog = (ILambdaContext)context;         

            if (string.IsNullOrEmpty(hostId) && string.IsNullOrEmpty(nodeId))
            {
                return fulfillmentMessage(102, "INVALID_NODE_ID_PARAM", "Invalid nodeID parameter", "JSONObject[\"nodeID\"] not found");
            }
            else if (!string.IsNullOrEmpty(hostId) || !string.IsNullOrEmpty(nodeId))
            {
                string hostId1 = string.IsNullOrEmpty(nodeId) ? hostId : nodeId;
                //hostId1 = hostId1.ToUpper();
                SyncInsFulCollection syncInsFulCol = null;
                List<string> actCodeNotModified = new List<string>();
                List<string> actCodesAll = new List<string>();

                try
                {
                    syncInsFulCol = await SyncLicenseDb.FindLicensesSyncedAlready(hostId1);                    
                }
                catch (Exception ex)
                {                    
                    syncInsFulCol = await SyncLicenseDb.FindLicensesSyncedAlready(hostId1);
                }                

                if (syncInsFulCol.Count == 0)
                {
                    contextLog.Logger.Log("SyncHost syncInsFulCol.Count == 0");
                    LambdaResponse response;
                    return response = new LambdaResponse
                    {
                        body = string.Empty,
                        statusCode = HttpStatusCode.NotModified
                    };
                }
                else if (installedFulfillments.Count > 0 && forceFnpLicense == "false")
                {
                    foreach (InstalledFulfillment ins in installedFulfillments)
                    {
                        List<SyncInstalledFulfillment> synclicLst = new List<SyncInstalledFulfillment>();

                        try
                        {
                            synclicLst = (from lic in syncInsFulCol
                                          where lic.ActivationCode.ToUpperInvariant() == ins.activationCode.ToUpperInvariant()
                                          select lic).OrderByDescending(s => s.EndDate).ToList();
                        }
                        catch (Exception ex)
                        {

                        }

                        if (synclicLst.Count > 0)
                        {
                            DateTime? dtEndDate;

                            if (synclicLst[0].EndDate != null)
                            {
                                dtEndDate = Convert.ToDateTime(synclicLst[0].EndDate);
                            }
                            else
                            {
                                dtEndDate = Convert.ToDateTime(ins.maintenanceEndDate);
                            }

                            if (ins.expirationDate == "permanent")
                            {
                                ins.expirationDate = null;
                            }                          
                             
                            if (Convert.ToDateTime(ins.maintenanceEndDate) == dtEndDate &&
                               Convert.ToDateTime(ins.expirationDate) == Convert.ToDateTime(synclicLst[0].Expdate) &&
                               Convert.ToInt32(ins.quantity) == Convert.ToInt32(synclicLst[0].Qty))
                            {
                                actCodeNotModified.Add(ins.activationCode.ToUpperInvariant());                                
                            }
                        }
                    }                    

                   actCodesAll = syncInsFulCol.Select(s => s.ActivationCode).Distinct().ToList();                  

                    if ((actCodeNotModified.Count == installedFulfillments.Count && actCodesAll.Count == installedFulfillments.Count) || syncInsFulCol.Count == 0)
                    {
                        contextLog.Logger.Log("SyncHost actCodeNotModified.Count");
                        LambdaResponse response;
                        return response = new LambdaResponse
                        {
                            body = string.Empty,
                            statusCode = HttpStatusCode.NotModified
                        };
                    }
                    else
                    {
                        try
                        {
                            if (actCodeNotModified.Count > 0)
                            {
                                SyncLicenseDb.InsertInstalledFulfillmentForSync(actCodeNotModified, hostId1);
                            }

                            return SyncModifiedLicense(hostId1, metaData, contextLog);
                        }
                        catch (Exception ex)
                        {
                            if (actCodeNotModified.Count > 0)
                            {
                                SyncLicenseDb.InsertInstalledFulfillmentForSync(actCodeNotModified, hostId1);
                            }

                            return SyncModifiedLicense(hostId1, metaData, contextLog);
                        }
                    }
                }
                else
                {                    
                    return SyncModifiedLicense(hostId1, metaData, contextLog);
                }
            }
            else
            {
                contextLog.Logger.Log("SyncHost host id null");
                LambdaResponse response;
                return response = new LambdaResponse
                {
                    body = string.Empty,
                    statusCode = HttpStatusCode.NotModified
                };
            }
        }

        public static string sqlgetlicensefile()
        {
            return SyncLicenseDb.sqlgetlicensefile();
        }
        public static LambdaResponse SyncModifiedLicense(string hostId1, StringBuilder metaData, ILambdaContext contextLog)
        {
            contextLog.Logger.Log("Sync Modified case");

          string qId = "0";
            try
            {                
                qId = SyncLicenseDb.SyncLicenses(hostId1, metaData.ToString());                
            }
            catch (Exception ex)
            {
                contextLog.Logger.Log("Error exception" + ex.Message.ToString());
                try
                {
                    qId = SyncLicenseDb.SyncLicenses(hostId1, metaData.ToString());
                }
                catch (Exception ex1)
                {
                    return new LambdaResponse
                    {
                        statusCode = HttpStatusCode.BadRequest,
                        body = "Error while generating license"
                    };
                }
            }           

            if (qId != "0")
            {                
                LicenseResponse.Rawlicense licenseResponse = new LicenseResponse.Rawlicense();
                contextLog.Logger.Log("sync modified qid" + qId);
                licenseResponse = FulfillmentLogic.GetAPI(qId);
                if (licenseResponse.License.Count > 0)
                {
                    if (licenseResponse.License[0] == "empty")
                    {
                        contextLog.Logger.Log("sync modified licenseResponse.License[0]" + licenseResponse.License[0]);
                        return new LambdaResponse
                        {
                            statusCode = HttpStatusCode.NotModified,
                            body = string.Empty
                        };
                    }
                    else
                    {
                        //as for now
                        return new LambdaResponse
                        {
                            statusCode = HttpStatusCode.OK,
                            body = licenseResponse.License[0]
                        };
                    }
                }
                else
                {
                    return fulfillmentMessage(204, "License not found", "Calling license service failed", string.Empty);
                }

            }
            else
            {
                LambdaResponse response;
                return response = new LambdaResponse
                {
                    body = string.Empty,
                    statusCode = HttpStatusCode.NotModified
                };
            }
        }
        
        public static LambdaResponse fulfillmentMessage(int errorCode, string type, string message, string details)
        {
            JObject jsonObject = new JObject();
            JObject jsonDetails = new JObject();
            if (!string.IsNullOrEmpty(details))
            {
                jsonObject["details"] = details;
            }

            if (errorCode > 0)
            {
                jsonDetails["errorCode"] = errorCode;
            }

            if (!string.IsNullOrEmpty(type))
            {
                jsonDetails["type"] = type;
            }

            if (!string.IsNullOrEmpty(message))
            {
                jsonDetails["message"] = message;
            }

            jsonObject["error"] = jsonDetails;

            LambdaResponse response;
            response = new LambdaResponse
            {
                statusCode = HttpStatusCode.BadRequest,
                body = jsonObject.ToString()
            };

            return response;
        }

        public static LambdaResponse devdbTesting(string connection)
        {
            LambdaResponse response;
            JObject jsonObject = new JObject();
            string test = Test.checkConnection(connection);
            jsonObject["dbConnection"] = connection;
            jsonObject["result"] = test;
            //jsonObject["envVari"] = DecryptUrl;
            return response = new LambdaResponse
            {
                statusCode = HttpStatusCode.OK,
                body = jsonObject.ToString()
            };
        }

        public static void InitializeConnection(string connStr)
        {
            ConfigSettings.Connection = connStr;
        }

        public static LambdaResponse errorResponse(JObject jsonObject)
        {
            LambdaResponse response;
            return response = new LambdaResponse
            {
                statusCode = HttpStatusCode.BadRequest,
                body = jsonObject.ToString()
            };
        }

        public static string GetConnection()
        {
            return ConfigSettings.Connection;
        }

        public static DecryptResponse.StringContent DecryptString(string encodestr, object context, string clientType)
        {
            ILambdaContext contextLog = (ILambdaContext)context;
           // contextLog.Logger.Log("encodestr" + encodestr);
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("StringContent", encodestr);

            if (clientType == "ecommerce")
            {
                parameters.Add("StringType", clientType);
            }
            else
            {
                parameters.Add("StringType", "");
            }

            HttpClient client = new HttpClient();         
            MultipartFormDataContent form = new MultipartFormDataContent();
            HttpContent DictionaryItems = new FormUrlEncodedContent(parameters);
            form.Add(DictionaryItems, "StringFormat");
            HttpResponseMessage response = null;

          //  contextLog.Logger.Log("urlpath" + DecryptUrl + "DecodeString");

            try
            {
                response = (client.PostAsync(DecryptUrl + "DecodeString", form)).Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            var k = response.Content.ReadAsStringAsync().Result;

            //contextLog.Logger.Log("k value" + k);
            DecryptResponse.RootObject decryptString = new DecryptResponse.RootObject();
            DecryptResponse.StringContent stringCon = new DecryptResponse.StringContent();

            decryptString = JsonConvert.DeserializeObject<DecryptResponse.RootObject>(k);

            stringCon = JsonConvert.DeserializeObject<DecryptResponse.StringContent>(decryptString.data.StringContent);

            return stringCon;
        }

        public static string DecryptFNE(Stream encodestr,object context)
        {
            ILambdaContext contextLog = (ILambdaContext)context;
           // contextLog.Logger.Log("RequestFNE path" + DecryptUrl + "DecodeFile");
            Dictionary<string, string> parameters = new Dictionary<string, string>();

            parameters.Add("FileType", "FNE");

            HttpClient client = new HttpClient();

            MultipartFormDataContent form = new MultipartFormDataContent();
            HttpContent content = new StringContent("fileToUpload");
            HttpContent dictionaryItems = new FormUrlEncodedContent(parameters);
            form.Add(content, "fileToUpload");
            form.Add(dictionaryItems, "FileFormat");

            //content = new StreamContent(new MemoryStream(buffer));

            content = new StreamContent(encodestr);          
            content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            {
                FileName = "subscribenet.bin"               
            };

            form.Add(content);

            HttpResponseMessage response = null;

            try
            {
                response = client.PostAsync(DecryptUrl + "DecodeFile", form).Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            var k = response.Content.ReadAsStringAsync().Result;        

            return Convert.ToString(k);
        }

        public static string EncryptString(string json, string clientType)
        {
            byte[] byt = System.Text.Encoding.ASCII.GetBytes(json);
            string base64str = Convert.ToBase64String(byt);

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("StringContent", base64str);            

            if (clientType == "ecommerce")
            {
                parameters.Add("StringType", clientType);
            }
            else
            {
                parameters.Add("StringType", "");
            }

            HttpClient client = new HttpClient();
            MultipartFormDataContent form = new MultipartFormDataContent();
            HttpContent DictionaryItems = new FormUrlEncodedContent(parameters);
            form.Add(DictionaryItems, "StringFormat");
            HttpResponseMessage response = null;

            try
            {
                response = (client.PostAsync(DecryptUrl + "EncodeString", form)).Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            var k = response.Content.ReadAsStringAsync().Result;

            DecryptResponse.RootObject encryptString = new DecryptResponse.RootObject();
            encryptString = JsonConvert.DeserializeObject<DecryptResponse.RootObject>(k);           

            return encryptString.data.StringContent;
        }

        public static LicenseResponse.Rawlicense GetAPI(string QID)
        {
            List<string> objLicense = new List<string>();
            List<string> objLicenseType = new List<string>();
            LicenseResponse.Rawlicense objRawlicense = new LicenseResponse.Rawlicense();

            // string Baseurl = "http://ksmdb1t.cos.is.keysight.com:8080/api/License/";
            string Baseurl = Environment.GetEnvironmentVariable("LicenseUrl");
            string urlParameters = "?id=" + QID;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync(urlParameters).Result;
                if (response.IsSuccessStatusCode)
                {
                    var dataObjects = response.Content.ReadAsStringAsync().Result;
                    objRawlicense = JsonConvert.DeserializeObject<LicenseResponse.Rawlicense>(dataObjects);
                }
                else
                {
                    objRawlicense.License = objLicense;
                    objRawlicense.LicenseType = objLicenseType;
                    objRawlicense.Status = Convert.ToString(Convert.ToInt32(response.StatusCode));
                    objRawlicense.StatusMessage = response.ReasonPhrase;
                    //Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                }
            }
            return objRawlicense;
        }

        public static string getSubstring(string fileContent, string startString, string endString)
        {
            if (fileContent.IndexOf(startString) > 0)
            {

                int startIndex = fileContent.IndexOf(startString) + startString.Length;
                int endIndex = fileContent.IndexOf(endString, startIndex);

                return fileContent.Substring(startIndex, endIndex - startIndex);
            }

            return string.Empty;
        }

    }
}
