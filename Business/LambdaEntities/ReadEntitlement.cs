﻿//-----------------------------------------------------------------------
// <copyright file="ReadEntitlement.cs" company="Keysight Technologies, Inc.">
//      Copyright (C) Keysight Technologies, Inc.
// </copyright>
//----------------------------------------------------------------------
namespace Lambda.Entities
{
    using System;

    /// <summary>
    /// All the properties of a KSM ReadEntitlement that are stored in the license.order_line database
    /// </summary>
    [Serializable]
    public class ReadEntitlement : BusinessEntityBase
    {
        public ReadEntitlement()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReadEntitlement"/> class.
        /// </summary>
        /// <param name="entitlementCode">The entitlementCode for that activation code</param>
        /// <param name="prodNum">The product number</param>
        /// <param name="endDate">The end date of order line.</param>
        /// <param name="activationCode">The activation code.</param>      
        /// <param name="qtyRedeemable">The qty redeemable.</param>    
        /// <param name="qtyRedeemed">The qty redeemed.</param>    
        /// <param name="lineId">The line id of order line.</param>    
        /// <param name="orderId">The orderId of order line.</param>  
        /// <param name="companyName">The company name for that activation code</param>
        /// <param name="customerEmail">The customer email address</param>
        /// <param name="oracleCustId">The parent account Id.</param>
        public ReadEntitlement(
            string entitlementCode,
            string prodNum,
            string endDate,
            string activationCode,
            long qtyRedeemable,
            long qtyRedeemed,
            long lineId,
            string orderId,            
            string companyName,
            string customerEmail,
            string oracleCustId
            )
        {
            if (activationCode == null)
            {
                throw new ArgumentNullException("activationCode", "activationCode must be specified for ReadEntitlements.");
            }

            this.EntitlementCode = entitlementCode;
            this.CompanyName = companyName;
            this.ActivationCode = activationCode;
            this.CustomerEmail = customerEmail;
            this.OracleCustId = oracleCustId;
            this.ProdNum = prodNum;
            this.EndDate = endDate;
            this.QtyRedeemable = qtyRedeemable;
            this.QtyRedeemed = qtyRedeemed;
            this.LineId = lineId;
            this.OrderId = orderId;
        }

        /// <summary>
        /// Gets the EntitlementCode with this ReadEntitlement.
        /// </summary>
        public string EntitlementCode { get; private set; }

        /// <summary>
        /// Gets the company name with this OrderLine.
        /// </summary>
        public string CompanyName { get; private set; }

        /// <summary>
        /// Gets the ActivationCode from theOrderLine table
        /// </summary> 
        public string ActivationCode { get; private set; }

        /// <summary>
        /// Gets the oracle customer id from OrderLine table
        /// </summary> 
        public string OracleCustId { get; private set; }

        /// <summary>
        /// Gets the customer email from OrderLine table
        /// </summary> 
        public string CustomerEmail { get; private set; }

        /// <summary>
        /// Gets the product number from ReadEntitlement table
        /// </summary> 
        public string ProdNum { get; private set; }

        /// <summary>
        /// Gets the order id from ReadEntitlement table
        /// </summary> 
        public string OrderId { get; private set; }

        /// <summary>
        /// Gets the qty redeemable from ReadEntitlement table
        /// </summary> 
        public long QtyRedeemable { get; private set; }

        /// <summary>
        /// Gets the qty redeemed from ReadEntitlement table
        /// </summary> 
        public long QtyRedeemed { get; private set; }

        /// <summary>
        /// Gets the line id from ReadEntitlement table
        /// </summary> 
        public long LineId { get; private set; }

        /// <summary>
        /// Gets the qty redeemed from ReadEntitlement table
        /// </summary> 
        public string EndDate { get; private set; }
    }
}
