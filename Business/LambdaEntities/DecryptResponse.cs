﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lambda.Entities
{
    public class DecryptResponse
    {
        public class StringContent
        {
            public string getHostInfo { get; set; }
            public string activationCode { get; set; }
            public string entitlementID { get; set; }
        }

        public class Data
        {
            public string StringContent { get; set; }
            public string Status { get; set; }
            public string StatusMessage { get; set; }
        }

        public class RootObject
        {
            public Data data { get; set; }
        }
    }
}
