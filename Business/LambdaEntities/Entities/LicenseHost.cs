﻿//-----------------------------------------------------------------------
// <copyright file="LicenseHost.cs" company="Keysight Technologies, Inc.">
//      Copyright (C) Keysight Technologies, Inc.
// </copyright>
//----------------------------------------------------------------------
namespace Lambda.Entities
{
    using System;

    /// <summary>
    /// All the properties of a KSM SyncInstalledFulfillment that are stored in the asl_schema.SyncInstalledFulfillment database
    /// </summary>
    [Serializable]
    public class LicenseHost : BusinessEntityBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseHost"/> class.
        /// </summary>
        /// <param name="qty">The license qty</param>
        /// <param name="hostId">The host id for the license</param>
        /// <param name="lineId">The line id for the license.</param>
        /// <param name="orderId">The order id of the license.</param>      
        public LicenseHost(
            uint qty,
            string hostId,
            string orderId,
            string lineId)
        {
            if (hostId == null)
            {
                throw new ArgumentNullException("hostId", "hostId must be specified for SyncInstalledFulfillments.");
            }

            this.Qty = qty;
            this.HostId = hostId;
            this.OrderId = orderId;
            this.LineId = lineId;
        }

        /// <summary>
        /// Gets the Qty associated with this SyncInstalledFulfillment.
        /// </summary>
        public uint Qty { get; private set; }

        /// <summary>
        /// Gets the Host id
        /// </summary> 
        public string HostId { get; private set; }

        /// <summary>
        /// Gets the Expiration date from SyncInstalledFulfillment table
        /// </summary> 
        public string OrderId { get; private set; }

        /// <summary>
        /// Gets the EndDate from SyncInstalledFulfillment table
        /// </summary> 
        public string LineId { get; private set; }
    }
}
