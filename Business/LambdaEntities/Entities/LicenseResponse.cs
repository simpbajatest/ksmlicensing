﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lambda.Entities
{
    public class LicenseResponse
    {
        public class Rawlicense
        {
            public List<string> License { get; set; }
            public List<string> LicenseType { get; set; }
            public string Status { get; set; }
            public string StatusMessage { get; set; }
        }

        public class RootObject
        {
            public Rawlicense data { get; set; }
        }
    }
}
