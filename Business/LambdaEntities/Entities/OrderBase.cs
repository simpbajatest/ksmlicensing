﻿using System;

namespace Lambda.Entities
{
    /// <summary>
    /// This is the base class for all orders.
    /// Currently the order could be from an Oracle order (Orders table)
    /// or from a Siebel agreement (Agreemeent table)
    /// Only fields that are shared between them should appear in this class.
    /// Others should appear in derived classes.  Thanks!
    /// </summary>
    public abstract class OrderBase : BusinessEntityBase
    {
        public OrderBase()
        {

        }

        #region Contructors

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderBase"/> class.
        /// </summary>
        /// <param name="orderIdBase">The ID for this <see cref="OrderBase"/></param>
        /// <param name="certificateId">The certificate number (Example: TTF6-Z8JU).</param>
        /// <param name="vendorOrderNum">The vendor order num.</param>
        /// <param name="status">The status.</param>
        /// <param name="lastModified">The last modified.</param>
        /// <param name="insertedAt">The inserted at.</param>
        /// <param name="companyName">Name of the company.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="firstName">The first name.</param>
        /// <param name="department">The department of the address for this order.</param>
        /// <param name="streetAddressLineOne">The first street address line of the address for this order.</param>
        /// <param name="streetAddressLineTwo">The second street address line of the address for this order.</param>
        /// <param name="city">The city of the address for this order.</param>
        /// <param name="state">The state of the address for this order.</param>
        /// <param name="postalCode">The postal code of the address for this order.</param>
        /// <param name="countryCode">The country code of the address for this order.</param>
        /// <param name="telephone">The telephone number for this order.</param>
        /// <param name="email">The email address for this order.</param>
        /// <param name="purchaseOrder">The purchase order number for this order.</param>
        /// <param name="certificateType">The certificate type for this order.</param>
        /// <param name="orderType">The order type for this order.</param>
        /// <param name="orderSource">The order source for this order.</param>  
        protected OrderBase(
            string orderIdBase,
            string certificateId,
            string vendorOrderNum,
            string status,
            DateTime lastModified,
            DateTime insertedAt,
            string companyName,
            string lastName,
            string firstName,
            string department,
            string streetAddressLineOne,
            string streetAddressLineTwo,
            string city,
            string state,
            string postalCode,
            string countryCode,
            string telephone,
            string email,
            string purchaseOrder,
            string certificateType,
            string orderType,
            string orderSource)
        {
            this.OrderIdBase = orderIdBase;
            this.CertificateId = certificateId;
            this.VendorOrderNum = vendorOrderNum;
            this.Status = status == null ? null : DataConversionUtility.StringToUpper(status);
            this.LastModified = lastModified;
            this.InsertedAt = insertedAt;
            this.CompanyName = companyName;
            this.LastName = lastName;
            this.FirstName = firstName;
            this.Department = department;
            this.StreetAddressLineOne = streetAddressLineOne;
            this.StreetAddressLineTwo = streetAddressLineTwo;
            this.City = city;
            this.State = state;
            this.PostalCode = postalCode;
            this.CountryCode = countryCode;
            this.Telephone = telephone;
            this.Email = email;
            this.PurchaseOrder = purchaseOrder;
            this.CertificateType = certificateType;
            this.OrderType = orderType;
            this.OrderSource = orderSource;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderBase"/> class.
        /// </summary>
        /// <param name="orderIdBase">The ID for this <see cref="OrderBase"/></param>
        /// <param name="certificateId">The certificate number (Example: TTF6-Z8JU).</param>
        /// <param name="vendorOrderNum">The vendor order num.</param>
        /// <param name="status">The status.</param>
        /// <param name="lastModified">The last modified.</param>
        /// <param name="insertedAt">The inserted at.</param>
        /// <param name="companyName">Name of the company.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="firstName">The first name.</param>
        /// <param name="department">The department of the address for this order.</param>
        /// <param name="streetAddressLineOne">The first street address line of the address for this order.</param>
        /// <param name="streetAddressLineTwo">The second street address line of the address for this order.</param>
        /// <param name="city">The city of the address for this order.</param>
        /// <param name="state">The state of the address for this order.</param>
        /// <param name="postalCode">The postal code of the address for this order.</param>
        /// <param name="countryCode">The country code of the address for this order.</param>
        /// <param name="telephone">The telephone number for this order.</param>
        /// <param name="email">The email address for this order.</param>
        /// <param name="purchaseOrder">The purchase order number for this order.</param>
        /// <param name="certificateType">The certificate type for this order.</param>
        /// <param name="orderType">The order type for this order.</param>
        /// <param name="orderSource">The order source for this order.</param>
        /// <param name="endCustomerCompany">The end customer company for this order.</param>
        /// <param name="endCustomerFirstName">The end customer first name for this order.</param>
        /// <param name="endCustomerLastName">The end customer last name for this order.</param>
        /// <param name="endCustomerAddress1">The end customer address1 for this order.</param>
        /// <param name="endCustomerAddress2">The end customer address2 for this order.</param>      
        /// <param name="endCustomerCity">The end customer city for this order.</param>
        /// <param name="endCustomerState">The end customer state for this order.</param>
        /// <param name="endCustomerCountry">The end customer country for this order.</param>
        /// <param name="endCustomerZipcode">The end customer zip code for this order.</param>
        /// <param name="entitlementCode">The entitlement code for this order.</param>
        /// <param name="customerAccountId">The customer account id for this order.</param>          
        /// <param name="creatorEmail">Creator email of the order</param>
        /// <param name="licenseReason">The license reason for this order.</param>   
        /// <param name="createdAt">added created to change the signature</param>
        protected OrderBase(
            string orderIdBase,
            string certificateId,
            string vendorOrderNum,
            string status,
            DateTime lastModified,
            DateTime insertedAt,
            string companyName,
            string lastName,
            string firstName,
            string department,
            string streetAddressLineOne,
            string streetAddressLineTwo,
            string city,
            string state,
            string postalCode,
            string countryCode,
            string telephone,
            string email,
            string purchaseOrder,
            string certificateType,
            string orderType,
            string orderSource,
            string endCustomerCompany,
            string endCustomerFirstName,
            string endCustomerLastName,
            string endCustomerAddress1,
            string endCustomerAddress2,
            string endCustomerCity,
            string endCustomerState,
            string endCustomerCountry,
            string endCustomerZipcode,
            string entitlementCode,
            string customerAccountId,
            string creatorEmail,
            string licenseReason,
            DateTime createdAt)
        {
            this.OrderIdBase = orderIdBase;
            this.CertificateId = certificateId;
            this.VendorOrderNum = vendorOrderNum;
            this.Status = status == null ? null : DataConversionUtility.StringToUpper(status);
            this.LastModified = lastModified;
            this.InsertedAt = insertedAt;
            this.CompanyName = companyName;
            this.LastName = lastName;
            this.FirstName = firstName;
            this.Department = department;
            this.StreetAddressLineOne = streetAddressLineOne;
            this.StreetAddressLineTwo = streetAddressLineTwo;
            this.City = city;
            this.State = state;
            this.PostalCode = postalCode;
            this.CountryCode = countryCode;
            this.Telephone = telephone;
            this.Email = email;
            this.PurchaseOrder = purchaseOrder;
            this.CertificateType = certificateType;
            this.OrderType = orderType;
            this.OrderSource = orderSource;
            this.EndCustomerCompany = endCustomerCompany;
            this.EndCustomerFirstName = endCustomerFirstName;
            this.EndCustomerLastName = endCustomerLastName;
            this.EndCustomerAddress1 = endCustomerAddress1;
            this.EndCustomerAddress2 = endCustomerAddress2;
            this.EndCustomerCity = endCustomerCity;
            this.EndCustomerState = endCustomerState;
            this.EndCustomerCountry = endCustomerCountry;
            this.EndCustomerZipcode = endCustomerZipcode;
            this.EntitlementCode = entitlementCode;
            this.CustomerAccountId = customerAccountId;
            this.CreatorEmail = creatorEmail;
            this.LicenseReason = licenseReason;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderBase"/> class.
        /// </summary>
        /// <param name="orderIdBase">The ID for this <see cref="OrderBase"/></param>
        /// <param name="certificateId">The certificate number (Example: TTF6-Z8JU).</param>
        /// <param name="vendorOrderNum">The vendor order num.</param>
        /// <param name="status">The status.</param>
        /// <param name="lastModified">The last modified.</param>
        /// <param name="insertedAt">The inserted at.</param>
        /// <param name="companyName">Name of the company.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="firstName">The first name.</param>
        /// <param name="department">The department of the address for this order.</param>
        /// <param name="streetAddressLineOne">The first street address line of the address for this order.</param>
        /// <param name="streetAddressLineTwo">The second street address line of the address for this order.</param>
        /// <param name="city">The city of the address for this order.</param>
        /// <param name="state">The state of the address for this order.</param>
        /// <param name="postalCode">The postal code of the address for this order.</param>
        /// <param name="countryCode">The country code of the address for this order.</param>
        /// <param name="telephone">The telephone number for this order.</param>
        /// <param name="email">The email address for this order.</param>
        /// <param name="purchaseOrder">The purchase order number for this order.</param>
        /// <param name="certificateType">The certificate type for this order.</param>
        /// <param name="orderType">The order type for this order.</param>
        /// <param name="orderSource">The order source for this order.</param>
        /// <param name="endCustomerCompany">The end customer company for this order.</param>
        /// <param name="endCustomerFirstName">The end customer first name for this order.</param>
        /// <param name="endCustomerLastName">The end customer last name for this order.</param>
        /// <param name="endCustomerAddress1">The end customer address1 for this order.</param>
        /// <param name="endCustomerAddress2">The end customer address2 for this order.</param>      
        /// <param name="endCustomerCity">The end customer city for this order.</param>
        /// <param name="endCustomerState">The end customer state for this order.</param>
        /// <param name="endCustomerCountry">The end customer country for this order.</param>
        /// <param name="endCustomerZipcode">The end customer zip code for this order.</param>
        /// <param name="entitlementCode">The entitlement code for this order.</param>
        /// <param name="customerAccountId">The customer account id for this order.</param>        
        protected OrderBase(
            string orderIdBase,
            string certificateId,
            string vendorOrderNum,
            string status,
            DateTime lastModified,
            DateTime insertedAt,
            string companyName,
            string lastName,
            string firstName,
            string department,
            string streetAddressLineOne,
            string streetAddressLineTwo,
            string city,
            string state,
            string postalCode,
            string countryCode,
            string telephone,
            string email,
            string purchaseOrder,
            string certificateType,
            string orderType,
            string orderSource,
           string endCustomerCompany,
            string endCustomerFirstName,
            string endCustomerLastName,
            string endCustomerAddress1,
            string endCustomerAddress2,
            string endCustomerCity,
            string endCustomerState,
            string endCustomerCountry,
            string endCustomerZipcode,
            string entitlementCode,
            string customerAccountId,
            string creatorEmail)
        {
            this.OrderIdBase = orderIdBase;
            this.CertificateId = certificateId;
            this.VendorOrderNum = vendorOrderNum;
            this.Status = status == null ? null : DataConversionUtility.StringToUpper(status);
            this.LastModified = lastModified;
            this.InsertedAt = insertedAt;
            this.CompanyName = companyName;
            this.LastName = lastName;
            this.FirstName = firstName;
            this.Department = department;
            this.StreetAddressLineOne = streetAddressLineOne;
            this.StreetAddressLineTwo = streetAddressLineTwo;
            this.City = city;
            this.State = state;
            this.PostalCode = postalCode;
            this.CountryCode = countryCode;
            this.Telephone = telephone;
            this.Email = email;
            this.PurchaseOrder = purchaseOrder;
            this.CertificateType = certificateType;
            this.OrderType = orderType;
            this.OrderSource = orderSource;
            this.EndCustomerCompany = endCustomerCompany;
            this.EndCustomerFirstName = endCustomerFirstName;
            this.EndCustomerLastName = endCustomerLastName;
            this.EndCustomerAddress1 = endCustomerAddress1;
            this.EndCustomerAddress2 = endCustomerAddress2;
            this.EndCustomerCity = endCustomerCity;
            this.EndCustomerState = endCustomerState;
            this.EndCustomerCountry = endCustomerCountry;
            this.EndCustomerZipcode = endCustomerZipcode;
            this.EntitlementCode = entitlementCode;
            this.CustomerAccountId = customerAccountId;
            this.CreatorEmail = creatorEmail;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the order type for this order.
        /// </summary>
        public string OrderType { get; protected set; }

            /// <summary>
            /// Gets or sets the certificate number (Example: TTF6-Z8JU).
            /// </summary>
            public string CertificateId { get; set; }

            /// <summary>
            /// Gets or sets the certificate type for this order.
            /// </summary>
            public string CertificateType { get; protected set; }

            /// <summary>
            /// Gets or sets the order number
            /// </summary>
            public string VendorOrderNum { get; protected internal set; }

            /// <summary>
            /// Gets or sets the purchase order number for this order.
            /// </summary>
            public string PurchaseOrder { get; protected set; }

            /// <summary>
            /// Gets or sets the status of the order, not the record.  
            /// </summary>
            /// <remarks>This field is not written back to the database.</remarks>
            public string Status { get; set; }

            /// <summary>
            /// Gets or sets the order source for this order.
            /// </summary>
            public string OrderSource { get; protected set; }

            /// <summary>
            /// Gets or sets the sieble Last_modified DateTime.  (DateTime.Min for Oracle Orders)
            /// </summary>
            public DateTime LastModified { get; protected set; }

            /// <summary>
            /// Gets or sets the Inserted_at DateTime
            /// </summary>
            public DateTime InsertedAt { get; protected set; }

            /// <summary>
            /// Gets the formatted order date.
            /// </summary>
            public string FormattedOrderDate
            {
                get
                {
                    return this.InsertedAt.ToString("dd-MMM-yyyy");
                }
            }

            /// <summary>
            /// Gets or sets the company the order is for.
            /// </summary>
            /// <value>The name of the company.</value>
            public string CompanyName { get; protected set; }

            /// <summary>
            /// Gets or sets the customer's last name.
            /// </summary>
            /// <value>The last name.</value>
            public string LastName { get; protected set; }

            /// <summary>
            /// Gets or sets the customer's first name.
            /// </summary>
            /// <value>The first name.</value>
            public string FirstName { get; protected set; }

            /// <summary>
            /// Gets the full contact name.  (Example: "Doe, John").
            /// </summary>
            public string FullName
            {
                get
                {
                    string value = null;
                    if (string.IsNullOrEmpty(this.FirstName))
                    {
                        value = this.LastName;
                    }
                    else if (string.IsNullOrEmpty(this.LastName))
                    {
                        value = this.FirstName;
                    }
                    else
                    {
                        value = this.LastName + ", " + this.FirstName;
                    }

                    if (value == ",")
                    {
                        // sometimes the first/last name is getting entered into the database as ',' - ignore these names
                        return string.Empty;
                    }
                    else
                    {
                        return value;
                    }
                }
            }

            /// <summary>
            /// Gets or sets the department of the address for this order.
            /// </summary>
            public string Department { get; protected set; }

            /// <summary>
            /// Gets the street address for display.
            /// </summary>
            public string StreetAddressForDisplay
            {
                get
                {
                    if (string.IsNullOrEmpty(this.StreetAddressLineOne))
                    {
                        return this.StreetAddressLineTwo;
                    }
                    else if (string.IsNullOrEmpty(this.StreetAddressLineTwo))
                    {
                        return this.StreetAddressLineOne;
                    }
                    else
                    {
                        return this.StreetAddressLineOne + ", " + this.StreetAddressLineTwo;
                    }
                }
            }

            /// <summary>
            /// Gets or sets the first street address line of the address for this order.
            /// </summary>
            public string StreetAddressLineOne { get; protected set; }

            /// <summary>
            /// Gets or sets the second street address line of the address for this order.
            /// </summary>
            public string StreetAddressLineTwo { get; protected set; }

            /// <summary>
            /// Gets or sets the city of the address for this order.
            /// </summary>
            public string City { get; protected set; }

            /// <summary>
            /// Gets or sets the state of the address for this order.
            /// </summary>
            public string State { get; protected set; }

            /// <summary>
            /// Gets or sets the postal code of the address for this order.
            /// </summary>
            public string PostalCode { get; protected set; }

            /// <summary>
            /// Gets or sets the country code of the address for this order.
            /// </summary>
            public string CountryCode { get; protected set; }

            /// <summary>
            /// Gets or sets the telephone number for this order.
            /// </summary>
            public string Telephone { get; protected set; }

            /// <summary>
            /// Gets or sets the email address for this order.
            /// </summary>
            public string Email { get; protected set; }

            /// <summary>
            /// Gets or sets the ID for this order/agreement
            /// </summary>
            public string OrderIdBase { get; protected internal set; }

            /// <summary>
            /// Gets or sets the Account Id for this order/agreement
            /// </summary>
            public string CustomerAccountId { get; protected set; }

            /// <summary>
            /// Gets or sets the Creator Email for this order/agreement
            /// </summary>
            public string CreatorEmail { get; protected set; }

            /// <summary>
            /// Gets or sets the License reason for this order/agreement
            /// </summary>
            public string LicenseReason { get; protected set; }

            /// <summary>
            /// Gets or sets the End Customer Company for this order.
            /// </summary>
            public string EndCustomerCompany { get; protected set; }

            /// <summary>
            /// Gets or sets the End Customer FirstName for this order.
            /// </summary>
            public string EndCustomerFirstName { get; protected set; }

            /// <summary>
            /// Gets or sets the End Customer LastName for this order.
            /// </summary>
            public string EndCustomerLastName { get; protected set; }

            /// <summary>
            /// Gets or sets the End Customer Address1 for this order.
            /// </summary>
            public string EndCustomerAddress1 { get; protected set; }

            /// <summary>
            /// Gets or sets the End Customer Address2 for this order.
            /// </summary>
            public string EndCustomerAddress2 { get; protected set; }

            /// <summary>
            /// Gets or sets the End Customer City for this order.
            /// </summary>
            public string EndCustomerCity { get; protected set; }

            /// <summary>
            /// Gets or sets the End Customer State for this order.
            /// </summary>
            public string EndCustomerState { get; protected set; }

            /// <summary>
            /// Gets or sets the End Customer Country for this order.
            /// </summary>
            public string EndCustomerCountry { get; protected set; }

            /// <summary>
            /// Gets or sets the End Customer Zipcode for this order.
            /// </summary>
            public string EndCustomerZipcode { get; protected set; }

            /// <summary>
            /// Gets or sets the Entitlement Code for this order.
            /// </summary>
            public string EntitlementCode { get; protected set; }

            /// <summary>
            /// Gets or sets the Entitlement Code for this order.
            /// </summary>
            public string ErrorMessage { get; protected set; }

            /// <summary>
            /// Gets or sets the Row Count for this order.
            /// </summary>
            public string RowCount { get; protected set; }

            #endregion

            #region Methods

            /// <summary>
            /// Compare equality of the orders.
            /// </summary>
            /// <param name="left">First order.</param>
            /// <param name="right">Second order.</param>
            /// <returns>True if the orders contain the same values.</returns>
            public static bool operator ==(OrderBase left, OrderBase right)
            {
                if (object.ReferenceEquals(left, right))
                {
                    return true;
                }
                else if (object.ReferenceEquals(left, null))
                {
                    return false;
                }
                else if (object.ReferenceEquals(right, null))
                {
                    return false;
                }

                return
                    left.OrderIdBase == right.OrderIdBase &&
                    left.CertificateId == right.CertificateId &&
                    left.VendorOrderNum == right.VendorOrderNum;
            }

            /// <summary>
            /// Determines if the orders are not equal.
            /// </summary>
            /// <param name="left">First order.</param>
            /// <param name="right">Second order.</param>
            /// <returns>True if the orders DO NOT contain all the same values.</returns>
            public static bool operator !=(OrderBase left, OrderBase right)
            {
                return !(left == right);
            }

            /// <summary>
            /// Compare equality of the orders.
            /// </summary>
            /// <param name="obj">The order to compare to.</param>
            /// <returns>True if the orders contain the same values.</returns>
            public override bool Equals(object obj)
            {
                OrderBase arg = obj as OrderBase;
                if (object.ReferenceEquals(arg, null))
                {
                    return false;
                }
                else
                {
                    return this == arg;
                }
            }

            /// <summary>
            /// Calculates the hashcode based on the contents of the order.
            /// </summary>
            /// <returns>The hashcode for the order.</returns>
            public override int GetHashCode()
            {
                return this.OrderIdBase.ToString().GetHashCode();
            }

            #endregion
        }
    }

