﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lambda.Entities
{
    public class ActivationCode
    {
        public string activationID { get; set; }
        public string quantity { get; set; }
        public string nodeID { get; set; }
    }

    public class AssetDetails
    {
        public string id { get; set; }
        public string type { get; set; }
    }

    public class InstalledFulfillment
    {
        public string activationCode { get; set; }
        public string expirationDate { get; set; }
        public string maintenanceEndDate { get; set; }
        public string quantity { get; set; }
    }

    public class RootObject
    {
        public List<AssetDetails> assets { get; set; }
        public List<ActivationCode> activations { get; set; }
        public List<string> activationCodes { get; set; }
        public List<InstalledFulfillment> installedFulfillments { get; set; }
        public bool returnAvailableQty { get; set; }
        public string entitlementID { get; set; }
        public string getHostInfo { get; set; }
        public string entitlementCode { get; set; }
        public string activationID { get; set; }
        public string quantity { get; set; }
        public string orderNumber { get; set; }
        public string hostID { get; set; }
        public string activationCode { get; set; }
        public string nodeID { get; set; }
        public dynamic hostDetails { get; set; }
        public string alias { get; set; }
        public string dnsName { get; set; }
        public string partNumber { get; set; }
        public string forceFnpLicense { get; set; }
    }

    public class SubscribeNet
    {
        public string RequestHostID { get; set; }
        public string ActivationID { get; set; }
        public string Qty { get; set; }
        public string Metadata { get; set; }
    }

    public class DecryptFNEFile
        {

        /// <summary>
        /// Gets or sets a value indicating root data.
        /// </summary>
        public RequestFileUpload Data { get; set; }

        }

    public class RequestFileUpload
    {
        #region Properties

        /// <summary>
        /// Gets the display host. As host filled at different property like NodeId, HostId etc. so internally all these different property will mapped into one. 
        /// </summary>
        public string DisplayHost
        {
            get
            {
                if (!string.IsNullOrEmpty(Convert.ToString(this.NodeId)))
                {
                    return this.NodeId;
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(this.HostId)))
                {
                    return this.HostId;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the meatdata. metadata will save at license, it will contain only dsnname and host details. 
        /// </summary>
        public string Metadata
        {
            get
            {
                if (!string.IsNullOrEmpty(Convert.ToString(this.DnsName)) && this.HostDetails != null)
                {
                    return "{dnsName :" + Convert.ToString(this.DnsName) + ", hostDetails :" + Convert.ToString(this.HostDetails) + "}";
                }
                else if (string.IsNullOrEmpty(Convert.ToString(this.DnsName)) && !string.IsNullOrEmpty(Convert.ToString(this.HostDetails)))
                {
                    return "{hostDetails :" + Convert.ToString(this.HostDetails) + "}";
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(this.DnsName)) && string.IsNullOrEmpty(Convert.ToString(this.HostDetails)))
                {
                    return "{dnsName :" + Convert.ToString(this.DnsName) + "}";
                }
                else
                {
                    return Convert.ToString(this.FileContent);
                }
            }
        }

        /// <summary>
        /// Gets or sets the host id for this.
        /// </summary>
        public string NodeId { get; set; }

        /// <summary>
        /// Gets or sets the host id for this.
        /// </summary>
        public string HostId { get; set; }

        /// <summary>
        /// Gets or sets the file name for this.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the dnsName for this.
        /// </summary>
        public string DnsName { get; set; }

        /// <summary>
        /// Gets or sets the hostDetails for this.
        /// </summary>
        public dynamic HostDetails { get; set; }

        /// <summary>
        /// Gets or sets the file content for this.
        /// </summary>
        public string FileContent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it is comming from update page.
        /// </summary>
        public bool IsUpdatePage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating status of the process.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the process receive any error message.
        /// </summary>
        public string StatusMessage { get; set; }

        #endregion
    }
}
