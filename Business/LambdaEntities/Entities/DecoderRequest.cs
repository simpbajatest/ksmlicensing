﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LambdaEntities.Entities
{
    public class DecoderRequest
    {

        public class Rawlicense
        {
            public string activationCode { get; set; }
            public bool getHostInfo { get; set; }
        }

        public class Content
        {
            public string StringContent { get; set; }
            public string Status { get; set; }
            public string StatusMessage { get; set; }
        }

        public class RootObject
        {
            public Content data { get; set; }
        }
    }

}
