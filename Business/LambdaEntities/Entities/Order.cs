﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lambda.Entities
{
    /// <summary>
    /// Contains information relating to an Order
    /// </summary>
    public class Order : OrderBase
    {
        #region Constructor 

        public Order()
        {

        }

        ///// <summary>
        ///// Initializes a new instance of the <see cref="Order"/> class.
        ///// </summary>
        ///// <param name="orderId">The order id (will be null before the order is created).</param>
        ///// <param name="certificateId">The certificate number (Example: TTF6-Z8JU).</param>
        ///// <param name="vendorName">The entity (Example AGILENT-TEST).</param>
        ///// <param name="vendorOrderNum">The vendor order num.</param>
        ///// <param name="status">The status.</param>
        ///// <param name="lastModified">The last modified.</param>
        ///// <param name="insertedAt">The inserted at.</param>
        ///// <param name="companyName">Name of the company.</param>
        ///// <param name="lastName">The last name.</param>
        ///// <param name="firstName">The first name.</param>
        ///// <param name="jobTitle">Job Title.</param>
        ///// <param name="parentOrderNumber">The parent order number (for split orders)</param>
        ///// <param name="roomFloor">The room and floor of the address for this order.</param>
        ///// <param name="department">The department of the address for this order.</param>
        ///// <param name="streetAddressLineOne">The first street address line of the address for this order.</param>
        ///// <param name="streetAddressLineTwo">The second street address line of the address for this order.</param>
        ///// <param name="city">The city of the address for this order.</param>
        ///// <param name="state">The state of the address for this order.</param>
        ///// <param name="postalCode">The postal code of the address for this order.</param>
        ///// <param name="countryCode">The country code of the address for this order.</param>
        ///// <param name="telephone">The telephone number for this order.</param>
        ///// <param name="email">The email address for this order.</param>
        ///// <param name="purchaseOrder">The purchase order number for this order.</param>
        ///// <param name="certificateType">The certificate type for this order.</param>
        ///// <param name="orderType">The order type for this order.</param>
        ///// <param name="orderSource">The order source for this order.</param>
        ///// <param name="endCustomerCompany">The end customer company for this order.</param>
        ///// <param name="endCustomerFirstName">The end customer first name for this order.</param>
        ///// <param name="endCustomerLastName">The end customer last name for this order.</param>
        ///// <param name="endCustomerAddress1">The end customer address1 for this order.</param>
        ///// <param name="endCustomerAddress2">The end customer address2 for this order.</param>        
        ///// <param name="endCustomerCity">The end customer city for this order.</param>
        ///// <param name="endCustomerState">The end customer state for this order.</param>
        ///// <param name="endCustomerCountry">The end customer country for this order.</param>
        ///// <param name="endCustomerZipcode">The end customer zip code for this order.</param>
        ///// <param name="entitlementCode">The entitlement code for this order.</param>
        ///// <param name="parentAccountId">The parent account id for this order.</param>         
        ///// <param name="creatorEmail">Creator email of the order</param>
        ///// <param name="licenseReason">License reason of the order</param>
        ///// <param name="createdAt">added to change the signature</param>
        //public Order(
        //    string orderId,
        //    string certificateId,
        //    string vendorName,
        //    string vendorOrderNum,
        //    string status,
        //    DateTime lastModified,
        //    DateTime insertedAt,
        //    string companyName,
        //    string lastName,
        //    string firstName,
        //    string jobTitle,
        //    string parentOrderNumber,
        //    string roomFloor,
        //    string department,
        //    string streetAddressLineOne,
        //    string streetAddressLineTwo,
        //    string city,
        //    string state,
        //    string postalCode,
        //    string countryCode,
        //    string telephone,
        //    string email,
        //    string purchaseOrder,
        //    string certificateType,
        //    string orderType,
        //    string orderSource,
        //    string endCustomerCompany,
        //    string endCustomerFirstName,
        //    string endCustomerLastName,
        //    string endCustomerAddress1,
        //    string endCustomerAddress2,
        //    string endCustomerCity,
        //    string endCustomerState,
        //    string endCustomerCountry,
        //    string endCustomerZipcode,
        //    string entitlementCode,
        //    string parentAccountId,
        //    string creatorEmail,
        //    string licenseReason,
        //    DateTime createdAt)           
        //{
        //    this.VendorName = vendorName;
        //    this.JobTitle = jobTitle;
        //    this.ParentOrderNumber = parentOrderNumber;
        //    this.RoomFloor = roomFloor;
        //}

        ///// <summary>
        ///// Initializes a new instance of the <see cref="Order"/> class.
        ///// </summary>
        ///// <param name="orderId">The order id (will be null before the order is created).</param>
        ///// <param name="certificateId">The certificate number (Example: TTF6-Z8JU).</param>
        ///// <param name="vendorName">The entity (Example AGILENT-TEST).</param>
        ///// <param name="vendorOrderNum">The vendor order num.</param>
        ///// <param name="status">The status.</param>
        ///// <param name="lastModified">The last modified.</param>
        ///// <param name="insertedAt">The inserted at.</param>
        ///// <param name="companyName">Name of the company.</param>
        ///// <param name="lastName">The last name.</param>
        ///// <param name="firstName">The first name.</param>
        ///// <param name="jobTitle">Job Title.</param>
        ///// <param name="parentOrderNumber">The parent order number (for split orders)</param>
        ///// <param name="roomFloor">The room and floor of the address for this order.</param>
        ///// <param name="department">The department of the address for this order.</param>
        ///// <param name="streetAddressLineOne">The first street address line of the address for this order.</param>
        ///// <param name="streetAddressLineTwo">The second street address line of the address for this order.</param>
        ///// <param name="city">The city of the address for this order.</param>
        ///// <param name="state">The state of the address for this order.</param>
        ///// <param name="postalCode">The postal code of the address for this order.</param>
        ///// <param name="countryCode">The country code of the address for this order.</param>
        ///// <param name="telephone">The telephone number for this order.</param>
        ///// <param name="email">The email address for this order.</param>
        ///// <param name="purchaseOrder">The purchase order number for this order.</param>
        ///// <param name="certificateType">The certificate type for this order.</param>
        ///// <param name="orderType">The order type for this order.</param>
        ///// <param name="orderSource">The order source for this order.</param>
        ///// <param name="endCustomerCompany">The end customer company for this order.</param>
        ///// <param name="endCustomerFirstName">The end customer first name for this order.</param>
        ///// <param name="endCustomerLastName">The end customer last name for this order.</param>
        ///// <param name="endCustomerAddress1">The end customer address1 for this order.</param>
        ///// <param name="endCustomerAddress2">The end customer address2 for this order.</param>        
        ///// <param name="endCustomerCity">The end customer city for this order.</param>
        ///// <param name="endCustomerState">The end customer state for this order.</param>
        ///// <param name="endCustomerCountry">The end customer country for this order.</param>
        ///// <param name="endCustomerZipcode">The end customer zip code for this order.</param>
        ///// <param name="entitlementCode">The entitlement code for this order.</param>
        ///// <param name="errorMessage">The Error Message</param>
        ///// <param name="rowCount">The Row Count</param>
        ///// <param name="parentAccountId">The parent account id for this order.</param>          
        ///// <param name="creatorEmail">Creator email of the order</param>
        ///// <param name="licenseReason">License reason of the order</param>
        //public Order(
        //    string orderId,
        //    string certificateId,
        //    string vendorName,
        //    string vendorOrderNum,
        //    string status,
        //    DateTime lastModified,
        //    DateTime insertedAt,
        //    string companyName,
        //    string lastName,
        //    string firstName,
        //    string jobTitle,
        //    string parentOrderNumber,
        //    string roomFloor,
        //    string department,
        //    string streetAddressLineOne,
        //    string streetAddressLineTwo,
        //    string city,
        //    string state,
        //    string postalCode,
        //    string countryCode,
        //    string telephone,
        //    string email,
        //    string purchaseOrder,
        //    string certificateType,
        //    string orderType,
        //    string orderSource,
        //    string endCustomerCompany,
        //    string endCustomerFirstName,
        //    string endCustomerLastName,
        //    string endCustomerAddress1,
        //    string endCustomerAddress2,
        //    string endCustomerCity,
        //    string endCustomerState,
        //    string endCustomerCountry,
        //    string endCustomerZipcode,
        //    string entitlementCode,
        //    string errorMessage,
        //    string rowCount,
        //    string parentAccountId,
        //    string creatorEmail,
        //    string licenseReason)            
        //{
        //    this.VendorName = vendorName;
        //    this.JobTitle = jobTitle;
        //    this.ParentOrderNumber = parentOrderNumber;
        //    this.RoomFloor = roomFloor;
        //    this.EntitlementCode = entitlementCode;
        //    this.OrderId = orderId;
        //    this.CustomerAccountId = parentAccountId;
        //}

        public Order(
                   string orderId,
                   string certificateId,
                   string vendorName,
                   string vendorOrderNum,
                   string status,
                   DateTime lastModified,
                   DateTime insertedAt,
                   string companyName,
                   string lastName,
                   string firstName,
                   string jobTitle,
                   string parentOrderNumber,
                   string roomFloor,
                   string department,
                   string streetAddressLineOne,
                   string streetAddressLineTwo,
                   string city,
                   string state,
                   string postalCode,
                   string countryCode,
                   string telephone,
                   string email,
                   string purchaseOrder,
                   string certificateType,
                   string orderType,
                   string orderSource,
                   string endCustomerCompany,
                   string endCustomerFirstName,
                   string endCustomerLastName,
                   string endCustomerAddress1,
                   string endCustomerAddress2,
                   string endCustomerCity,
                   string endCustomerState,
                   string endCustomerCountry,
                   string endCustomerZipcode,
                   string entitlementCode,
                   string parentAccountId,
                   string creatorEmail)
                   : base(
                       orderId,
                       certificateId,
                       vendorOrderNum,
                       status,
                       lastModified,
                       insertedAt,
                       companyName,
                       lastName,
                       firstName,
                       department,
                       streetAddressLineOne,
                       streetAddressLineTwo,
                       city,
                       state,
                       postalCode,
                       countryCode,
                       telephone,
                       email,
                       purchaseOrder,
                       certificateType,
                       orderType,
                       orderSource,
                       endCustomerCompany,
                       endCustomerFirstName,
                       endCustomerLastName,
                       endCustomerAddress1,
                       endCustomerAddress2,
                       endCustomerCity,
                       endCustomerState,
                       endCustomerCountry,
                       endCustomerZipcode,
                       entitlementCode,
                       parentAccountId,
                       creatorEmail)
        {
            this.VendorName = vendorName;
            this.JobTitle = jobTitle;
            this.ParentOrderNumber = parentOrderNumber;
            this.RoomFloor = roomFloor;            
        }
        #endregion

        #region Properties
       
        /// <summary>
        /// Gets or sets the OrderId as a string
        /// </summary>
        public string OrderPK
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the Order Id for this <see cref="Order"/>.
        /// </summary>
        public string OrderId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the parent order number.
        /// </summary>
        public string ParentOrderNumber { get; private set; }

        /// <summary>
        /// Gets the entity (vendor name) for the Oracle order.  (This value is null for Siebel orders)
        /// </summary>
        public string VendorName { get; private set; }

        /// <summary>
        /// Gets the customer's job title.
        /// </summary>
        /// <value>The job title.</value>
        public string JobTitle { get; private set; }

        /// <summary>
        /// Gets the room and floor of the address for this order.
        /// </summary>
        public string RoomFloor { get; private set; }

        #endregion     
    }
}
