﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Lambda.Entities
{

    /// <summary>
    /// Converts data types from one type to another.
    /// </summary>
    public static class DataConversionUtility
    {
        #region Methods

        /// <summary>
        /// Converts string to long (Int64)
        /// </summary>
        /// <param name="x">The value to convert.</param>
        /// <returns>x converted to a string</returns>
        public static string Int64ToString(long x)
        {
            return Convert.ToString(x, CultureInfo.InvariantCulture.NumberFormat);
        }

        /// <summary>
        /// Converts the specified long (Int64) to a string
        /// </summary>
        /// <param name="instring">The string to convert</param>
        /// <returns>instring converted to a long</returns>
        public static long StringToInt64(string instring)
        {
            return Convert.ToInt64(instring, CultureInfo.InvariantCulture.NumberFormat);
        }

        /// <summary>
        /// Returns string from [column] identified by [label]
        /// </summary>
        /// <param name="row">The row of interest.</param>
        /// <param name="label">The name of the column.</param>
        /// <returns>The string value of a single column in the row.</returns>
        public static string GetRowValue(DataRow row, string label)
        {
            if (string.IsNullOrEmpty(label))
            {
                throw new ArgumentException("label cannot be null nor empty");
            }

            if (row == null)
            {
                throw new ArgumentException("data row has errors");
            }

            try
            {
                string s = row[DataConversionUtility.StringToUpper(label)].ToString();
                if (s.Length == 0)
                {
                    return string.Empty;
                }

                return s;
            }
            catch (Exception e)
            {
                throw new DataException("GetRowValue failed", e);
            }
        }

        /// <summary>
        /// Overloaded. Returns BOXED value from [column] identified by [label]. If the column doesn't exist in the
        /// row OR the column value is Null or String.Empty, then return the value provided in 
        /// [default] param. 
        /// </summary>
        /// <param name="row">The row of interest.</param>
        /// <param name="label">The name of the column.</param>
        /// <param name="defaultValue">The BOXED value to return if column is not found, null, or an empty string.</param>
        /// <returns>The string value of a single column in the row.</returns>
        public static object GetRowValue(DataRow row, string label, object defaultValue)
        {
            if (string.IsNullOrEmpty(label))
            {
                throw new ArgumentException("label cannot be null nor empty");
            }

            if (row == null)
            {
                throw new ArgumentException("data row is null");
            }

            try
            {
                // convert label to uppercase
                label = label.ToUpperInvariant();

                // check to see if the column exists in the row.
                if (row.Table.Columns.Contains(label))
                {
                    // column exists, is the value null or an empty string?
                    object value = row[label];

                    if (!(value is DBNull))
                    {
                        return string.IsNullOrEmpty(value as string) ? defaultValue : value;
                    }
                    else
                    {
                        return defaultValue;
                    }
                }
                else
                {
                    // column doesn't exist, return the defaultValue
                    return defaultValue;
                }
            }
            catch (Exception e)
            {
                throw new DataException("GetRowValue failed", e);
            }
        }

        /// <summary>
        /// Returns string from [row,column] identified by [int row, label]
        /// </summary>
        /// <param name="ds">The dataset.</param>
        /// <param name="row">The index of the row.</param>
        /// <param name="label">The name of the column.</param>
        /// <returns>The string value of a single cell in the first table of the dataset.</returns>
        public static string GetRowValue(DataSet ds, int row, string label)
        {
            if (string.IsNullOrEmpty(label))
            {
                throw new ArgumentException("label cannot be null nor empty");
            }

            if (ds == null)
            {
                throw new DataException("dataset is null.");
            }

            if (row < 0)
            {
                throw new DataException("Request row number is less than zero.");
            }

            if (ds.Tables[0].Rows.Count <
                row + 1)
            {
                throw new DataException("Requested row number is greater than collection contains");
            }

            try
            {
                object o = ds.Tables[0].Rows[row][DataConversionUtility.StringToUpper(label)];
                if (o == null)
                {
                    return string.Empty;
                }

                return o.ToString();
            }
            catch (Exception e)
            {
                throw new DataException("GetRowValue failed", e);
            }
        }

        /// <summary>
        /// Provides System.Globalization.CultureInfo.InvariantCulture for you
        /// </summary>
        /// <param name="instring">The string to convert.</param>
        /// <returns>Returns a copy of the string converted to all upper case.</returns>
        public static string StringToUpper(string instring)
        {
            if (string.IsNullOrEmpty(instring))
            {
                return string.Empty;
            }

            return instring.ToUpper(System.Globalization.CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Uses a Dag-approved regular expresion to determine if an email address valid.
        /// </summary>
        /// <param name="proposedAddress">The proposed address.</param>
        /// <returns>
        ///   <c>true</c> if the proposed e-mail address is valid; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsEmailAddressValid(string proposedAddress)
        {
            if (string.IsNullOrEmpty(proposedAddress))
            {
                return false;
            }

            Regex emailRegEx = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                         @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                         @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            return emailRegEx.IsMatch(proposedAddress);
        }

        /// <summary>
        /// Uses a Dag-approved regular expresion to determine if an email address valid.
        /// </summary>
        /// <param name="proposedAddress">The proposed address.Group of comma seprated email address</param>
        /// <param name="delimiter">new delimitor for email</param>
        /// <returns>
        ///   <c>true</c> if All e-mail address is valid in string ; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsAllEmailAddressValid(string proposedAddress, char delimiter)
        {
            if (!string.IsNullOrEmpty(proposedAddress.Trim()))
            {
                proposedAddress = proposedAddress.Replace(',', delimiter).Replace(';', delimiter);

                // ignore blank email address but if all values are blank return false
                string[] emails = proposedAddress.Split(new[] { delimiter }, StringSplitOptions.RemoveEmptyEntries).Where(m => !string.IsNullOrEmpty(m.Trim())).Select(s => s).ToArray();
                if (emails.Length == 0)
                {
                    return false;
                }

                foreach (string email in emails)
                {
                    if (!IsEmailAddressValid(email.Trim()))
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get first to email address from multiple emails
        /// </summary>
        /// <param name="proposedAddress">The proposed address.Group of comma seprated email address</param>
        /// <param name="delimiter">new delimitor for email</param>
        /// <returns>
        ///   <c>true</c> if All e-mail address is valid in string ; otherwise, <c>false</c>.
        /// </returns>
        public static string GetSingleEmailAddress(string proposedAddress, char delimiter)
        {
            if (!string.IsNullOrEmpty(proposedAddress.Trim()))
            {
                proposedAddress = proposedAddress.Replace(',', delimiter).Replace(';', delimiter);

                // ignore blank email address but if all values are blank return false
                string[] emails = proposedAddress.Split(new[] { delimiter }, StringSplitOptions.RemoveEmptyEntries).Where(m => !string.IsNullOrEmpty(m.Trim())).Select(s => s).ToArray();
                if (emails.Length > 0)
                {
                    return emails[0];
                }
            }

            // code will never come at this point 
            return string.Empty;
        }

        /// <summary>
        /// Get cc email address with to email address
        /// </summary>
        /// <param name="toAddress">The to address.Group of comma seprated email address</param>
        /// <param name="ccAddress">The cc address.Group of comma seprated email address</param>
        /// <param name="delimiter">new delimitor for email</param>
        /// <returns>
        ///   <c>true</c> if All e-mail address is valid in string ; otherwise, <c>false</c>.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Using Win32 naming for consistency.")]
        public static string CreateCCEmailAddress(string toAddress, string ccAddress, char delimiter)
        {
            if (!string.IsNullOrEmpty(toAddress.Trim()))
            {
                toAddress = toAddress.Replace(',', delimiter).Replace(';', delimiter);
                ccAddress = ccAddress.Replace(',', delimiter).Replace(';', delimiter);

                // ignore blank email address but if all values are blank return false
                string[] emails = toAddress.Split(new[] { delimiter }, StringSplitOptions.RemoveEmptyEntries).Where(m => !string.IsNullOrEmpty(m.Trim())).Select(s => s).ToArray();
                if (emails.Length > 0)
                {
                    for (int i = 1; i < emails.Length; i++)
                    {
                        if (!ccAddress.ToUpper().Contains(emails[i].ToUpper()))
                        {
                            ccAddress = string.IsNullOrWhiteSpace(ccAddress) == false ? ccAddress + delimiter + emails[i] : emails[i];
                        }
                    }
                }

                return ccAddress;
            }

            // code will never come at this point 
            return string.Empty;
        }

        /// <summary>
        /// Determines whether the string can be converted to a uint.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        ///     <c>true</c> if the uint is valid; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidUint(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            try
            {
                uint.Parse(value, CultureInfo.InvariantCulture);
                return true;
            }
            catch (FormatException)
            {
                // the user entered something other than a number
                return false;
            }
            catch (OverflowException)
            {
                // this occurs with decimal, negative and extremely large numbers, 
                return false;
            }
        }

        /// <summary>
        /// Gets the quantity from the string
        /// </summary>
        /// <param name="value">The string to convert.</param>
        /// <returns>The value as an uint.</returns>
        public static uint ConvertToUint(string value)
        {
            if (!IsValidUint(value))
            {
                throw new FormatException("Not a valid uint.");
            }

            return uint.Parse(value, CultureInfo.InvariantCulture);
        }

        #endregion
    }
}
