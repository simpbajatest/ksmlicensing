﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lambda.Entities
{
    public static class StringExtensions
    {
        /// <summary>
        /// Create the valid delimeted email string 
        /// </summary>  
        /// <param name="value">The string value.</param>
        /// <param name="oldDelimiter">old delimitor for email</param>
        /// <param name="newDelimiter">new delimitor for email</param>
        /// <returns>
        /// Truncated string.
        /// </returns>
        public static string ToValidEmailString(this string value, char oldDelimiter, char newDelimiter)
        {
            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrEmpty(value))
            {
                value = value.Replace(oldDelimiter, newDelimiter);
                foreach (string email in value.Split(newDelimiter))
                {
                    if (!string.IsNullOrEmpty(email.Trim()))
                    {
                        if (!string.IsNullOrEmpty(sb.ToString()))
                        {
                            sb.Append(newDelimiter);
                        }

                        sb.Append(email);
                    }
                }
            }

            return sb.ToString();
        }
    }
}
