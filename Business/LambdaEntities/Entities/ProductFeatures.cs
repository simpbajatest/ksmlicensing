﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Lambda.Entities
{

    /// <summary>
    ///  Contains Host type information contained in 
    ///     LICENSE.VALIDATION_TYPE and
    ///     LICENSE.VALIDATION_PROPERTIES
    /// </summary>
    public class ProductFeatures : BusinessEntityBase
    {
        #region Fields

        /// <summary>
        /// The validation paterns for this host spec
        /// </summary>
        private readonly List<string> validationPatterns = new List<string>();

        /// <summary>
        /// The database id of the host spec
        /// </summary>
        private long hostSpecId;

        #endregion

        #region Constructor      

        /// <summary>
        /// Initializes a new instance of the <see cref="HostSpec"/> class.
        /// </summary>
        /// <param name="quantity">The quantity of product feature.</param>
        /// <param name="feature">feature of product.</param>    
        public ProductFeatures(
                        long quantity,
                        string feature)
        {           

            this.Quantity = quantity;
            this.Feature = feature;          
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the database quantity of the product feature.
        /// </summary>
        public long Quantity
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the feature of the product
        /// This is NOT intended for customers to see. (see Title)
        /// </summary>
        /// <value>The feature of the host.</value>
        /// <remarks>This value is set in the database.</remarks>
        public string Feature
        {
            get;
            private set;
        }

        
        #endregion

        #region Methods

        /// <summary>
        /// Compares the HostSpec's to see if they are the same HostSpec.
        /// Compares the HostSpecId for equality.
        /// </summary>
        /// <param name="left">Left operand.</param>
        /// <param name="right">Right operand.</param>
        /// <returns>True if the HostSpec's contain the same HostSpecId.</returns>
        public static bool operator ==(ProductFeatures left, ProductFeatures right)
        {
            if (object.ReferenceEquals(left, right))
            {
                return true;
            }
            else if (object.ReferenceEquals(left, null))
            {
                return false;
            }
            else if (object.ReferenceEquals(right, null))
            {
                return false;
            }

            return left.Feature == right.Feature;
        }

        /// <summary>
        /// Determines if the entitlements are not equal.
        /// Uses the HostSpecId to compare.
        /// </summary>
        /// <param name="left">Left operand.</param>
        /// <param name="right">Right operand.</param>
        /// <returns>True if the hosts DO NOT contain the same HostSpecId.</returns>
        public static bool operator !=(ProductFeatures left, ProductFeatures right)
        {
            return !(left == right);
        }

        /// <summary>
        /// Compares the entitlements to see if they are the same entitlement.
        /// Compares the HostSpecId for equality.
        /// </summary>
        /// <param name="obj">The entitlement to compare to.</param>
        /// <returns>True if the entitlements contain the same HostSpecId.</returns>
        public override bool Equals(object obj)
        {
            ProductFeatures arg = obj as ProductFeatures;
            if (object.ReferenceEquals(arg, null))
            {
                return false;
            }
            else
            {
                return this == arg;
            }
        }

        /// <summary>
        /// Gets the unique hashcode for this entitlement.
        /// Based on the entitlement id.
        /// </summary>
        /// <returns>Unique hashcode for this instance.</returns>
        public override int GetHashCode()
        {
            return this.Feature.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "{0} ({1})", this.Quantity, this.Feature.ToString());
        }
        
        #endregion
    }
}