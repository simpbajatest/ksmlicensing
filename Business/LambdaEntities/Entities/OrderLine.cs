﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lambda.Entities
{
    /// <summary>
    /// Contains information relating to an OrderLine
    /// </summary>
    public class OrderLine : LineBase
    {
        #region Constructors

        public OrderLine()
        {            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderLine"/> class.
        /// </summary>
        /// <param name="masterErpLineNumber">The Master ERP line number.</param>
        /// <param name="splitQtyFlag">The Split Quantity Flag.</param>
        /// <param name="asmNotifyCustomerDate">The KSM Notify Customer Date.</param>
        /// <param name="csrEmail">The Customer Sales Rep Email address.</param>
        /// <param name="customerEmail">The customer email.</param>
        /// <param name="deliveryMethod">The Delivery Method.</param>
        /// <param name="description">The description of the product in the order line.</param>
        /// <param name="emailedAtDate">The Emailed At date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="erpLineNumber">The ERP Line Number.</param>
        /// <param name="erpScheduledShipDate">The ERP Scheduled Ship Date.</param>
        /// <param name="erpStatus">The ERP Status.</param>
        /// <param name="extAsBase">The Ext As Base.</param>
        /// <param name="extendedBaseLineId">The Extended Base Line Id.</param>
        /// <param name="externalLineId">The External Line Id.</param>
        /// <param name="insertedAt">The inserted at field.</param>
        /// <param name="notes">The notes.</param>
        /// <param name="orderId">The OrderId in this object.</param>
        /// <param name="orderLineId">The OrderLineId.</param>
        /// <param name="printStatus">The Print Status.</param>
        /// <param name="productId">The Product ID.</param>
        /// <param name="prodNum">The product number.</param>
        /// <param name="qtyOrdered">The quantity ordered.</param>
        /// <param name="qtyRedeemable">The quantity redeemable.</param>
        /// <param name="qtyRedeemed">The quantity redeemed.</param>
        /// <param name="secondaryEmail">The Secondary Email address.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="customerRequestDate">The customer request date.</param>
        /// <param name="status">The status of the order line.</param>
        /// <param name="agreementnumber">The agreementnumber value.</param>
        /// <param name="assetnumber">The assetnumber value.</param>
        /// <param name="activationcode">The activationcode value.</param>
        /// <param name="errorMessage">The Error Message.</param>
        /// <param name="itemtype">The Item Type.</param>
        /// <param name="linkid">The link Ids.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "SA0102: A syntax error has been discovered", Justification = "Optional paramenter in use")]
        public OrderLine(
            string masterErpLineNumber,
            string splitQtyFlag,
            DateTime asmNotifyCustomerDate,
            string csrEmail,
            string customerEmail,
            string deliveryMethod,
            string description,
            DateTime emailedAtDate,
            DateTime endDate,
            string erpLineNumber,
            DateTime erpScheduledShipDate,
            string erpStatus,
            bool extAsBase,
            long extendedBaseLineId,
            long externalLineId,
            DateTime insertedAt,
            string notes,
            string orderId,
            long orderLineId,
            string printStatus,
            long productId,
            string prodNum,
            long qtyOrdered,
            long qtyRedeemable,
            long qtyRedeemed,
            string secondaryEmail,
            DateTime startDate,
            DateTime? customerRequestDate,
            string status,
            string agreementnumber,
            string assetnumber,
            string activationcode,
            string errorMessage,
            string itemtype,
            string linkid)
            : base(
                   description,
                   endDate,
                   orderId,
                   orderLineId,
                   productId,
                   prodNum,
                   qtyOrdered,
                   startDate,
                   status)
        {
            this.MasterErpLineNumber = masterErpLineNumber;
            this.SplitQtyFlag = splitQtyFlag;
            this.AsmNotifyCustomerDate = asmNotifyCustomerDate;
            this.CsrEmail = csrEmail;
            this.CustomerEmail = customerEmail;
            this.CustomerRequestDate = customerRequestDate;
            this.DeliveryMethod = deliveryMethod;
            this.EmailedAtDate = emailedAtDate;
            this.ErpLineNumber = erpLineNumber;
            this.ErpScheduledShipDate = erpScheduledShipDate;
            this.ErpStatus = erpStatus;
            this.ExtAsBase = extAsBase;
            this.ExtendedBaseLineId = extendedBaseLineId;
            this.ExternalLineId = externalLineId;
            this.InsertedAt = insertedAt;
            this.Notes = notes;
            this.PrintStatus = printStatus;
            this.QtyRedeemable = qtyRedeemable;
            this.QtyRedeemed = qtyRedeemed;
            this.SecondaryEmail = secondaryEmail;
            this.AgreementNumber = agreementnumber;
            this.AssetNumber = assetnumber;
            this.ActivationCode = activationcode;
            this.ErrorMessage = errorMessage;
            this.ItemType = itemtype;
            this.LinkId = linkid;
        }

        #endregion

        #region Properties 

        /// <summary>
        /// Gets the OrderId in this object
        /// </summary>
        public string OrderId
        {
            get;
            private set;
        }


        /// <summary>
        /// Gets or sets the LinkId from the ILD3
        /// </summary>
        public string LinkId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the OrderLineId
        /// </summary>
        public int OrderLineId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the AgreementNumber here
        /// </summary>
        public string AgreementNumber { get; set; }

        /// <summary>
        /// Gets or sets the AssetNumber here
        /// </summary>
        public string AssetNumber { get; set; }

        /// <summary>
        /// Gets or sets the AssetNumber here
        /// </summary>
        public string ProductNumber { get; set; }

        /// <summary>
        /// Gets or sets the ActivationCode here
        /// </summary>
        public string ActivationCode { get; set; }

        /// <summary>
        /// Gets or sets the Item Type
        /// </summary>
        public string ItemType { get; set; }

        /// <summary>
        /// Gets the Master ERP line number
        /// </summary>
        public string MasterErpLineNumber
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the Split Quantity Flag
        /// </summary>
        public string SplitQtyFlag
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the KSM Notify Customer Date
        /// </summary>
        public DateTime AsmNotifyCustomerDate
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the Customer Sales Rep Email address
        /// </summary>
        public string CsrEmail
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the customer email
        /// </summary>
        public string CustomerEmail
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the customer request date. (this is used to calculate that start date for subscriptions)
        /// </summary>
        public DateTime? CustomerRequestDate
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the Delivery Method
        /// </summary>
        public string DeliveryMethod
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the "Emailed At" date
        /// </summary>
        public DateTime EmailedAtDate
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the ERP Line Number
        /// </summary>
        public string ErpLineNumber
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the ERP Scheduled Ship Date
        /// </summary>
        public DateTime ErpScheduledShipDate
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the ERP Status
        /// </summary>
        public string ErpStatus
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether the Ext As Base.
        /// </summary>
        public bool ExtAsBase
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the Extended Base Line Id
        /// </summary>
        public long ExtendedBaseLineId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the External Line Id
        /// </summary>
        public long ExternalLineId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the inserted_at date
        /// </summary>
        public DateTime InsertedAt
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the Notes
        /// </summary>
        public string Notes
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the Print Status
        /// </summary>
        public string PrintStatus
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the quantity redeemable.
        /// </summary>
        public long QtyRedeemable
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the quantity redeemed.
        /// </summary>
        public long QtyRedeemed
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the Secondary Email address
        /// </summary>
        public string SecondaryEmail
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the email Message
        /// </summary>
        public string ErrorMessage
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the cert id
        /// </summary>
        public string CertID
        {
            get;
            set;
        }

        #endregion
    }
}
