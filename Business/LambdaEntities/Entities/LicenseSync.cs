﻿namespace Lambda.Entities
{
    public class LicenseSync
    {
        public string Host_Id { get; set; }
        public string End_Date { get; set; }
        public string Exp_Date { get; set; }
        public string Qty { get; set; }
        public string Lic_Type { get; set; }
        public string License_Id { get; set; }
        public string Activation_Code { get; set; }
    }
}
