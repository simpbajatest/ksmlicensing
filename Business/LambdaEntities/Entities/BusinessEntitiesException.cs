﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lambda.Entities
{

    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A base class of an exception thrown from the BusinessEntities project
    /// </summary>
    [Serializable]
    public class BusinessEntitiesException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessEntitiesException"/> class.
        /// </summary>
        public BusinessEntitiesException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessEntitiesException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public BusinessEntitiesException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessEntitiesException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        public BusinessEntitiesException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessEntitiesException"/> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization info.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected BusinessEntitiesException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }
    }
}
