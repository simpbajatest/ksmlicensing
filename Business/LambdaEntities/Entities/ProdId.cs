﻿namespace Lambda.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Handling of the product key
    /// </summary>
    [Serializable]
    public class ProdId : IdBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ProdId class.
        /// </summary>
        public ProdId()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ProdId class.
        /// </summary>
        /// <param name="id">The license id.</param>
        public ProdId(long id)
            : base(id)
        {
        }

        /// <summary>
        /// Initializes a new instance of the ProdId class.
        /// </summary>
        /// <param name="id">A string representation of the id.</param>
        public ProdId(string id)
            : base(id)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProdId"/> class.
        /// </summary>
        /// <param name="info">The serialization info.</param>
        /// <param name="context">The streaming context.</param>
        protected ProdId(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        #endregion
    }
}
