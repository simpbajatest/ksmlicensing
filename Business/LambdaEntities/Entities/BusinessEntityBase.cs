﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Lambda.Entities
{  /// <summary>
   /// Track changes to a record,  [ CHANGED UNCHANGED NEW]
   /// </summary>
    public enum ChangeFlag
    {
        /// <summary>
        /// Record was changed, will need to update it
        /// </summary>
        Changed,

        /// <summary>
        /// Record was not changed
        /// </summary>
        Unchanged,

        /// <summary>
        /// Record is new,will need to insert it
        /// </summary>
        New
    }

    /// <summary>
    /// Base class for BusinessEntity classes
    /// </summary>
    public abstract class BusinessEntityBase
    {
        #region Fields

        /// <summary>
        /// Indicator of whether this object is new, changed or unchanged.
        /// </summary>
        private ChangeFlag changeFlag = ChangeFlag.Unchanged;

        #endregion       

        #region Properties

        /// <summary>
        /// Gets or sets the value of the ChangeFlag.  
        /// The ChangeFlag indicates whether this item is new, changed or unchanged.
        /// </summary>
        public ChangeFlag ChangeFlag
        {
            get
            {
                return this.changeFlag;
            }

            set
            {
                this.changeFlag = value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates the object
        /// </summary>
        public virtual void Validate()
        {
            // some existing classes call validate on objects that inherit from this, 
            // and an exception blows them up:
            // throw new NotImplementedException();
        }

        /// <summary>
        /// Determines whether two such objects are equivalent
        /// </summary>
        /// <param name="obj">Another object of this type to be compared to this one</param>
        /// <returns>True if this objects is equivalent to another</returns>
        [SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations", Justification = "We want to force inheriting classes to think about this if they use it.")]
        public override bool Equals(object obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Delivers a unique hash code for this object
        /// </summary>
        /// <returns>Integer hash code</returns>
        [SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations", Justification = "We want to force inheriting classes to think about this if they use it.")]
        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
