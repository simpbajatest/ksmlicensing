﻿//-----------------------------------------------------------------------
// <copyright file="SyncInstalledFulfillment.cs" company="Keysight Technologies, Inc.">
//      Copyright (C) Keysight Technologies, Inc.
// </copyright>
//----------------------------------------------------------------------
namespace Lambda.Entities
{
    using System;  

    /// <summary>
    /// All the properties of a KSM SyncInstalledFulfillment that are stored in the asl_schema.SyncInstalledFulfillment database
    /// </summary>
    [Serializable]
    public class SyncInstalledFulfillment : BusinessEntityBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyncInstalledFulfillment"/> class.
        /// </summary>
        /// <param name="qty">The SyncInstalledFulfillment qty</param>
        /// <param name="expDate">The login email address</param>
        /// <param name="endDate">The SyncInstalledFulfillment status (Active or Inactive).</param>
        /// <param name="activationCode">The created at date.</param>      
        public SyncInstalledFulfillment(
            uint qty,                      
            DateTime? expDate,
            DateTime? endDate,
            string activationCode)
        {
            if (activationCode == null)
            {
                throw new ArgumentNullException("activationCode", "activationCode must be specified for SyncInstalledFulfillments.");
            }

            this.Qty = qty;
            this.ActivationCode = activationCode;
            this.Expdate = expDate;
            this.EndDate = endDate;           
        }

        /// <summary>
        /// Gets the Qty associated with this SyncInstalledFulfillment.
        /// </summary>
        public uint Qty { get; private set; }

        /// <summary>
        /// Gets the ActivationCode from theSyncInstalledFulfillment table
        /// </summary> 
        public string ActivationCode { get; private set; }       

        /// <summary>
        /// Gets the Expiration date from SyncInstalledFulfillment table
        /// </summary> 
        public DateTime? Expdate { get; private set; }

        /// <summary>
        /// Gets the EndDate from SyncInstalledFulfillment table
        /// </summary> 
        public DateTime? EndDate { get; private set; }
    }
}
