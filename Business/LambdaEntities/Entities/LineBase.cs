﻿using System;

namespace Lambda.Entities
{
    /// <summary>
    /// This class contains the information that is common between order lines
    /// and agreement lines.  To access information specific to Oracle (Orders table)
    /// or Siebel (Agreement table) use the derived class.
    /// </summary>
    public abstract class LineBase : BusinessEntityBase
    {
        #region Constructors

        public LineBase()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LineBase"/> class.
        /// </summary>
        /// <param name="description">The description of the product in the order line.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="orderId">The OrderId in this object.</param>
        /// <param name="orderLineId">The OrderLineId.</param>
        /// <param name="productId">The Product ID.</param>
        /// <param name="prodNum">The product number.</param>
        /// <param name="qtyOrdered">The quantity ordered.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="status">The status of the order line.</param>
        public LineBase(
            string description,
            DateTime endDate,
            string orderId,
            long orderLineId,
            long productId,
            string prodNum,
            long qtyOrdered,
            DateTime startDate,
            string status)
        {
            this.Description = description;
            this.EndDate = endDate;
            this.OrderIdBase = orderId;
            this.LineIdBase = orderLineId;
            this.ProductId = productId;
            this.ProdNum = prodNum;
            this.QtyOrdered = qtyOrdered;
            this.StartDate = startDate;
            this.Status = status;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the description of the product in the order line.
        /// </summary>
        /// <remarks>
        /// This field is not written back to the database.
        /// </remarks>
        public string Description
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the quantity ordered.
        /// </summary>
        public long QtyOrdered
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the end_date
        /// </summary>
        public DateTime EndDate
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the OrderIdBase in this object
        /// </summary>
        public string OrderIdBase
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the LineIdBase
        /// </summary>
        public long LineIdBase
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the ProductId
        /// </summary>
        public long ProductId
        {
            get;

            // NOTE:  this is read/write because of a kludge in CreatorLoader.cs
            set;
        }

        /// <summary>
        /// Gets the Product Number.
        /// </summary>
        public string ProdNum
        {
            get;
            private set;
        }        

        /// <summary>
        /// Gets the start date.
        /// </summary>
        public DateTime StartDate
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the status of the status of the order line, not the record.
        /// </summary>
        /// <remarks>This field is not written back to the database.</remarks>
        public string Status
        {
            get;
            private set;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Compare equality of the base lines.
        /// </summary>
        /// <param name="left">First base line.</param>
        /// <param name="right">Second base line.</param>
        /// <returns>True if the base lines contain the same values.</returns>
        public static bool operator ==(LineBase left, LineBase right)
        {
            if (object.ReferenceEquals(left, right))
            {
                return true;
            }
            else if (object.ReferenceEquals(left, null))
            {
                return false;
            }
            else if (object.ReferenceEquals(right, null))
            {
                return false;
            }

            return
              left.OrderIdBase == right.OrderIdBase &&
              left.ProdNum == right.ProdNum &&
              left.LineIdBase == right.LineIdBase &&
              left.QtyOrdered == right.QtyOrdered &&
              left.StartDate == right.StartDate &&
              left.EndDate == right.EndDate;
        }

        /// <summary>
        /// Determines if the order lines are not equal.
        /// </summary>
        /// <param name="left">First order line.</param>
        /// <param name="right">Second order line.</param>
        /// <returns>True if the order lines DO NOT contain all the same values.</returns>
        public static bool operator !=(LineBase left, LineBase right)
        {
            return !(left == right);
        }

        /// <summary>
        /// Compare equality of the base lines.
        /// </summary>
        /// <param name="obj">The base line to compare to.</param>
        /// <returns>True if the base lines contain the same values.</returns>
        public override bool Equals(object obj)
        {
            LineBase arg = obj as LineBase;
            if (object.ReferenceEquals(arg, null))
            {
                return false;
            }
            else
            {
                return this == arg;
            }
        }

        /// <summary>
        /// Calculates the hashcode based on the contents of the base line.
        /// </summary>
        /// <returns>The hashcode for the order line.</returns>
        public override int GetHashCode()
        {
            int hashcode = this.OrderIdBase.GetHashCode() ^
                           this.LineIdBase.GetHashCode();
            return hashcode;
        }

        #endregion
    }
}
