﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Globalization;

namespace Lambda.Entities
{
    public class Product : BusinessEntityBase
    {
        #region Fields

        /// <summary>
        /// The description of this version of the product (not a number)
        /// </summary>
        private string versionDescription = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// DO NOT USE THIS CONSTRUCTOR FOR NEW CODE
        /// </summary>
        public Product()
        {
            this.ProductNum = "SimTest";
            this.ProdLine = "1234";
            this.ProductDesc = "This is for testing purpose only";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// </summary>
        /// <param name="productId">The unique identifier for this product.</param>
        /// <param name="entity">Entity name</param>
        /// <param name="productNum">Product number</param>
        /// <param name="productDesc">Product description</param>
        /// <param name="trialDesc">Trial description</param>
        /// <param name="status">Product status</param>
        /// <param name="revision">Product revision.</param>
        /// <param name="productVerNum">Product version number</param>
        /// <param name="productVerDesc">Product version description</param>
        /// <param name="redeemOnePerHostFlag">Redeem one per host flag</param>
        /// <param name="orderable">Orderable flag</param>
        /// <param name="allowRehosting">Allow rehosting flag</param>
        /// <param name="hostIdHelpUrl">Host ID Help URL</param>
        /// <param name="renewableLicense">Renewable license flag</param>
        //// <param name="lineNumber">Line Number</param>
        //// <param name="quantity">Quantity Required</param>
        [SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "11#", Justification = "Not enough time to refactor.")]
        public Product(
                       int productId,
                       string entity,
                       string productNum,
                       string productDesc,
                       string trialDesc,
                       string status,
                       string revision,
                       uint productVerNum,
                       string productVerDesc,
                       string redeemOnePerHostFlag,
                       string orderable,
                       string allowRehosting,
                       string hostIdHelpUrl,
                       string renewableLicense)
        //// string lineNumber,
        //// string quantity)
        {
            this.ProductId = Convert.ToInt32(productId.ToString());
            this.EntityName = entity;
            this.ProductNum = productNum;
            this.ProductDesc = productDesc;
            this.TrialDescription = trialDesc;
            this.Status = status;
            this.Revision = revision;
            this.VersionId = productVerNum;
            this.VersionDescription = productVerDesc;
            ////this.LineNumber = lineNumber;
            ////this.Quantity = quantity;

            if (!string.IsNullOrEmpty(redeemOnePerHostFlag))
            {
                this.LimitQuantityToOnePerHost = redeemOnePerHostFlag == "Y";
            }
            else
            {
                this.LimitQuantityToOnePerHost = false;
            }

            if (!string.IsNullOrEmpty(orderable))
            {
                this.OrderableFlag = orderable == "Y" ? true : false;
            }
            else
            {
                this.OrderableFlag = false;
            }

            if (!string.IsNullOrEmpty(allowRehosting))
            {
                this.AllowRehostingFlag = allowRehosting == "Y" ? true : false;
            }
            else
            {
                this.AllowRehostingFlag = false;
            }

            if (!string.IsNullOrEmpty(hostIdHelpUrl))
            {
                try
                {
                    Uri uri = new Uri(hostIdHelpUrl);
                    this.HostIdHelpUrl = uri;
                }
                catch (UriFormatException ufe)
                {
                  throw new BusinessEntitiesException("Product HostID Help URL string is badly formatted.", ufe);
                }
            }

            if (!string.IsNullOrEmpty(renewableLicense))
            {
                this.RenewableLicense = renewableLicense == "Y" ? true : false;
            }
            else
            {
                this.RenewableLicense = false;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.LID3 - ManualOrderEntry Page changes  -Added new column RMU 
        /// </summary>
        /// <param name="productId">The unique identifier for this product.</param>
        /// <param name="entity">Entity name</param>
        /// <param name="productNum">Product number</param>
        /// <param name="productDesc">Product description</param>
        /// <param name="status">Product status</param>
        /// <param name="revision">Product revision.</param>
        /// <param name="productVerNum">Product version number</param>
        /// <param name="productVerDesc">Product version description</param>
        /// <param name="redeemOnePerHostFlag">Redeem one per host flag</param>
        /// <param name="orderable">Orderable flag</param>
        /// <param name="allowRehosting">Allow rehosting flag</param>
        /// <param name="hostIdHelpUrl">Host ID Help URL</param>
        /// <param name="renewableLicense">Renewable license flag</param>
        /// <param name="rmu">RMU Number</param>
        /// <param name="prodLine">product line</param>        
        [SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "11#", Justification = "Not enough time to refactor.")]
        public Product(
                       int productId,
                       string entity,
                       string productNum,
                       string productDesc,
                       string status,
                       string revision,
                       uint productVerNum,
                       string productVerDesc,
                       string redeemOnePerHostFlag,
                       string orderable,
                       string allowRehosting,
                       string hostIdHelpUrl,
                       string renewableLicense,
                       string rmu,
                       string prodLine)
        {
            this.ProductId = Convert.ToInt32(productId.ToString());
            this.EntityName = entity;
            this.ProductNum = productNum;
            this.ProductDesc = productDesc;
            this.Status = status;
            this.Revision = revision;
            this.VersionId = productVerNum;
            this.VersionDescription = productVerDesc;
            if (!string.IsNullOrEmpty(rmu))
            {
                this.Rmu = Convert.ToInt64(rmu);
            }
           
            if (!string.IsNullOrEmpty(redeemOnePerHostFlag))
            {
                this.LimitQuantityToOnePerHost = redeemOnePerHostFlag == "Y";
            }
            else
            {
                this.LimitQuantityToOnePerHost = false;
            }

            if (!string.IsNullOrEmpty(orderable))
            {
                this.OrderableFlag = orderable == "Y" ? true : false;
            }
            else
            {
                this.OrderableFlag = false;
            }

            if (!string.IsNullOrEmpty(allowRehosting))
            {
                this.AllowRehostingFlag = allowRehosting == "Y" ? true : false;
            }
            else
            {
                this.AllowRehostingFlag = false;
            }

            if (!string.IsNullOrEmpty(hostIdHelpUrl))
            {
                try
                {
                    Uri uri = new Uri(hostIdHelpUrl);
                    this.HostIdHelpUrl = uri;
                }
                catch (UriFormatException ufe)
                {
                   throw new BusinessEntitiesException("Product HostID Help URL string is badly formatted.", ufe);
                }
            }

            if (!string.IsNullOrEmpty(renewableLicense))
            {
                this.RenewableLicense = renewableLicense == "Y" ? true : false;
            }
            else
            {
                this.RenewableLicense = false;
            }

            this.ProdLine = prodLine;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// </summary>
        /// <param name="productId">The unique identifier for this product.</param>
        /// <param name="entity">Entity name</param>
        /// <param name="productNum">Product number</param>
        /// <param name="productDesc">Product description</param>
        /// <param name="status">Product status</param>
        /// <param name="revision">Product revision.</param>
        /// <param name="productVerNum">Product version number</param>
        /// <param name="productVerDesc">Product version description</param>
        /// <param name="redeemOnePerHostFlag">Redeem one per host flag</param>
        /// <param name="orderable">Orderable flag</param>
        /// <param name="allowRehosting">Allow rehosting flag</param>
        /// <param name="hostIdHelpUrl">Host ID Help URL</param>
        /// <param name="renewableLicense">Renewable license flag</param>
        //// <param name="quantity">Quantity Required</param>
        [SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "11#", Justification = "Not enough time to refactor.")]
        public Product(
                       int productId,
                       string entity,
                       string productNum,
                       string productDesc,
                       string status,
                       string revision,
                       uint productVerNum,
                       string productVerDesc,
                       string redeemOnePerHostFlag,
                       string orderable,
                       string allowRehosting,
                       string hostIdHelpUrl,
                       string renewableLicense)
        //// string lineNumber,
        //// string quantity)
        {
            this.ProductId = Convert.ToInt32(productId.ToString());
            this.EntityName = entity;
            this.ProductNum = productNum;
            this.ProductDesc = productDesc;
            this.Status = status;
            this.Revision = revision;
            this.VersionId = productVerNum;
            this.VersionDescription = productVerDesc;

            ////this.LineNumber = lineNumber;
            ////this.Quantity = quantity;

            if (!string.IsNullOrEmpty(redeemOnePerHostFlag))
            {
                this.LimitQuantityToOnePerHost = redeemOnePerHostFlag == "Y";
            }
            else
            {
                this.LimitQuantityToOnePerHost = false;
            }

            if (!string.IsNullOrEmpty(orderable))
            {
                this.OrderableFlag = orderable == "Y" ? true : false;
            }
            else
            {
                this.OrderableFlag = false;
            }

            if (!string.IsNullOrEmpty(allowRehosting))
            {
                this.AllowRehostingFlag = allowRehosting == "Y" ? true : false;
            }
            else
            {
                this.AllowRehostingFlag = false;
            }

            if (!string.IsNullOrEmpty(hostIdHelpUrl))
            {
                try
                {
                    Uri uri = new Uri(hostIdHelpUrl);
                    this.HostIdHelpUrl = uri;
                }
                catch (UriFormatException ufe)
                {
                   throw new BusinessEntitiesException("Product HostID Help URL string is badly formatted.", ufe);
                }
            }

            if (!string.IsNullOrEmpty(renewableLicense))
            {
                this.RenewableLicense = renewableLicense == "Y" ? true : false;
            }
            else
            {
                this.RenewableLicense = false;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// </summary>    
        /// <param name="productId">Product Id</param>
        /// <param name="productNum">Product number</param>
        /// <param name="productDesc">Product description</param>       
        /// <param name="seed1">Feature of the product</param>  
        /// <param name="partNum">part number of the product</param>  
        /// <param name="switchboardModel">switchboard Model of the product</param>  
        /// <param name="switchboardOption">switchboard Option of the product</param>  
        /// <param name="version">version of the product</param>
        /// <param name="revision">revision of the product</param> 
        /// <param name="prodLine">product line of the product</param> 
        /// <param name="noStoreFlag">no store flag of the product</param> 
        [SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "11#", Justification = "Not enough time to refactor.")]
        public Product(
                       int productId,
                       string productNum,
                       string productDesc,
                       string seed1,
                       string partNum,
                       string switchboardModel,
                       string switchboardOption,
                       uint version,
                       string revision,
                       string prodLine,
                       string noStoreFlag)
        {
            this.ProductId = Convert.ToInt32(productId.ToString());
            this.ProductNum = productNum;
            this.ProductDesc = productDesc;
            this.Seed1 = seed1;
            this.PartNumber = partNum;
            this.SwitchboardModel = switchboardModel;
            this.SwitchboardOption = switchboardOption;
            this.VersionId = version;
            this.Revision = revision;
            this.ProdLine = prodLine;
            this.NoStoreFlag = noStoreFlag;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// </summary>
        /// <param name="productId">The unique identifier for this product.</param>
        /// <param name="entity">Entity name</param>
        /// <param name="productNum">Product number</param>
        /// <param name="productDesc">Product description</param>
        /// <param name="status">Product status</param>
        /// <param name="revision">Product revision.</param>
        /// <param name="productVerNum">Product version number</param>
        /// <param name="productVerDesc">Product version description</param>
        /// <param name="redeemOnePerHostFlag">Redeem one per host flag</param>
        /// <param name="orderable">Orderable flag</param>
        /// <param name="allowRehosting">Allow rehosting flag</param>
        /// <param name="hostIdHelpUrl">Host ID Help URL</param>
        /// <param name="renewableLicense">Renewable license flag</param>
        /// <param name="trialduration">Trial duration</param>
        /// <param name="trialdescription">Trial description</param>
        /// <param name="disableautoactivation">Disable Auto Activation</param>
        [SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "11#", Justification = "Not enough time to refactor.")]
        public Product(
                       int productId,
                       string entity,
                       string productNum,
                       string productDesc,
                       string status,
                       string revision,
                       uint productVerNum,
                       string productVerDesc,
                       string redeemOnePerHostFlag,
                       string orderable,
                       string allowRehosting,
                       string hostIdHelpUrl,
                       string renewableLicense,
                       string trialduration,
                       string trialdescription,
                       string disableautoactivation)
        {
            this.ProductId = Convert.ToInt32(productId.ToString());
            this.EntityName = entity;
            this.ProductNum = productNum;
            this.ProductDesc = productDesc;
            this.Status = status;
            this.Revision = revision;
            this.VersionId = productVerNum;
            this.VersionDescription = productVerDesc;
            this.TrialDuration = trialduration;
            this.TrialDescription = trialdescription;
            this.DisableAutoActivation = disableautoactivation;

            if (!string.IsNullOrEmpty(redeemOnePerHostFlag))
            {
                this.LimitQuantityToOnePerHost = redeemOnePerHostFlag == "Y";
            }
            else
            {
                this.LimitQuantityToOnePerHost = false;
            }

            if (!string.IsNullOrEmpty(orderable))
            {
                this.OrderableFlag = orderable == "Y" ? true : false;
            }
            else
            {
                this.OrderableFlag = false;
            }

            if (!string.IsNullOrEmpty(allowRehosting))
            {
                this.AllowRehostingFlag = allowRehosting == "Y" ? true : false;
            }
            else
            {
                this.AllowRehostingFlag = false;
            }

            if (!string.IsNullOrEmpty(hostIdHelpUrl))
            {
                try
                {
                    Uri uri = new Uri(hostIdHelpUrl);
                    this.HostIdHelpUrl = uri;
                }
                catch (UriFormatException ufe)
                {
                    throw new BusinessEntitiesException("Product HostID Help URL string is badly formatted.", ufe);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// </summary>
        /// <param name="productId">The unique identifier for this product.</param>
        /// <param name="entity">Entity name</param>
        /// <param name="productNum">Product number</param>
        /// <param name="productDesc">Product description</param>
        /// <param name="status">Product status</param>
        /// <param name="revision">Product revision.</param>
        /// <param name="productVerNum">Product version number</param>
        /// <param name="productVerDesc">Product version description</param>
        /// <param name="redeemOnePerHostFlag">Redeem one per host flag</param>
        /// <param name="orderable">Orderable flag</param>
        /// <param name="allowRehosting">Allow rehosting flag</param>
        /// <param name="hostIdHelpUrl">Host ID Help URL</param>
        /// <param name="renewableLicense">Renewable license flag</param>
        /// <param name="rmu">Rmu number</param>
        /// <param name="lineNumber">Line Number</param>
        /// <param name="quantity">Quantity Required</param>
        /// <param name="quantityredeemed">QuantityRedeemed Required</param>
        /// <param name="linkId">Link Id</param>
        /// <param name="endDate">End Date</param>
        /// <param name="orderLineId">OrderLineId Required</param>
        [SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "11#", Justification = "Not enough time to refactor.")]

        public Product(
                       int productId,
                       string entity,
                       string productNum,
                       string productDesc,
                       string status,
                       string revision,
                       uint productVerNum,
                       string productVerDesc,
                       string redeemOnePerHostFlag,
                       string orderable,
                       string allowRehosting,
                       string hostIdHelpUrl,
                       string renewableLicense,
                       long rmu,
                       string lineNumber,
                      string quantity,
                      string quantityredeemed,
                      long linkId,
                      DateTime endDate,
                      int orderLineId)
        {
            this.ProductId = Convert.ToInt32(productId.ToString());
            this.EntityName = entity;
            this.ProductNum = productNum;
            this.ProductDesc = productDesc;
            this.Status = status;
            this.Revision = revision;
            this.VersionId = productVerNum;
            this.VersionDescription = productVerDesc;
            this.LineNumber = lineNumber;
            this.Quantity = quantity;
            this.QuantityRedeemed = quantityredeemed;
            this.OrderLineID = orderLineId;
            this.LinkId = linkId;
            this.EndDate = endDate;
            this.Rmu = rmu;

            if (!string.IsNullOrEmpty(redeemOnePerHostFlag))
            {
                this.LimitQuantityToOnePerHost = redeemOnePerHostFlag == "Y";
            }
            else
            {
                this.LimitQuantityToOnePerHost = false;
            }

            if (!string.IsNullOrEmpty(orderable))
            {
                this.OrderableFlag = orderable == "Y" ? true : false;
            }
            else
            {
                this.OrderableFlag = false;
            }

            if (!string.IsNullOrEmpty(allowRehosting))
            {
                this.AllowRehostingFlag = allowRehosting == "Y" ? true : false;
            }
            else
            {
                this.AllowRehostingFlag = false;
            }

            if (!string.IsNullOrEmpty(hostIdHelpUrl))
            {
                try
                {
                    Uri uri = new Uri(hostIdHelpUrl);
                    this.HostIdHelpUrl = uri;
                }
                catch (UriFormatException ufe)
                {
                    throw new BusinessEntitiesException("Product HostID Help URL string is badly formatted.", ufe);
                }
            }

            if (!string.IsNullOrEmpty(renewableLicense))
            {
                this.RenewableLicense = renewableLicense == "Y" ? true : false;
            }
            else
            {
                this.RenewableLicense = false;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// </summary>
        /// <param name="productId">The unique identifier for this product.</param>
        /// <param name="entity">Entity name</param>
        /// <param name="productNum">Product number</param>
        /// <param name="status">Product status</param>
        /// <param name="trialdescription">Trial Description</param>
        /// <param name="trialduration">Trial Duration</param>
        /// <param name="trialappid">Trial AppId</param>
        [SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "11#", Justification = "Not enough time to refactor.")]
        public Product(
                       int productId,
                       string entity,
                       string productNum,
                       string status,
                      string trialdescription,
                      string trialduration,
                      int trialappid)
        {
            this.ProductId = Convert.ToInt32(productId.ToString());
            this.EntityName = entity;
            this.ProductNum = productNum;
            this.Status = status;
            this.TrialDescription = trialdescription;
            this.TrialDuration = trialduration;
            this.TrialAppId = trialappid;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether rehosting is allowed for this product.
        /// </summary>
        public bool AllowRehostingFlag { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether renew is allowed for this product.
        /// </summary>
        public bool RenewableLicenseFlag { get; set; }

        /// <summary>
        /// Gets or sets the entity (business group) this product belongs to.
        /// </summary>
        public string EntityName { get; set; }

        /// <summary>
        /// Gets or sets the host id help URL for this product (optional).
        /// </summary>
        /// <value>The host id help URL.</value>
        public Uri HostIdHelpUrl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the quantity should be limited to one per host when this license product is assigned to a host.
        /// </summary>
        /// <value>
        ///    <c>true</c> if the quantity should be limited to one per host; otherwise, <c>false</c>.
        /// </value>
        public bool LimitQuantityToOnePerHost { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this product is orderable.
        /// </summary>
        public bool OrderableFlag { get; set; }

        /// <summary>
        /// Gets or sets the this product's unique Id
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the Product Number
        /// </summary>
        public string ProductNum { get; set; }

        /// <summary>
        /// Gets or sets the Product Description
        /// </summary>
        public string ProductDesc { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the RenewableLicense property
        /// </summary>
        public bool RenewableLicense { get; set; }

        /// <summary>
        /// Gets or sets the Product Status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the Product feature
        /// </summary>
        public string Seed1 { get; set; }

        /// <summary>
        /// Gets or sets the Product certificate part number
        /// </summary>
        public string PartNumber { get; set; }

        /// <summary>
        /// Gets or sets the Product switchboard model
        /// </summary>
        public string SwitchboardModel { get; set; }

        /// <summary>
        /// Gets or sets the Product switchboard option
        /// </summary>
        public string SwitchboardOption { get; set; }

        /// <summary>
        /// Gets or sets the product revision
        /// </summary>
        public string Revision { get; set; }

        /// <summary>
        /// Gets or sets the product line
        /// </summary>
        public string ProdLine { get; set; }

        /// <summary>
        ///  Gets or sets a value for this product is essential.
        /// </summary>
        public string NoStoreFlag { get; set; }

        /// <summary>
        ///  Gets or sets a value for this product RMU value.
        /// </summary>
        public long Rmu { get; set; }

        /// <summary>
        /// Gets or sets the End date of the product
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the Warranty of the product
        /// </summary>
        public string Warranty { get; set; }

        /// <summary>
        ///  Gets or sets a value for this product LinkParentProductId value.
        /// </summary>
        public string LinkParentProductId { get; set; }

        /// <summary>
        ///  Gets or sets a value for this product Duration value.
        /// </summary>
        public int Duration { get; set; }

        /// <summary>
        ///  Gets or sets a value for this product LinkId value.
        /// </summary>
        public long LinkId { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether selected product is parent or child.
        /// </summary>
        public bool IsParent { get; set; }

        /// <summary>
        /// Gets a value indicating whether this product is valid.
        /// Logic in this method should exactly correspond with Validate method.
        /// </summary>
        public bool Valid
        {
            get
            {
                if (string.IsNullOrEmpty(this.ProductNum))
                {
                    return false;
                }

                if (string.IsNullOrEmpty(this.EntityName))
                {
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Gets or sets the Version Id
        /// </summary>
        public uint VersionId { get; set; }

        /// <summary>
        /// Gets or sets the Version Description
        /// </summary>
        public string VersionDescription
        {
            get
            {
                return this.versionDescription;
            }

            set
            {
                // this string shouldn't be null, it gets bound to the GUI
                this.versionDescription = string.IsNullOrEmpty(value) ? string.Empty : value;
            }
        }

        /// <summary>
        /// Gets or sets the LineNumber
        /// </summary>
        public string LineNumber { get; set; }

        /// <summary>
        /// Gets or sets the Quantity
        /// </summary>
        public string Quantity { get; set; }

        /// <summary>
        /// Gets or sets the QuantityRedeemed
        /// </summary>
        public string QuantityRedeemed { get; set; }

        /// <summary>
        /// Gets or sets the OrderID
        /// </summary>
        public int OrderLineID { get; set; }

        /// <summary>
        /// Gets or sets the TrialDescription
        /// </summary>
        public string TrialDescription { get; set; }

        /// <summary>
        /// Gets or sets the TrialDuration
        /// </summary>
        public string TrialDuration { get; set; }

        /// <summary>
        /// Gets or sets the TrialAppId
        /// </summary>
        public int TrialAppId { get; set; }

        /// <summary>
        /// Gets or sets the DisableAutoActivation
        /// </summary>
        public string DisableAutoActivation { get; set; }

        /// <summary>
        /// Gets or sets the EntitelementType
        /// </summary>
        public string EntitelementType { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="u">The left hand side.</param>
        /// <param name="v">The right hand side.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(Product u, Product v)
        {
            if (object.ReferenceEquals(u, v))
            {
                return true;
            }
            else if (object.ReferenceEquals(u, null))
            {
                return false;
            }
            else if (object.ReferenceEquals(v, null))
            {
                return false;
            }

            return u.ProductId == v.ProductId &&
                    u.EntityName == v.EntityName &&
                    u.ProductNum == v.ProductNum &&
                    u.ProductDesc == v.ProductDesc &&
                    u.Status == v.Status &&
                    u.Revision == v.Revision &&
                    u.VersionId == v.VersionId &&
                    u.VersionDescription == v.VersionDescription;
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="u">The left hand side.</param>
        /// <param name="v">The right hand side.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(Product u, Product v)
        {
            return !(u == v);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            Product arg = obj as Product;
            if (object.ReferenceEquals(arg, null))
            {
                return false;
            }
            else
            {
                return this == arg;
            }
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return this.ProductId.GetHashCode();
        }

        

        /// <summary>
        /// Gets the display text with version.
        /// </summary>
        /// <returns>
        /// The product number, description and version as a string.
        /// </returns>
        public string GetDisplayTextWithVersion()
        {
            string value = this.ProductNum + " " + this.ProductDesc;
            if (string.IsNullOrEmpty(this.VersionDescription))
            {
                return value;
            }

            string version = " - " + this.VersionDescription;
            try
            {
                // if the conversion to a double succeeds, add a "v"
                Convert.ToDouble(this.VersionDescription, CultureInfo.InvariantCulture);
                version = " - v" + this.VersionDescription;
            }
            catch (FormatException)
            {
            }
            catch (OverflowException)
            {
            }

            return value + version;
        }

        #endregion
    }
}
