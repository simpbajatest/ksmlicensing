﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Lambda.Entities
{
    public class LambdaResponse
    {
        public HttpStatusCode statusCode { get; set; }
        public string body { get; set; }
    }
}
