﻿namespace Lambda.Entities
{
    using System;
    using System.Globalization;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    /// <summary>
    /// This is the base class for all Id's.  Contains the basic
    /// properties and operators.
    /// </summary>
    [Serializable]
    public class IdBase : IComparable, ISerializable
    {
        #region Fields

        /// <summary>
        /// The number.
        /// </summary>
        private long id;

        /// <summary>
        /// True if the id is valid. (greater than 0)
        /// </summary>
        private bool valid;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the IdBase class.
        /// </summary>
        public IdBase()
        {
        }

        /// <summary>
        /// Initializes a new instance of the IdBase class.
        /// </summary>
        /// <param name="id">A string representation of the id.</param>
        public IdBase(string id)
        {
            this.Id = Convert.ToInt64(id, CultureInfo.InvariantCulture);
            this.IdString = id;
        }

        /// <summary>
        /// Initializes a new instance of the IdBase class.
        /// </summary>
        /// <param name="id">The number.</param>
        public IdBase(long id)
        {
            this.Id = id;
            this.IdString = Convert.ToString(id);
        }

        /// <summary>
        /// Initializes a new instance of the IdBase class. (supports serialization)
        /// </summary>
        /// <param name="info">Info to deserialize.</param>
        /// <param name="context">Streaming context.</param>
        protected IdBase(SerializationInfo info, StreamingContext context)
        {
            this.IdString = Convert.ToString(info.GetValue("IdString", typeof(string)));
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        public long Id
        {
            get
            {
                return this.id;
            }

            set
            {
                this.id = value;
                this.valid = value > 0;
            }
        }

        /// <summary>
        /// Gets or sets the string representation of the Id.
        /// </summary>
        public string IdString
        {
            get
            {
                return this.Id.ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            }

            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    try
                    {
                        this.Id = Convert.ToInt64(value, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                    }
                    catch (FormatException e)
                    {
                        throw new ArgumentOutOfRangeException(
                            string.Format("Not a valid id ({0})", value),
                            e);
                    }
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the Id is valid.
        /// </summary>
        public bool Valid
        {
            get
            {
                return this.valid;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Checks for equality of the two Ids.
        /// </summary>
        /// <param name="u">The first Id.</param>
        /// <param name="v">The second Id.</param>
        /// <returns>True if the Id's contain the same values.</returns>
        public static bool operator ==(IdBase u, IdBase v)
        {
            if (object.ReferenceEquals(u, v))
            {
                return true;
            }
            else if (object.ReferenceEquals(u, null))
            {
                return false;
            }
            else if (object.ReferenceEquals(v, null))
            {
                return false;
            }

            return
                u.Id == v.Id &&
                u.Valid == v.Valid;
        }

        /// <summary>
        /// Compares inequality of the two Id's.
        /// </summary>
        /// <param name="u">The first Id.</param>
        /// <param name="v">The second Id.</param>
        /// <returns>True if the Id's are NOT equal.</returns>
        public static bool operator !=(IdBase u, IdBase v)
        {
            return !(u == v);
        }

        /// <summary>
        /// Checks for equality of the two Ids.
        /// </summary>
        /// <param name="obj">Id to compare.</param>
        /// <returns>True if the Id's are equal.</returns>
        public override bool Equals(object obj)
        {
            IdBase arg = obj as IdBase;
            if (object.ReferenceEquals(arg, null))
            {
                return false;
            }
            else
            {
                return this == arg;
            }
        }

        /// <summary>
        /// Gets the hashcode for this Id.
        /// </summary>
        /// <returns>The hashcode for the Id.</returns>
        public override int GetHashCode()
        {
            int hashcode = this.Id.GetHashCode();
            return hashcode;
        }

        /// <summary>
        /// The string representation of the Id.
        /// </summary>
        /// <returns>The formatted string form of the id</returns>
        public override string ToString()
        {
            if (this.Valid)
            {
                return this.IdString;
            }
            else
            {
                return "-1";
            }
        }

        /// <summary>
        /// Comparator for two id's. Returns an indication of their relative values,
        /// to be used in sorting.
        /// </summary>
        /// <param name="value">Id to compare to.</param>
        /// <returns>0 if equal, negative if parameter is less, positive if parameter is more</returns>
        public int CompareTo(IdBase value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            if (!this.Valid || !value.Valid)
            {
                throw new ArgumentOutOfRangeException("Can not compare invalid values");
            }

            return this.id.CompareTo(value.Id);
        }

        /// <summary>
        /// Provided to support serialization.
        /// </summary>
        /// <param name="info">The object to add the data to.</param>
        /// <param name="context">The streaming context.</param>
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            string idString = this.IdString;
            if (!string.IsNullOrEmpty(idString))
            {
                info.AddValue("IdString", idString);
            }
        }

        #endregion

        #region IComparable Members

        /// <summary>
        /// Compares the current instance with another object of the same type and returns an integer that indicates whether the current instance precedes, follows, or occurs in the same position in the sort order as the other object.
        /// </summary>
        /// <param name="obj">An object to compare with this instance.</param>
        /// <returns>
        /// A 32-bit signed integer that indicates the relative order of the objects being compared. The return value has these meanings: Value Meaning Less than zero This instance precedes <paramref name="obj"/> in the sort order. Zero This instance occurs in the same position in the sort order as <paramref name="obj"/>. Greater than zero This instance follows <paramref name="obj"/> in the sort order.
        /// </returns>
        /// <exception cref="T:System.ArgumentException"><paramref name="obj"/> is not the same type as this instance. </exception>
        public int CompareTo(object obj)
        {
            return this.CompareTo(obj as IdBase);
        }

        #endregion
    }
}