﻿using System.Collections.Generic;
using System.Linq;

namespace Lambda.Entities
{
    /// <summary>
    /// Contains a collection of products.
    /// </summary>
    public class ProductCollection : BusinessCollectionBase<Product>
    {
        /// <summary>
        /// Initializes a new instance of the ProductCollection class.
        /// </summary>
        public ProductCollection()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ProductCollection class.
        /// </summary>
        /// <param name="initialList">The initial list of products</param>
        public ProductCollection(IList<Product> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Add these products to this collection if they are not
        /// already in this collection.
        /// </summary>
        /// <param name="products">The products to add.</param>
        public void AddRange(ProductCollection products)
        {
            if (products != null)
            {
                foreach (Product product in products)
                {
                    // if this product id is not already in the list
                    // then add this product
                    if (!this.Items.Any(p => p.ProductId == product.ProductId))
                    {
                        this.Items.Add(product);
                    }
                }
            }
        }
    }
}
