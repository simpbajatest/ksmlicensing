﻿//-----------------------------------------------------------------------
// <copyright file="ProductFeaturesCollection.cs" company="Keysight Technologies, Inc.">
//      Copyright (C) Keysight Technologies, Inc.
// </copyright>
//-----------------------------------------------------------------------
namespace Lambda.Entities
{
    using System.Collections.Generic;

    /// <summary>
    /// Defines a collection of ProductFeatures objects
    /// </summary>
    public class ProductFeaturesCollection : BusinessCollectionBase<ProductFeatures>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ProductFeaturesCollection class.
        /// </summary>
        /// <param name="initialList">The initial list of ProductFeaturess.</param>
        public ProductFeaturesCollection(IList<ProductFeatures> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the ProductFeaturesCollection class.
        /// </summary>
        public ProductFeaturesCollection()
            : base()
        {
        }

        #endregion
    }
}
