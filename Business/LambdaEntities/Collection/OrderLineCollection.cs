﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lambda.Entities
{ 

    /// <summary>
    /// Public class on order lines
    /// </summary>
    public class OrderLineCollection : BusinessCollectionBase<OrderLine>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderLineCollection"/> class.
        /// </summary>
        /// <param name="initialList">The initial list of order lines.</param>
        public OrderLineCollection(IList<OrderLine> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderLineCollection"/> class.
        /// </summary>
        public OrderLineCollection()
            : base()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Compares the two collections for content equality.
        /// </summary>
        /// <param name="left">left side operand</param>
        /// <param name="right">right side operand</param>
        /// <returns>Are they equal</returns>
        public static bool operator ==(OrderLineCollection left, OrderLineCollection right)
        {
            if (object.ReferenceEquals(left, right))
            {
                return true;
            }
            else if (object.ReferenceEquals(left, null))
            {
                return false;
            }
            else if (object.ReferenceEquals(right, null))
            {
                return false;
            }

            if (left.Count != right.Count)
            {
                return false;
            }

            foreach (OrderLine orderLine in left)
            {
                if (!right.Contains(orderLine))
                {
                    return false;
                }
            }

            foreach (OrderLine orderLine in right)
            {
                if (!left.Contains(orderLine))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Compares for inequality.
        /// Returns true if the data in the two collections are not equal.
        /// </summary>
        /// <param name="left">left side operand</param>
        /// <param name="right">right side operand</param>
        /// <returns>Are they unequal</returns>
        public static bool operator !=(OrderLineCollection left, OrderLineCollection right)
        {
            return !(left == right);
        }

        /// <summary>
        /// Compares the two order lines to see if they have equal contents.
        /// </summary>
        /// <param name="obj">object to compare</param>
        /// <returns>Are they equal</returns>
        public override bool Equals(object obj)
        {
            OrderLineCollection arg = obj as OrderLineCollection;
            if (object.ReferenceEquals(arg, null))
            {
                return false;
            }
            else
            {
                return this == arg;
            }
        }

        /// <summary>
        /// Gets the hashcode for this collection.
        /// </summary>
        /// <returns>hash code based on order line id</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            if (!this.Any())
            {
                return "(empty)";
            }

            StringBuilder orderLineIds = null;
            foreach (OrderLine orderLine in this)
            {
                if (orderLineIds == null)
                {
                    orderLineIds = new StringBuilder();
                }
                else
                {
                    orderLineIds.Append(",");
                }

                orderLineIds.Append(orderLine.OrderLineId);
            }

            return orderLineIds.ToString();
        }

        #endregion
    }
}
