﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Lambda.Entities
{

    /// <summary>
    /// Base class for collections of business entity objects.
    /// Ideas and some code for this taken from Imar Spaanjaars
    /// http://imar.spaanjaars.com/QuickDocId.aspx?quickdoc=476
    /// </summary>
    /// <typeparam name="T">The type of item in the collection</typeparam>
    public abstract class BusinessCollectionBase<T> : Collection<T> where T : BusinessEntityBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessCollectionBase&lt;T&gt;"/> class.
        /// </summary>
        public BusinessCollectionBase()
            : base(new List<T>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessCollectionBase&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="initialList">The initial list.</param>
        public BusinessCollectionBase(IList<T> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Adds the items in the specified collection to this collection.
        /// (Does not check for duplicates or do anyting special.)
        /// </summary>
        /// <param name="itemsToAdd">Items to add to this collection.</param>
        public void AddRange(BusinessCollectionBase<T> itemsToAdd)
        {
            if (itemsToAdd != null)
            {
                foreach (T item in itemsToAdd)
                {
                    this.Add(item);
                }
            }
        }

        /// <summary>
        /// Sorts the specified comparer.
        /// </summary>
        /// <param name="comparer">The comparer.</param>
        public void Sort(IComparer<T> comparer)
        {
            if (comparer == null)
            {
                throw new ArgumentNullException("comparer", "Comparer is null.");
            }

            // cast to a List
            List<T> list = this.Items as List<T>;
            if (list == null)
            {
                return;
            }

            // sort the list using the built-in List sort
            list.Sort(comparer);
        }
    }
}
