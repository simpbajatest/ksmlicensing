﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lambda.Entities
{

    /// <summary>
    /// Public class on order lines
    /// </summary>
    public class ReadEntitlementCollection : BusinessCollectionBase<ReadEntitlement>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ReadEntitlementCollection"/> class.
        /// </summary>
        /// <param name="initialList">The initial list of order lines.</param>
        public ReadEntitlementCollection(IList<ReadEntitlement> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReadEntitlementCollection"/> class.
        /// </summary>
        public ReadEntitlementCollection()
            : base()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Compares the two collections for content equality.
        /// </summary>
        /// <param name="left">left side operand</param>
        /// <param name="right">right side operand</param>
        /// <returns>Are they equal</returns>
        public static bool operator ==(ReadEntitlementCollection left, ReadEntitlementCollection right)
        {
            if (object.ReferenceEquals(left, right))
            {
                return true;
            }
            else if (object.ReferenceEquals(left, null))
            {
                return false;
            }
            else if (object.ReferenceEquals(right, null))
            {
                return false;
            }

            if (left.Count != right.Count)
            {
                return false;
            }

            foreach (ReadEntitlement orderLine in left)
            {
                if (!right.Contains(orderLine))
                {
                    return false;
                }
            }

            foreach (ReadEntitlement orderLine in right)
            {
                if (!left.Contains(orderLine))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Compares for inequality.
        /// Returns true if the data in the two collections are not equal.
        /// </summary>
        /// <param name="left">left side operand</param>
        /// <param name="right">right side operand</param>
        /// <returns>Are they unequal</returns>
        public static bool operator !=(ReadEntitlementCollection left, ReadEntitlementCollection right)
        {
            return !(left == right);
        }

        /// <summary>
        /// Compares the two order lines to see if they have equal contents.
        /// </summary>
        /// <param name="obj">object to compare</param>
        /// <returns>Are they equal</returns>
        public override bool Equals(object obj)
        {
            ReadEntitlementCollection arg = obj as ReadEntitlementCollection;
            if (object.ReferenceEquals(arg, null))
            {
                return false;
            }
            else
            {
                return this == arg;
            }
        }

        /// <summary>
        /// Gets the hashcode for this collection.
        /// </summary>
        /// <returns>hash code based on order line id</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

       
        #endregion
    }
}
