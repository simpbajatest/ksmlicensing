﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lambda.Entities
{ 

    /// <summary>
    /// Order Collection
    /// </summary>
    public class OrderCollection : BusinessCollectionBase<Order>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderCollection"/> class.
        /// </summary>
        /// <param name="initialList">The initial list of orders.</param>
        public OrderCollection(IList<Order> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderCollection"/> class.
        /// </summary>
        public OrderCollection()
            : base()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Compares the two collections for content equality.
        /// </summary>
        /// <param name="left">left side operand</param>
        /// <param name="right">right side operand</param>
        /// <returns>Are they equal</returns>
        public static bool operator ==(OrderCollection left, OrderCollection right)
        {
            if (object.ReferenceEquals(left, right))
            {
                return true;
            }
            else if (object.ReferenceEquals(left, null))
            {
                return false;
            }
            else if (object.ReferenceEquals(right, null))
            {
                return false;
            }

            if (left.Count != right.Count)
            {
                return false;
            }

            foreach (Order order in left)
            {
                if (!right.Contains(order))
                {
                    return false;
                }
            }

            foreach (Order order in right)
            {
                if (!left.Contains(order))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Compares for inequality.
        /// Returns true if the data in the two collections are not equal.
        /// </summary>
        /// <param name="left">left side operand</param>
        /// <param name="right">right side operand</param>
        /// <returns>Are they unequal</returns>
        public static bool operator !=(OrderCollection left, OrderCollection right)
        {
            return !(left == right);
        }

        /// <summary>
        /// Compares the two orders to see if they have equal contents.
        /// </summary>
        /// <param name="obj">object to compare</param>
        /// <returns>Are they equal</returns>
        public override bool Equals(object obj)
        {
            OrderCollection arg = obj as OrderCollection;
            if (object.ReferenceEquals(arg, null))
            {
                return false;
            }
            else
            {
                return this == arg;
            }
        }

        /// <summary>
        /// Gets the hashcode for this collection.
        /// </summary>
        /// <returns>hash code based on order line id</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            if (!this.Any())
            {
                return "(empty)";
            }

            StringBuilder orderIds = null;
            foreach (Order order in this)
            {
                if (orderIds == null)
                {
                    orderIds = new StringBuilder();
                }
                else
                {
                    orderIds.Append(",");
                }

                orderIds.Append(order.OrderId.ToString());
            }

            return orderIds.ToString();
        }

        #endregion
    }
}
