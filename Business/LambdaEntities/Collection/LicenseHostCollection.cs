﻿//-----------------------------------------------------------------------
// <copyright file="LicenseHostCollection.cs" company="Keysight Technologies, Inc.">
//      Copyright (C) Keysight Technologies, Inc.
// </copyright>
//-----------------------------------------------------------------------
namespace Lambda.Entities
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Agreement Collection
    /// </summary>
    public class LicenseHostCollection : BusinessCollectionBase<LicenseHost>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseHostCollection"/> class.
        /// </summary>
        /// <param name="initialList">The initial list of LicenseHosts.</param>
        public LicenseHostCollection(IList<LicenseHost> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseHostCollection"/> class.
        /// </summary>
        public LicenseHostCollection()
            : base()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Compares the two collections for content equality.
        /// </summary>
        /// <param name="left">left side operand</param>
        /// <param name="right">right side operand</param>
        /// <returns>Are they equal</returns>
        public static bool operator ==(LicenseHostCollection left, LicenseHostCollection right)
        {
            if (object.ReferenceEquals(left, right))
            {
                return true;
            }
            else if (object.ReferenceEquals(left, null))
            {
                return false;
            }
            else if (object.ReferenceEquals(right, null))
            {
                return false;
            }

            if (left.Count != right.Count)
            {
                return false;
            }

            foreach (LicenseHost LicenseHost in left)
            {
                if (!right.Contains(LicenseHost))
                {
                    return false;
                }
            }

            foreach (LicenseHost LicenseHost in right)
            {
                if (!left.Contains(LicenseHost))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Compares for inequality.
        /// Returns true if the data in the two collections are not equal.
        /// </summary>
        /// <param name="left">left side operand</param>
        /// <param name="right">right side operand</param>
        /// <returns>Are they unequal</returns>
        public static bool operator !=(LicenseHostCollection left, LicenseHostCollection right)
        {
            return !(left == right);
        }

        /// <summary>
        /// Compares the two LicenseHosts to see if they have equal contents.
        /// </summary>
        /// <param name="obj">object to compare</param>
        /// <returns>Are they equal</returns>
        public override bool Equals(object obj)
        {
            LicenseHostCollection arg = obj as LicenseHostCollection;
            if (object.ReferenceEquals(arg, null))
            {
                return false;
            }
            else
            {
                return this == arg;
            }
        }

        /// <summary>
        /// Gets the hashcode for this collection.
        /// </summary>
        /// <returns>hash code based on LicenseHost load id</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            if (!this.Any())
            {
                return "(empty)";
            }

            StringBuilder LicenseHostIds = null;
            foreach (LicenseHost LicenseHost in this)
            {
                if (LicenseHostIds == null)
                {
                    LicenseHostIds = new StringBuilder();
                }
                else
                {
                    LicenseHostIds.Append(",");
                }

                LicenseHostIds.Append(LicenseHost.HostId.ToString());
            }

            return LicenseHostIds.ToString();
        }

        #endregion
    }
}
