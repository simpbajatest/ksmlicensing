﻿//-----------------------------------------------------------------------
// <copyright file="SyncInsFulCollection.cs" company="Keysight Technologies, Inc.">
//      Copyright (C) Keysight Technologies, Inc.
// </copyright>
//-----------------------------------------------------------------------
namespace Lambda.Entities
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Agreement Collection
    /// </summary>
    public class SyncInsFulCollection : BusinessCollectionBase<SyncInstalledFulfillment>
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncInsFulCollection"/> class.
        /// </summary>
        /// <param name="initialList">The initial list of SyncInstalledFulfillments.</param>
        public SyncInsFulCollection(IList<SyncInstalledFulfillment> initialList)
            : base(initialList)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncInsFulCollection"/> class.
        /// </summary>
        public SyncInsFulCollection()
            : base()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Compares the two collections for content equality.
        /// </summary>
        /// <param name="left">left side operand</param>
        /// <param name="right">right side operand</param>
        /// <returns>Are they equal</returns>
        public static bool operator ==(SyncInsFulCollection left, SyncInsFulCollection right)
        {
            if (object.ReferenceEquals(left, right))
            {
                return true;
            }
            else if (object.ReferenceEquals(left, null))
            {
                return false;
            }
            else if (object.ReferenceEquals(right, null))
            {
                return false;
            }

            if (left.Count != right.Count)
            {
                return false;
            }

            foreach (SyncInstalledFulfillment SyncInstalledFulfillment in left)
            {
                if (!right.Contains(SyncInstalledFulfillment))
                {
                    return false;
                }
            }

            foreach (SyncInstalledFulfillment SyncInstalledFulfillment in right)
            {
                if (!left.Contains(SyncInstalledFulfillment))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Compares for inequality.
        /// Returns true if the data in the two collections are not equal.
        /// </summary>
        /// <param name="left">left side operand</param>
        /// <param name="right">right side operand</param>
        /// <returns>Are they unequal</returns>
        public static bool operator !=(SyncInsFulCollection left, SyncInsFulCollection right)
        {
            return !(left == right);
        }

        /// <summary>
        /// Compares the two SyncInstalledFulfillments to see if they have equal contents.
        /// </summary>
        /// <param name="obj">object to compare</param>
        /// <returns>Are they equal</returns>
        public override bool Equals(object obj)
        {
            SyncInsFulCollection arg = obj as SyncInsFulCollection;
            if (object.ReferenceEquals(arg, null))
            {
                return false;
            }
            else
            {
                return this == arg;
            }
        }

        /// <summary>
        /// Gets the hashcode for this collection.
        /// </summary>
        /// <returns>hash code based on SyncInstalledFulfillment load id</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            if (!this.Any())
            {
                return "(empty)";
            }

            StringBuilder SyncInstalledFulfillmentIds = null;
            foreach (SyncInstalledFulfillment SyncInstalledFulfillment in this)
            {
                if (SyncInstalledFulfillmentIds == null)
                {
                    SyncInstalledFulfillmentIds = new StringBuilder();
                }
                else
                {
                    SyncInstalledFulfillmentIds.Append(",");
                }

                SyncInstalledFulfillmentIds.Append(SyncInstalledFulfillment.ActivationCode.ToString());
            }

            return SyncInstalledFulfillmentIds.ToString();
        }

        #endregion
    }
}
