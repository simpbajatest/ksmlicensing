﻿using System;

using Oracle.ManagedDataAccess.Client;

namespace DatabaseLayer
{  

    /// <summary>
    /// Adds instance methods to the OracleDataReader class.
    /// </summary>
    public static class DataReaderExtension
    {
        /// <summary>
        /// Determines whether the specified data reader has the given column.
        /// </summary>
        /// <param name="dataReader">The data reader.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <returns>
        ///   <c>true</c> if the specified data reader has column; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasColumn(this OracleDataReader dataReader, string columnName)
        {
            for (int i = 0; i < dataReader.FieldCount; i++)
            {
                if (dataReader.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                {
                    // found a match
                    return true;
                }
            }

            // no match found
            return false;
        }
    }
}
