﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using Lambda.Entities;

namespace DatabaseLayer
{

    /// <summary>
    /// A static class for oracle database actions
    /// </summary>
    internal static class DatabaseAccessUtility
    {
        #region Methods

        #region Public Methods

        /// <summary>
        /// Executes an sql command that returns the first column of the first row in the result set returned by the query.
        /// </summary>
        /// <param name="sqlCommand">The sql syntax.</param>
        /// <param name="parameters">The parameters for this SQL</param>
        /// <returns>The result of the command as a string.</returns>
        public static string SqlGet(string sqlCommand, params OracleParameter[] parameters) // returns a string (scalar)
        {
            if (string.IsNullOrEmpty(sqlCommand))
            {
                throw new ArgumentException("sql command cannot be null nor empty");
            }

            try
            {
                using (OracleConnection cn = new OracleConnection(ConfigSettings.Connection))
                {
                    cn.Open();
                    using (OracleCommand mycommand = new OracleCommand(sqlCommand, cn))
                    {
                        if (parameters != null)
                        {
                            foreach (OracleParameter parameter in parameters)
                            {
                                if (parameter != null)
                                {
                                    mycommand.Parameters.Add(parameter);
                                }
                            }
                        }

                        object result = mycommand.ExecuteScalar();
                        mycommand.Parameters.Clear();
                        return result == null ? string.Empty : result.ToString();
                    }
                }
            }
            catch (Exception e)
            {
                throw new DataException(e.Message + " SQL: " + sqlCommand, e);
            }
        }

        /// <summary>
        /// Executes an sql command that returns an integer.
        /// New method is called SqlGetInt.  The new one throws a DataException.
        /// </summary>
        /// <param name="sqlCommand">The text of the query.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        /// an Int64 scalar
        /// </returns>
        public static long Int64SqlGet_Legacy(string sqlCommand, params OracleParameter[] parameters)
        {
            if (string.IsNullOrEmpty(sqlCommand))
            {
                throw new ArgumentException("sql command cannot be null nor empty");
            }

            string s = SqlGet(sqlCommand, parameters);
            if (string.IsNullOrEmpty(s) ||
                s.Equals("null", StringComparison.InvariantCultureIgnoreCase))
            {
                return 0;
            }

            return DataConversionUtility.StringToInt64(s);
        }

        /// <summary>
        /// NOTE:  This method throws a DataException if something goes wrong
        /// with the OracleException.  The legacy method does not.  (Int64SqlGet)
        /// Executes an sql command that returns an integer
        /// </summary>
        /// <param name="sqlCommand">The text of the query.</param>
        /// <param name="parameters">The parameters to use in the query.</param>
        /// <returns>an Int64 scalar</returns>
        /// <exception cref="DataException">If something goes wrong with the database query.</exception>
        public static long SqlGetInt(string sqlCommand, params OracleParameter[] parameters)
        {
            if (string.IsNullOrEmpty(sqlCommand))
            {
                throw new ArgumentException("sql command cannot be null nor empty");
            }

            object result;
            try
            {
                using (OracleConnection cn = new OracleConnection(ConfigSettings.Connection))
                {
                    cn.Open();
                    using (OracleCommand mycommand = new OracleCommand(sqlCommand, cn))
                    {
                        if (parameters != null &&
                            parameters.Length > 0)
                        {
                            mycommand.Parameters.AddRange(parameters);
                        }

                        result = mycommand.ExecuteScalar();
                        mycommand.Parameters.Clear();
                    }
                }
            }
            catch (Exception e)
            {
                throw new DataException(e.Message + " SQL: " + sqlCommand, e);
            }

            // if no records matched the criteria, return 0
            if (result == null ||
                string.IsNullOrEmpty(result.ToString()))
            {
                return 0;
            }

            return DataConversionUtility.StringToInt64(result.ToString());
        }

        /// <summary>
        /// Executes an sql command to get sequenceName.nextval
        /// Preappends "ASL_SCHEMA." if necessary
        /// Post appends ".nextval" all the time
        /// </summary>
        /// <param name="sequenceName">The name of the sequence.</param>
        /// <returns>A unique number to use as the primary key.</returns>
        public static long SqlGetSeq(string sequenceName)
        {
            if (string.IsNullOrEmpty(sequenceName))
            {
                throw new ArgumentException("sequence name cannot be null nor empty");
            }

            StringBuilder sb = new StringBuilder("select  ");
            if (!sequenceName.Contains("ASL_SCHEMA.") &&
                !sequenceName.Contains("asl_schema.") &&
                !sequenceName.Contains("LICENSE.") &&
                !sequenceName.Contains("license.") &&
                !sequenceName.Contains("TIBCO_INPUT.") &&
                !sequenceName.Contains("tibco_input."))
            {
                sb.Append("ASL_SCHEMA.");
            }

            sb.Append(sequenceName);
            sb.Append(".nextval from dual");

            using (OracleConnection cn = new OracleConnection(ConfigSettings.Connection))
            {
                cn.Open();
                using (OracleCommand mycommand = new OracleCommand(sb.ToString(), cn))
                {
                    return DataConversionUtility.StringToInt64(mycommand.ExecuteScalar().ToString());
                }
            }
        }

        /// <summary>
        /// Executes a sql command that "does" something, but doesn't return anything
        /// </summary>
        /// <param name="sql">The text of the query.</param>
        /// <param name="parameters">Optional parameters.  Must be bound by name.</param>
        public static void SqlDo(string sql, params OracleParameter[] parameters)
        {
            if (string.IsNullOrEmpty(sql))
            {
                throw new ArgumentException("sql command cannot be null nor empty");
            }

            try
            {
                using (OracleConnection cn = new OracleConnection(ConfigSettings.Connection))
                {
                    cn.Open();
                    using (OracleCommand command = new OracleCommand(sql, cn))
                    {
                        if (parameters != null)
                        {
                            foreach (OracleParameter parameter in parameters)
                            {
                                if (parameter != null)
                                {
                                    command.Parameters.Add(parameter);
                                }
                            }
                        }

                        command.BindByName = true;
                        command.ExecuteNonQuery();
                        command.Parameters.Clear();
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {
                throw new DataException(e.Message + " SQL: " + sql, e);
            }
        }

        /// <summary>
        /// Executes a sql command that "does" something, but doesn't return anything
        /// </summary>
        /// <param name="sql">The text of the query.</param>
        /// <param name="parameters">Optional parameters.  Must be bound by name.</param>
        public static void SqlDoArray(string sql, int count, params OracleParameter[] parameters)
        {
            if (string.IsNullOrEmpty(sql))
            {
                throw new ArgumentException("sql command cannot be null nor empty");
            }

            try
            {
                using (OracleConnection cn = new OracleConnection(ConfigSettings.Connection))
                {
                    cn.Open();
                    using (OracleCommand command = new OracleCommand(sql, cn))
                    {
                        if (parameters != null)
                        {
                            foreach (OracleParameter parameter in parameters)
                            {
                                if (parameter != null)
                                {
                                    command.Parameters.Add(parameter);
                                }
                            }
                        }

                        command.ArrayBindCount = count;
                        command.BindByName = true;
                        command.ExecuteNonQuery();
                        command.Parameters.Clear();
                    }
                    cn.Close();
                }
            }
            catch (Exception e)
            {
                throw new DataException(e.Message + " SQL: " + sql, e);
            }
        }

        /// <summary>
        /// Executes a sql command that "does" something, but doesn't return anything
        /// </summary>
        /// <param name="sql">The text of the query for Pegasus.</param>
        /// <param name="flag">Optional parameters flag.  Must be bound by name. for Pegasus</param>
        /// <param name="parameters">Optional parameters.  Must be bound by name. for Pegasus</param>       
        /// <returns>Returning Auto incremented id</returns>
        public static int SqlDo(string sql, bool flag, params OracleParameter[] parameters)
        {
            int i = 0;
            if (string.IsNullOrEmpty(sql))
            {
                throw new ArgumentException("sql command cannot be null nor empty");
            }

            try
            {
                using (OracleConnection cn = new OracleConnection(ConfigSettings.Connection))
                {
                    cn.Open();
                    using (OracleCommand command = new OracleCommand(sql, cn))
                    {
                        if (parameters != null)
                        {
                            foreach (OracleParameter parameter in parameters)
                            {
                                if (parameter != null)
                                {
                                    command.Parameters.Add(parameter);
                                }
                            }
                        }

                        command.BindByName = true;
                        int rcount = command.ExecuteNonQuery();
                        if (flag == true)
                        {
                            i = 1;
                        }
                        else
                        {
                            i = rcount;
                        }

                        command.Parameters.Clear();
                    }
                }
            }
            catch (Exception e)
            {
                throw new DataException(e.Message + " SQL: " + sql, e);
            }

            return i;
        }

        /// <summary>
        /// Returns instring surrounded by single tic marks
        /// </summary>
        /// <param name="instring">A string to add tic marks to.</param>
        /// <returns>A copy of instring with tic marks added</returns>
        public static string TicString(string instring)
        {
            if (string.IsNullOrEmpty(instring))
            {
                return "''";
            }

            string s = instring.Replace("'", "''");
            return "'" + s + "'";
        }

        /// <summary>
        /// Returns a dataset from the query.
        /// </summary>
        /// <param name="query">The text of the query.</param>
        /// <param name="parameters">The parameters for this SQL</param>
        /// <returns>The dataset created from the query.</returns>
        public static DataSet SelectOracleRows(StringBuilder query, params OracleParameter[] parameters)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            return SelectOracleRows(query.ToString(), parameters);
        }

        /// <summary>
        /// Returns a dataset from the query.
        /// </summary>
        /// <param name="query">The text of the query.</param>
        /// <param name="parameters">The parameters for this SQL</param>
        /// <returns>The dataset created from the query.</returns>
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Not a priority to rearchitect the internal UI to not use DataSets")]
        public static DataSet SelectOracleRows(string query, params OracleParameter[] parameters)
        {
            if (string.IsNullOrEmpty(query))
            {
                throw new ArgumentException("query string cannot be null nor empty");
            }

            try
            {
                using (OracleConnection conn = new OracleConnection(ConfigSettings.Connection))
                {
                    conn.Open();
                    using (OracleDataAdapter adapter = new OracleDataAdapter())
                    {
                        adapter.SelectCommand = new OracleCommand(query, conn);

                        if (parameters != null)
                        {
                            foreach (OracleParameter parameter in parameters)
                            {
                                if (parameter != null)
                                {
                                    adapter.SelectCommand.Parameters.Add(parameter);
                                }
                            }
                        }

                        DataSet dataset = new DataSet();
                        dataset.Locale = CultureInfo.InvariantCulture;
                        adapter.Fill(dataset);
                        adapter.SelectCommand.Parameters.Clear();
                        return dataset;
                    }
                }
            }
            catch (DataException e)
            {
                throw new DataException(e.Message + " SQL:" + query, e);
            }
            catch (OracleException e)
            {
                throw new DataException(e.Message + " " + query, e);
            }
            catch (Exception e)
            {
                throw new DataException(e.Message + " SQL:" + query, e);
            }
        }

        /// <summary>
        /// Returns a dataset from the query.
        /// </summary>
        /// <param name="procedure">The Stored Procedure to Execute.</param>
        /// <param name="parameters">Paramters for the stored procedure</param>
        /// <returns>The dataset created from the query.</returns>
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Not a priority to rearchitect the internal UI to not use DataSets")]
        public static DataSet SelectFromStoredProcedure(string procedure, OracleParameter[] parameters)
        {
            if (string.IsNullOrEmpty(procedure))
            {
                throw new ArgumentException("procedure string cannot be null nor empty");
            }

            try
            {
                using (OracleConnection connection = new OracleConnection(ConfigSettings.Connection))
                {
                    using (OracleCommand command = new OracleCommand(procedure, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddRange(parameters);

                        using (OracleDataAdapter adapter = new OracleDataAdapter(command))
                        {
                            DataSet dataset = new DataSet();
                            adapter.Fill(dataset);
                            command.Parameters.Clear();
                            return dataset;
                        }
                    }
                }
            }
            catch (DataException e)
            {
                string message = string.Format(
                    "DataException '{0}' from SelectFromStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
            catch (OracleException e)
            {
                string message = string.Format(
                    "OracleException '{0}' from SelectFromStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
            catch (Exception e)
            {
                string message = string.Format(
                    "'{0}' from SelectFromStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
        }

        /// <summary>
        /// Returns a dataset from the query.
        /// </summary>
        /// <param name="procedure">The Stored Procedure to Execute.</param>
        /// <param name="parameters">Paramters for the stored procedure</param>
        /// <param name="out_param">The parameters return out_param</param>
        /// <returns>The data readercreated from the query.</returns>
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Not a priority to rearchitect the internal UI to not use DataSets")]
        public static OracleDataReader SelectFromProcedureWithOutput(string procedure, OracleParameter[] parameters, ref long out_param)
        {
            OracleDataReader reader = null;
            if (string.IsNullOrEmpty(procedure))
            {
                throw new ArgumentException("procedure string cannot be null nor empty");
            }

            try
            {
                OracleConnection connection = new OracleConnection(ConfigSettings.Connection);
                OracleCommand command = new OracleCommand(procedure, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddRange(parameters);
                command.Connection.Open();
                reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                out_param = ((Oracle.ManagedDataAccess.Types.OracleDecimal)command.Parameters["out_param"].Value).ToInt64();
                command.Parameters.Clear();
                return reader;
            }
            catch (DataException e)
            {
                string message = string.Format(
                    "DataException '{0}' from SelectFromStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
            catch (OracleException e)
            {
                string message = string.Format(
                    "OracleException '{0}' from SelectFromStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
            catch (Exception e)
            {
                string message = string.Format(
                    "'{0}' from SelectFromStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
        }

        /// <summary>
        /// Returns a dataset from the query.
        /// </summary>
        /// <param name="procedure">The Stored Procedure to Execute.</param>
        /// <param name="parameters">Paramters for the stored procedure</param>        
        /// <returns>The data readercreated from the query.</returns>
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Not a priority to rearchitect the internal UI to not use DataSets")]
        public static OracleDataReader SelectFromProcedureWithOutputForErroredOrder(string procedure, OracleParameter[] parameters)
        {
            OracleDataReader reader = null;
            if (string.IsNullOrEmpty(procedure))
            {
                throw new ArgumentException("procedure string cannot be null nor empty");
            }

            try
            {
                OracleConnection connection = new OracleConnection(ConfigSettings.Connection);
                OracleCommand command = new OracleCommand(procedure, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddRange(parameters);
                command.Connection.Open();
                reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                ////  out_param = ((Oracle.DataAccess.Types.OracleDecimal)command.Parameters["out_param"].Value).ToInt64();
                command.Parameters.Clear();
                return reader;
            }
            catch (DataException e)
            {
                string message = string.Format(
                    "DataException '{0}' from SelectFromStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
            catch (OracleException e)
            {
                string message = string.Format(
                    "OracleException '{0}' from SelectFromStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
            catch (Exception e)
            {
                string message = string.Format(
                    "'{0}' from SelectFromStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
        }

        /// <summary>
        /// Returns a dataset from the query.
        /// </summary>
        /// <param name="procedure">The Stored Procedure to Execute.</param>
        /// <param name="parameters">Paramters for the stored procedure</param>        
        /// <returns>The data readercreated from the query.</returns>
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Not a priority to rearchitect the internal UI to not use DataSets")]
        public static DataSet SelectFromProcedureWithDataset(string procedure, OracleParameter[] parameters)
        {
            if (string.IsNullOrEmpty(procedure))
            {
                throw new ArgumentException("procedure string cannot be null nor empty");
            }

            try
            {
                DataSet dataset = new DataSet();
                OracleConnection connection = new OracleConnection(ConfigSettings.Connection);
                OracleCommand command = new OracleCommand(procedure, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddRange(parameters);
                command.Connection.Open();
                using (OracleDataAdapter adapter = new OracleDataAdapter(command))
                {
                    dataset.Locale = CultureInfo.InvariantCulture;
                    adapter.Fill(dataset);
                    adapter.SelectCommand.Parameters.Clear();
                }

                return dataset;
            }
            catch (DataException e)
            {
                string message = string.Format(
                    "DataException '{0}' from SelectFromStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
            catch (OracleException e)
            {
                string message = string.Format(
                    "OracleException '{0}' from SelectFromStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
            catch (Exception e)
            {
                string message = string.Format(
                    "'{0}' from SelectFromStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
        }

        /// <summary>
        /// Executes a stored procedure, such as save/insert
        /// </summary>
        /// <param name="procedure">The name of the stored procedure</param>
        /// <param name="parameters">The parameters</param>
        /// <returns>The number of rows affected</returns>
        public static int ExecuteStoredProcedure(string procedure, OracleParameter[] parameters)
        {
            if (string.IsNullOrEmpty(procedure))
            {
                throw new ArgumentException("query string cannot be null nor empty");
            }

            try
            {
                using (OracleConnection connection = new OracleConnection(ConfigSettings.Connection))
                {
                    connection.Open();
                    using (OracleCommand command = new OracleCommand(procedure, connection))
                    {
                        command.CommandText = procedure;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddRange(parameters);
                        int returnValue = command.ExecuteNonQuery();
                        command.Parameters.Clear();
                        return returnValue;
                    }
                }
            }
            catch (DataException e)
            {
                string message = string.Format(
                    "DataException '{0}' from ExecuteStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
            catch (OracleException e)
            {
                string message = string.Format(
                    "OracleException '{0}' from ExecuteStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
            catch (Exception e)
            {
                string message = string.Format(
                    "'{0}' from ExecuteStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
        }

        /// <summary>
        /// Executes a stored procedure, such as save/insert
        /// </summary>
        /// <param name="procedure">The name of the stored procedure</param>
        /// <param name="parameters">The parameters</param>
        /// <param name="out_licenseId">The parameters return license ID</param>
        /// <returns>The number of rows affected</returns>
        public static int ExecuteProcedureWithOutput(string procedure, OracleParameter[] parameters, ref long out_licenseId)
        {
            if (string.IsNullOrEmpty(procedure))
            {
                throw new ArgumentException("query string cannot be null nor empty");
            }

            try
            {
                using (OracleConnection connection = new OracleConnection(ConfigSettings.Connection))
                {
                    connection.Open();
                    using (OracleCommand command = new OracleCommand(procedure, connection))
                    {
                        command.CommandText = procedure;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddRange(parameters);
                        int returnValue = command.ExecuteNonQuery();
                        out_licenseId = ((Oracle.ManagedDataAccess.Types.OracleDecimal)command.Parameters["out_licenseId"].Value).ToInt64();
                        command.Parameters.Clear();
                        return returnValue;
                    }
                }
            }
            catch (DataException e)
            {
                string message = string.Format(
                    "DataException '{0}' from ExecuteStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
            catch (OracleException e)
            {
                string message = string.Format(
                    "OracleException '{0}' from ExecuteStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
            catch (Exception e)
            {
                string message = string.Format(
                    "'{0}' from ExecuteStoredProcedure '{1}'\r\n{2}",
                    e.Message,
                    procedure,
                    ToString(parameters));
                throw new DataException(message, e);
            }
        }

        /// <summary>
        /// A generic N parameter, function call
        /// </summary>
        /// <param name="functionName">Name of the function.</param>
        /// <param name="args">The args - must be an even number of name-value pairs.</param>
        /// <returns>The result of hte call to the oracle function as a string</returns>
        /// <exception cref="ArgumentException">Argument Exception</exception>
        /// <exception cref="InvalidOperationException">Invalid Operation Exception</exception>
        public static string CallOracleFunction(string functionName, params string[] args)
        {
            if (string.IsNullOrEmpty(functionName))
            {
                throw new ArgumentException("function name cannot be null nor empty");
            }

            // Must have an even number of args becuase we expect name-value pairs
            if (args.Length % 2 != 0)
            {
                throw new ArgumentException("Must have a name-value pair for each parameter");
            }

            StringBuilder sb = new StringBuilder();
            if (!functionName.ToLower().Contains("asl_schema.") &&
                !functionName.ToLower().Contains("license."))
            {
                // assume asl_schema unless explicitly set to license.
                sb.Append("ASL_SCHEMA.");
            }

            sb.Append(functionName);
            try
            {
                using (OracleConnection cn = new OracleConnection(ConfigSettings.Connection))
                {
                    cn.Open();
                    using (OracleCommand command = new OracleCommand(sb.ToString(), cn))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        // Add the return parameter
                        using (OracleParameter returnParameter = new OracleParameter("return_value", OracleDbType.Varchar2))
                        {
                            returnParameter.Direction = ParameterDirection.ReturnValue;
                            returnParameter.Size = 30;
                            command.Parameters.Add(returnParameter);

                            // Add parameters based on the args
                            int ii = 0;
                            while (ii < args.Length)
                            {
                                string name = args[ii++];
                                string value = args[ii++];
                                command.Parameters.Add(new OracleParameter(name, value));
                            }

                            command.ExecuteNonQuery();
                            command.Parameters.Clear();
                            return returnParameter.Value.ToString();
                        }
                    }
                }
            }
            catch (InvalidOperationException e)
            {
                string message = string.Format(
                    "OracleException {0} from CallOracleFunction '{1}'\r\n{2}",
                    e.Message,
                    functionName,
                    ToString(args));
                throw new DataException(message, e);
            }
            catch (NullReferenceException)
            {
                return string.Empty;
            }
            catch (Exception e)
            {
                string message = string.Format(
                    "'{0}' from CallOracleFunction '{1}'\r\n{2}",
                    e.Message,
                    functionName,
                    ToString(args));
                throw new DataException(message, e);
            }
        }

        /// <summary>
        /// Format:  'MM/DD/YYYY HH:MI:SS PM'
        /// </summary>
        /// <param name="inDate">The date to convert.</param>
        /// <returns>
        /// A string representation of the date time.
        /// </returns>
        public static string DateTimeToString(DateTime inDate)
        {
            if (string.IsNullOrEmpty(Convert.ToString(inDate, CultureInfo.InvariantCulture)))
            {
                throw new ArgumentException("indate cannot be null nor empty");
            }

            return "to_date('" + inDate + "','MM/DD/YYYY HH:MI:SS PM')";
        }

        /// <summary>
        /// Format:  'MM/DD/YYYY HH:MI:SS PM'
        /// </summary>
        /// <param name="inDate">The date to convert.</param>
        /// <returns>
        /// A string representation of the date time.
        /// </returns>
        public static string DateTimeToString(DateTime? inDate)
        {
            if (inDate == null ||
                string.IsNullOrEmpty(Convert.ToString(inDate, CultureInfo.InvariantCulture)))
            {
                throw new ArgumentException("inDate cannot be null nor empty");
            }

            return "to_date('" + inDate + "','MM/DD/YYYY HH:MI:SS PM')";
        }

        /// <summary>
        /// Converts Boolean to Y/N
        /// </summary>
        /// <param name="inValue">The boolean value to convert.</param>
        /// <returns>A string value of "Y" or "N".</returns>
        public static string BoolToYN(bool inValue) // TODO Refactor
        {
            return inValue ? "Y" : "N";
        }

        /// <summary>
        /// Converts Y/N to Boolean
        /// </summary>
        /// <param name="inValue">The in value.</param>
        /// <returns>True if inValue == "Y", otherwise false.</returns>
        public static bool YNToBool(string inValue) // TODO Refactor
        {
            return inValue == "Y";
        }

        /// <summary>
        /// Converts Y/N string to bool.
        /// </summary>
        /// <param name="inValue">The in value.  Should be Y, N, null, or empty.</param>
        /// <param name="defaultValue">The default value to use if the input string is null or empty.</param>
        /// <returns>The corresponding bool or the default value.</returns>
        public static bool YNToBool(string inValue, bool defaultValue)
        {
            if (string.IsNullOrEmpty(inValue))
            {
                return defaultValue;
            }

            return inValue.ToUpperInvariant() == "Y";
        }

        /// <summary>
        /// Adds the standard sort criteria expression to the given sql.
        /// </summary>
        /// <param name="sql">The SQL to which to add the sort criteria</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="entityToDbColumnMap">The entity to database column name map.</param>
        /// <returns>The new sql.</returns>
        public static string AddSortExpression(string sql, string sortExpression, Dictionary<string, string> entityToDbColumnMap)
        {
            if (string.IsNullOrEmpty(sql))
            {
                throw new ArgumentException("Sql cannot be null or empty.", "sql");
            }

            if (string.IsNullOrEmpty(sortExpression))
            {
                return sql;
            }

            bool isReverseSort = false;
            StringBuilder sqlSortExpression = new StringBuilder();

            if (sortExpression.ToLowerInvariant().EndsWith(" desc"))
            {
                isReverseSort = true;
                sortExpression = sortExpression.Substring(0, sortExpression.Length - 5);
            }

            string[] columnNames = sortExpression.Split(',');

            foreach (string columnName in columnNames)
            {
                string normalizedColumnName = columnName.Trim().ToLowerInvariant();
                sqlSortExpression.Append(entityToDbColumnMap[normalizedColumnName] + ", ");
            }

            // take off the last comma and space
            if (sqlSortExpression.Length > 0)
            {
                sqlSortExpression.Remove(sqlSortExpression.Length - 2, 2);
            }

            // add the "desc" if this is a reverse sort
            if (isReverseSort)
            {
                sqlSortExpression.Append(" desc");
            }

            return string.Format(
                CultureInfo.InvariantCulture,
                "{0} ORDER BY {1}",
                sql,
                sqlSortExpression);
        }

        /// <summary>
        /// Opens the connection, sends the command text and creates an oracle data reader.  (NOTE: The connection must be Disposed)
        /// </summary>
        /// <param name="connection">The connection .</param>
        /// <param name="sql">The SQL command.</param>
        /// <param name="parameters">The parameters for the command (optional).</param>
        /// <returns>The newly created OracleDataReader</returns>
        public static OracleDataReader CreateDataReader(OracleConnection connection, string sql, params OracleParameter[] parameters)
        {
            connection.Open();

            using (OracleCommand command = connection.CreateCommand())
            {
                command.CommandText = sql;
                command.CommandType = CommandType.Text;

                if (parameters != null)
                {
                    command.BindByName = true;
                    foreach (OracleParameter parameter in parameters)
                    {
                        if (parameter != null)
                        {
                            command.Parameters.Add(parameter);
                        }
                    }
                }

                OracleDataReader dataReader = command.ExecuteReader();
                command.Parameters.Clear();
                return dataReader;
            }
        }

     
        #endregion

        #region Internal Methods

        /// <summary>
        /// Wraps the SQL in a statement that will limit the row count.
        /// </summary>
        /// <param name="sql">The SQL command.</param>
        /// <param name="maxCount">The max count.</param>
        /// <returns>the SQL in a statement that will limit the row count</returns>
        internal static string LimitRowCount(string sql, int maxCount)
        {
            if (maxCount <= 0)
            {
                throw new ArgumentOutOfRangeException("maxCount", "Must be greater than zero");
            }

            return maxCount == int.MaxValue ? sql : string.Format(CultureInfo.InvariantCulture, "select * from ({0}) where rownum <= {1}", sql, maxCount);
        }

        /// <summary>
        /// Escape all special chars in the input string so they are not treated as 'special'
        /// </summary>
        /// <param name="value">The value to escape.</param>
        /// <param name="escapeChar">The character to use to escape special characters.</param>
        /// <returns>
        /// The value with special characters escaped.
        /// </returns>
        internal static string EscapeSpecialCharacters(string value, char escapeChar)
        {
            // Need to escape all special chars in the input string so they are not treated as 'special'
            value = value.Replace("!", escapeChar + "!");
            value = value.Replace("_", escapeChar + "_");
            value = value.Replace("%", escapeChar + "%");
            return value;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Changes the name-value pairs to a single string.
        /// </summary>
        /// <param name="args">The array of name-value pairs.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        private static string ToString(string[] args)
        {
            StringBuilder info = new StringBuilder();
            for (int ii = 0; ii < args.Length; ii += 2)
            {
                info.AppendFormat("{0} = {1}\r\n", args[ii], args[ii + 1]);
            }

            return info.ToString();
        }

        /// <summary>
        /// Changes the args to a string the string.
        /// </summary>
        /// <param name="oracleParameters">The oracle parameters.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        private static string ToString(OracleParameter[] oracleParameters)
        {
            StringBuilder info = new StringBuilder();
            foreach (OracleParameter parameter in oracleParameters)
            {
                info.AppendFormat("{0} = {1}\r\n", parameter.ParameterName, parameter.Value);
            }

            return info.ToString();
        }

        #endregion

        #endregion
    }
}
