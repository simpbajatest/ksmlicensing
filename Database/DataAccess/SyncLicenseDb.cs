﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using Lambda.Entities;
using System.Globalization;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Amazon.DynamoDBv2.DataModel;
using Amazon.Runtime;
using Amazon.DynamoDBv2.DocumentModel;
using System.Threading.Tasks;
using System.Data;

namespace DatabaseLayer
{
   public static class SyncLicenseDb
    {
        /// <summary>
        /// Gets the list of sync licenses
        /// </summary>
        private static SyncInsFulCollection SyncLicenseCol
        {
            get;set;
        }

        /// <summary>
        /// Finds the licenses for a host.
        /// </summary>
        /// <param name="hostIdentifier">The host identifier.</param>
        /// <returns>
        /// A populated <see cref="LicenseCollection"/>.
        /// </returns>
        public static SyncInsFulCollection FindInsFulForSync(string hostIdentifier)
        {
            if (hostIdentifier == null)
            {
                throw new ArgumentNullException("hostIdentifier");
            }

            if (string.IsNullOrEmpty(hostIdentifier))
            {
                throw new ArgumentOutOfRangeException("hostIdentifier", "hostIdentifier cannot be null or empty");
            }

            string sql =
                " select distinct ll.qty,ll.exp_date, s.END_DATE, ol.Activation_Code from license.license_log ll " +
                "    left outer join license.product p on p.product_id = ll.product_id " +
                "    join license.orders o ON ll.order_id = o.order_id " +
                " join order_line ol on ll.line_id=ol.line_id and ol.order_id=ll.order_id " +
                " LEFT JOIN license.license_log_subscription lls on ll.license_id=lls.license_id " +
                " LEFT JOIn asl_schema.subscription s on s.subscription_id=lls.subscription_id and s.version = lls.subscription_version " +
                " join license.entities e ON e.e_name = o.vendor_name " +
                " where upper(nvl(ll.node_id, '')) = :NODE_ID_INPUT " +
                " AND NOT EXISTS (SELECT 1 FROM ASL_SCHEMA.RUNTIME_VALUES " +
                " WHERE ITEM_NAME IN('SyncLicenseTypeExclusionList') " +
                " AND vvalue LIKE '%' || upper(ll.license_type) || ',%' ) " +
                " and e.use_activation_code = 'Y' " +
                " and ol.activation_code is not null AND ll.Qty>0 " +
                " AND upper(NVL(o.order_type, 'X')) not in ('TRIAL','RENTAL') " +
                " AND NOT(upper(NVL(order_source, 'X')) = 'MANUAL' AND NVL(order_type, 'X') = ('INTERNAL')) " +
                " AND p.allow_rehosting_flag = 'Y' ";
               

            OracleParameter[] parameters =
                {
                    new OracleParameter("NODE_ID_INPUT", hostIdentifier.ToUpperInvariant())
                };
           
            SyncInsFulCollection licCol = PopulateInsFuls(sql, parameters);

            return licCol;
        }

        /// <summary>
        /// This function to find the license Host and Product information
        /// </summary>
        /// <param name="orderId">Sending Order ID</param>
        /// <param name="lineId">lineId of line</param>
        /// <returns>Returning the License Collection</returns>
        public static LicenseHostCollection FindLicenseHost(string orderId, long lineId)
        {
            string sql = "select distinct ll.node_id, ll.qty, ll.order_id, ll.line_id " +
                         " FROM license.license_log ll " +  
                         " WHERE ll.order_id = :ORDER_ID_INPUT " +
                         "    AND ll.line_id = :LINE_ID_INPUT " +
                         " AND NOT EXISTS(SELECT 1 FROM ASL_SCHEMA.RUNTIME_VALUES " +
                         "   WHERE ITEM_NAME IN('ActivateLicenseTypeExclusionList') " +
                        " AND vvalue LIKE '%' || upper(ll.license_type) || ',%') " +                       
                         "    AND ll.Qty>0 ";

            OracleParameter[] parameters =
            {
                        new OracleParameter("ORDER_ID_INPUT", orderId),
                        new OracleParameter("LINE_ID_INPUT", lineId)
                    };

            return PopulateLicenseHosts(sql, parameters);
        }

        /// <summary>
        /// Finds the licenses for a host.
        /// </summary>
        /// <param name="hostIdentifier">The host identifier.</param>
        /// <returns>
        /// A populated <see cref="LicenseCollection"/>.
        /// </returns>
        public static async Task<SyncInsFulCollection> FindLicensesSyncedAlready(string hostIdentifier)
        {
            if (hostIdentifier == null)
            {
                throw new ArgumentNullException("hostIdentifier");
            }

            if (string.IsNullOrEmpty(hostIdentifier))
            {
                throw new ArgumentOutOfRangeException("hostIdentifier", "hostIdentifier cannot be null or empty");
            } 

            // Create the key conditions from hashKey and condition
            Dictionary<string, Condition> keyConditions = new Dictionary<string, Condition>(StringComparer.InvariantCultureIgnoreCase)
            {
                // Hash key condition. ComparisonOperator must be "EQ".
                {
                    "HOST_ID",
                    new Condition
                    {
                        ComparisonOperator = "EQ",
                        AttributeValueList = new List<AttributeValue>
                        {
                            new AttributeValue { S = hostIdentifier }
                        }
                    }
                },
                // Range key condition
                {
                    "UK_NODEID_LICID_SUSID",
                    new Condition
                    {
                        ComparisonOperator = "BEGINS_WITH",
                        AttributeValueList = new List<AttributeValue>
                        {
                            new AttributeValue { S = hostIdentifier }
                        }
                    }
                }
                };
            
            // Define marker variable
            Dictionary<string, AttributeValue> startKey = null;
            SyncInsFulCollection licCol = new SyncInsFulCollection();
            //var credentials = new BasicAWSCredentials("AKIAVUV3SF2DPMUJQYLG", "TbNq3TTPgBN8WbXYBcj1urWr0ezOf/9o1JLzGsyW");
            // var client = new AmazonDynamoDBClient(credentials, RegionEndpoint.USWest2);
            using (var client = new AmazonDynamoDBClient())
            {

                do
            {
                // Create Query request
                QueryRequest request = new QueryRequest
                {
                    TableName = "KSM_ISG_SYNC_VER_2",
                    ExclusiveStartKey = startKey,
                    KeyConditions = keyConditions,
                    ExpressionAttributeValues = new Dictionary<string, AttributeValue>
                      {
                        { ":row_status", new AttributeValue { S = "Y" } }
                      },
                    ExpressionAttributeNames = new Dictionary<string, string>
                      {
                        { "#rs", "ROW_STATUS" }
                      },
                    FilterExpression = "#rs = :row_status",
                    ProjectionExpression = "LIC_EXP_DATE, SUS_END_DATE, ACTIVATION_CODE,QTY,ROW_STATUS",
                    ConsistentRead = true
                };
               

                    // Issue request
                    var result = await client.QueryAsync(request);

                    // View all returned items
                    List<Dictionary<string, AttributeValue>> items = result.Items;
                    foreach (Dictionary<string, AttributeValue> item in items)
                    {
                        uint qty = 0;
                        DateTime? expDate = null;
                        DateTime? endDate = null;
                        string activationCode = string.Empty;

                        foreach (var keyValuePair in item)
                        {
                            if (keyValuePair.Key == "LIC_EXP_DATE")
                            {
                                if (!string.IsNullOrEmpty(keyValuePair.Value.S.Trim()))
                                {
                                    expDate = Convert.ToDateTime(keyValuePair.Value.S.Trim());
                                }
                            }
                            else if (keyValuePair.Key == "SUS_END_DATE")
                            {
                                if (!string.IsNullOrEmpty(keyValuePair.Value.S.Trim()))
                                {
                                    endDate = Convert.ToDateTime(keyValuePair.Value.S.Trim());
                                }
                            }
                            else if (keyValuePair.Key == "QTY")
                            {
                                if (!string.IsNullOrEmpty(keyValuePair.Value.S))
                                {
                                    qty = Convert.ToUInt32(keyValuePair.Value.S);
                                }
                            }
                            else if (keyValuePair.Key == "ACTIVATION_CODE")
                            {
                                activationCode = keyValuePair.Value.S.Trim();
                            }

                        }
                        SyncInstalledFulfillment lic = new SyncInstalledFulfillment(qty, expDate, endDate, activationCode);
                        licCol.Add(lic);
                    }

                    // Set marker variable
                    startKey = result.LastEvaluatedKey;
                } while (startKey != null && startKey.Count > 0) ;
            }

            return licCol;
        }

        /// <summary>
        /// delete installed fulfillment for a host.
        /// </summary>
        /// <param name="hostIdentifier">The host identifier.</param>       
        public static void DeleteInsFulForSync(string hostIdentifier)
        {           
            const string DeleteInsFulSql =
            "DELETE FROM license.AA_IXIA_API " +
            "WHERE request='SYNC' and upper(node_id) = upper(:HOST_ID_INPUT)";

            OracleParameter[] parameters =
                {
                    new OracleParameter("HOST_ID_INPUT", hostIdentifier)
                };

            DatabaseAccessUtility.SqlDo(DeleteInsFulSql, parameters);
        }

        public static string sqlgetlicensefile()
        {
            const string EesofSql =
                        "select license_file from license_log_file " +
"where license_log_file_id = :license_id_input";

            OracleParameter[] eesofParameters =
                {
                            new OracleParameter("license_id_input", "2585743")
                        };

            string result = DatabaseAccessUtility.SqlGet(EesofSql, eesofParameters);
            return result;
        }

        /// <summary>
        /// sync licenses.
        /// </summary>
        /// <param name="hostIdentifier">The host identifier.</param>    
        /// <param name="metaData">The metadata.</param> 
        public static string SyncLicenses(string hostIdentifier, string metaData)
        {
          return DatabaseAccessUtility.CallOracleFunction("License.LICENSE_GENERATION.Sync_Licenses", "v_Host_Id", hostIdentifier.ToString(CultureInfo.InvariantCulture), "v_MetaData", metaData);           
        }

        /// <summary>
        /// Activate licenses.
        /// </summary>
        /// <param name="hostIdentifier">The host identifier.</param>    
        /// <param name="metaData">The metadata.</param> 
        public static string ActivateLicenses(string hostIdentifier, string metaData)
        {
            return DatabaseAccessUtility.CallOracleFunction("License.LICENSE_GENERATION.Activate_Deactivate_Licenses", "v_Host_Id", hostIdentifier.ToString(CultureInfo.InvariantCulture), "v_MetaData", metaData);
        }

        /// <summary>
        /// Activate licenses.
        /// </summary>
        /// <param name="hostIdentifier">The host identifier.</param>    
        /// <param name="metaData">The metadata.</param> 
        public static DataSet ActivateLicensesNew(string hostIdentifier, string metaData)
        {
            string package = "LICENSE_GENERATION_API.ACTIVATE_DEACTIVATE_LICENSES";
            OracleParameter[] parameters =
            {
                 new OracleParameter("v_Host_Id", hostIdentifier.ToString(CultureInfo.InvariantCulture)),
                 new OracleParameter("v_MetaData", metaData),               
                 new OracleParameter("CUR_LICENSE", OracleDbType.RefCursor, 0,  ParameterDirection.Output, true, 0, 0, string.Empty, DataRowVersion.Default, Convert.DBNull),
            };

            DataSet dataset = DatabaseAccessUtility.SelectFromStoredProcedure(package, parameters);
            return dataset;
        }

        /// <summary>
        /// Insert installed fulfillment row.
        /// </summary>
        /// <param name="activationCode">Ins ful sync activation code.</param>
        /// <param name="hostId">Ins ful sync host id</param>        
        public static void InsertInsFulSync(
                                           string activationCode,                                          
                                           string hostId,
                                            long qty,
                                           string request)
        {
            const string Sql =
                "insert into license.AA_IXIA_API " +
                "           (activation_code,  node_id,qty, request) values (:AC_INPUT, :NODE_ID_INPUT, :QTY_INPUT, :REQUEST_INPUT) ";
                   

                OracleParameter[] parameters =
                    {
                        new OracleParameter("AC_INPUT", activationCode),
                        new OracleParameter("NODE_ID_INPUT", hostId),
                        new OracleParameter("QTY_INPUT", qty),
                        new OracleParameter("REQUEST_INPUT", request)
                    };

                DatabaseAccessUtility.SqlDo(Sql, parameters);
            }

        /// <summary>
        /// Insert installed fulfillment row.
        /// </summary>
        /// <param name="actCodes">Ins ful sync activation code.</param>           
        public static void InsertInstalledFulfillmentForFulfillment(int[] qtys,string[] actCodeList, string[] hostIds, string[] requests, int count)
        { 
            const string Sql =
                "insert into license.AA_IXIA_API " +
                "           (activation_code,  node_id,qty, request) values (:AC_INPUT, :NODE_ID_INPUT, :QTY_INPUT, :REQUEST_INPUT) ";


            OracleParameter[] parameters =
                {
                        new OracleParameter("AC_INPUT", actCodeList),
                        new OracleParameter("NODE_ID_INPUT", hostIds),
                        new OracleParameter("QTY_INPUT", qtys),
                        new OracleParameter("REQUEST_INPUT", requests)
                    };

            DatabaseAccessUtility.SqlDoArray(Sql, count, parameters);
        }

        /// <summary>
        /// Insert installed fulfillment row.
        /// </summary>
        /// <param name="actCodes">Ins ful sync activation code.</param>           
        public static void InsertInstalledFulfillmentForSync(List<string> actCodes, string hostId)
        {

            int[] qtys = new int[actCodes.Count];
            string[] hostIds = new string[actCodes.Count];
            string[] actCodeList = new string[actCodes.Count];
            string[] requests = new string[actCodes.Count];

            for (int j = 0; j < actCodes.Count; j++)
            {
                qtys[j] = 0;
                hostIds[j] = hostId;
                actCodeList[j] = Convert.ToString(actCodes[j]);
                requests[j] = "SYNC";
            }

            const string Sql =
                "insert into license.AA_IXIA_API " +
                "           (activation_code,  node_id,qty, request) values (:AC_INPUT, :NODE_ID_INPUT, :QTY_INPUT, :REQUEST_INPUT) ";


            OracleParameter[] parameters =
                {
                        new OracleParameter("AC_INPUT", actCodeList),
                        new OracleParameter("NODE_ID_INPUT", hostIds),
                        new OracleParameter("QTY_INPUT", qtys),
                        new OracleParameter("REQUEST_INPUT", requests)
                    };

            DatabaseAccessUtility.SqlDoArray(Sql, actCodes.Count, parameters);
        }

        /// <summary>
        /// Populates the collection of licenses using the sql and parameters (if any) specified.
        /// IMPORTANT!! If you pass subscription data to this command to populate the tied subscriptions,
        /// you Must use the <see cref="SubscriptionDb.SubscriptionTableColumns"/> which aliases the subscription columns, otherwise,
        /// you won't get your subscription data populated.
        /// </summary>
        /// <param name="sql">The SQL command.</param>
        /// <param name="parameters">Any parameters to use in the query.</param>
        /// <returns>The licenses found by the sql.</returns>
        private static SyncInsFulCollection PopulateInsFuls(
                                                          string sql,
                                                          params OracleParameter[] parameters)
        {
            SyncInsFulCollection insFuls = new SyncInsFulCollection();

            using (OracleConnection connection = new OracleConnection(ConfigSettings.Connection))
            {
                using (OracleDataReader dataReader = DatabaseAccessUtility.CreateDataReader(connection, sql, parameters))
                {
                    while (dataReader.Read())
                    {
                        SyncInstalledFulfillment insFul;
                        try
                        {
                            insFul = PopulateInsFul(dataReader);

                            if (insFul != null)
                            { 
                            insFuls.Add(insFul);
                            }
                        }
                        catch (BusinessEntitiesException e)
                        {
                            // Host Type validation errors should not be logged.  But the host type validation
                            //   error has to remain in effect becuase other processes are dependent on it.
                            if (!e.ToString().ToUpperInvariant().Contains("CANNOT CONVERT InstalledFulfillment entry of license_log"))
                            {
                                //AslEventLog.LogWarning(e.ToString());
                            }

                            // Something was wrong with this license - skip it
                            continue;
                        }
                    }
                }
            }

            return insFuls;
        }

        /// <summary>
        /// Creates the license object from the data reader.
        /// </summary>
        /// <param name="dataReader">The data reader.</param>
        /// <returns>The license.</returns>
        internal static SyncInstalledFulfillment PopulateInsFul(OracleDataReader dataReader)
        {           
            uint quantity = Convert.ToUInt32(dataReader["QTY"].ToString(), CultureInfo.InvariantCulture);          
            string endDateText = dataReader["END_DATE"].ToString();
            string expirationDateText = dataReader["EXP_DATE"].ToString();

            // now process the date fields
            DateTime? expirationDate = null;
            DateTime? endDate = null;            

            if (!string.IsNullOrEmpty(expirationDateText))
            {
                try
                {
                    expirationDate = Convert.ToDateTime(expirationDateText, CultureInfo.InvariantCulture);
                }
                catch (FormatException)
                {
                }
            }

            if (!string.IsNullOrEmpty(endDateText))
            {
                try
                {
                    endDate = Convert.ToDateTime(endDateText, CultureInfo.InvariantCulture);
                }
                catch (FormatException)
                {
                }
            }

              string  activationCode = dataReader["ACTIVATION_CODE"].ToString();          

            SyncInstalledFulfillment insFul = new SyncInstalledFulfillment(
                                           quantity,
                                           expirationDate,
                                           endDate,                                           
                                           activationCode);

            return insFul;
        }

        /// <summary>
        /// Populates the collection of licenses using the sql and parameters (if any) specified.
        /// IMPORTANT!! If you pass subscription data to this command to populate the tied subscriptions,
        /// you Must use the <see cref="SubscriptionDb.SubscriptionTableColumns"/> which aliases the subscription columns, otherwise,
        /// you won't get your subscription data populated.
        /// </summary>
        /// <param name="sql">The SQL command.</param>
        /// <param name="parameters">Any parameters to use in the query.</param>
        /// <returns>The licenses found by the sql.</returns>
        private static LicenseHostCollection PopulateLicenseHosts(
                                                          string sql,
                                                          params OracleParameter[] parameters)
        {
            LicenseHostCollection insFuls = new LicenseHostCollection();

            using (OracleConnection connection = new OracleConnection(ConfigSettings.Connection))
            {
                using (OracleDataReader dataReader = DatabaseAccessUtility.CreateDataReader(connection, sql, parameters))
                {
                    while (dataReader.Read())
                    {
                        LicenseHost insFul;
                        try
                        {
                            insFul = PopulateLicenseHost(dataReader);

                            if (insFul != null)
                            {
                                insFuls.Add(insFul);
                            }
                        }
                        catch (BusinessEntitiesException e)
                        {
                            // Host Type validation errors should not be logged.  But the host type validation
                            //   error has to remain in effect becuase other processes are dependent on it.
                            if (!e.ToString().ToUpperInvariant().Contains("CANNOT CONVERT LicenseHostCollection entry of license_log"))
                            {
                                //AslEventLog.LogWarning(e.ToString());
                            }

                            // Something was wrong with this license - skip it
                            continue;
                        }
                    }
                }
            }

            return insFuls;
        }

        /// <summary>
        /// Creates the license object from the data reader.
        /// </summary>
        /// <param name="dataReader">The data reader.</param>
        /// <returns>The license.</returns>
        internal static LicenseHost PopulateLicenseHost(OracleDataReader dataReader)
        {
            uint quantity = Convert.ToUInt32(dataReader["QTY"].ToString(), CultureInfo.InvariantCulture);
            string hostId = dataReader["NODE_ID"].ToString();
            string orderId = dataReader["ORDER_ID"].ToString();
            string lineId = dataReader["LINE_ID"].ToString();

            LicenseHost insFul = new LicenseHost(
                                           quantity,
                                           hostId,
                                           orderId,
                                           lineId);
            return insFul;
        }
    }
}
