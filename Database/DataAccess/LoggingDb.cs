﻿//-----------------------------------------------------------------------
// <copyright file="LoggingDb.cs" company="Keysight Technologies, Inc.">
//      Copyright (C) Keysight Technologies, Inc.
// </copyright>
//-------------------------------------------------------------------
namespace DatabaseLayer
{
    using System;
    using System.Text;
    using Lambda.Entities;
   
    using Oracle.ManagedDataAccess.Client;

    /// <summary>
    /// The Log Types
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// Debug Type
        /// </summary>
        Debug,

        /// <summary>
        /// Information Type
        /// </summary>
        Information,

        /// <summary>
        /// Warning Type
        /// </summary>
        Warning,

        /// <summary>
        /// Error Type
        /// </summary>
        Error
    }

    /// <summary>
    /// Contains the logic for db operations that Log to the Database (This class calls stored procedures in LICENSE.LOGGING)
    /// </summary>
    public static class LoggingDb
    {
        /// <summary>
        /// Logs transactions to the database.  If the database is unavailable then
        /// it will fail over to the EventLog
        /// </summary>
        /// <param name="orderId">Order Id to be logged.</param>
        /// <param name="logLevel">The <see cref="LogLevel"/>.</param>
        /// <param name="eventName">The type of event to log; Debug, Info, Warning, Error</param>
        /// <param name="componentName">The Component Name; Transport, GetCertificate, ProcessEdlivery, etc.</param>
        /// <param name="notes">Specific information on the transaction or stack trace.  Field length is 1024</param>
        /// <param name="requester">If you have the username pass it in</param>
        public static void LogToTransaction(string orderId, LogLevel logLevel, string eventName, string componentName, string notes, string requester)
        {
            if (eventName == null)
            {
                throw new ArgumentNullException("eventName");
            }

            if (notes == null)
            {
                throw new ArgumentNullException("notes");
            }

            try
            {
                // create 'message' from component & notes
                StringBuilder message = new StringBuilder();
                if (!string.IsNullOrEmpty(componentName))
                {
                    message.Append('[');
                    message.Append(componentName.ToUpperInvariant());
                    message.Append("] : ");
                }

                message.Append(notes);

                if (string.IsNullOrEmpty(orderId))
                {
                    logLevel = LogLevel.Error;
                    throw new ArgumentOutOfRangeException("orderId", "OrderId is invalid (Cannot be null, empty, or 0.)");
                }

                const string Sql = "insert into license.transaction t " +
                                   "    (t.order_id, t.type, t.event, t.notes, t.emp_name) " +
                                   "values " +
                                   "    (:ORDER_ID_INPUT, " +
                                   "     substr(:TYPE_NAME_INPUT, 1, 20), " +
                                   "     substr(:EVENT_NAME_INPUT, 1, 30), " +
                                   "     substr(:MESSAGE_INPUT, 1, 1024), " +
                                   "     substr(:REQUESTOR_INPUT, 1, 40)) ";

                OracleParameter[] parameters =
                    {
                        new OracleParameter("ORDER_ID_INPUT", orderId.ToString()),
                        new OracleParameter("TYPE_NAME_INPUT", logLevel.ToString()),
                        new OracleParameter("EVENT_NAME_INPUT", eventName),
                        new OracleParameter("MESSAGE_INPUT", message.ToString()),
                        new OracleParameter("REQUESTOR_INPUT", requester)
                    };

                DatabaseAccessUtility.SqlDo(Sql, parameters);
            }
            catch
            {
                switch (logLevel)
                {
                    case LogLevel.Debug:
                    case LogLevel.Information:
                       // AslEventLog.LogInfo(eventName, notes);
                        break;
                    case LogLevel.Warning:
                        //AslEventLog.LogWarning(eventName, notes);
                        break;
                    case LogLevel.Error:
                        Exception exception = new Exception(orderId + " " + logLevel.ToString() + " " + eventName + " " + notes + " " + requester);
                       // AslEventLog.LogError(exception);
                        break;
                }
            }
        }

        /// <summary>
        /// Logs transactions to the database.  If the database is unavailable then
        /// it will fail over to the EventLog
        /// </summary>
        /// <param name="orderId">Order Id to be logged.</param>
        /// <param name="logLevel">The <see cref="LogLevel"/>.</param>
        /// <param name="eventName">The type of event to log; Debug, Info, Warning, Error</param>
        /// <param name="componentName">The Component Name; Transport, GetCertificate, ProcessEdlivery, etc.</param>
        /// <param name="notes">Specific information on the transaction or stack trace.  Field length is 1024</param>
        /// <param name="requester">If you have the username pass it in</param>
        public static void LogToTransactionManualOrder(string orderId, LogLevel logLevel, string eventName, string componentName, string notes, string requester)
        {
            if (eventName == null)
            {
                throw new ArgumentNullException("eventName");
            }

            if (notes == null)
            {
                throw new ArgumentNullException("notes");
            }

            try
            {
                const string Sql = "UPDATE license.transaction  " +
                                    "SET" +
                                    "  type = :TYPE_NAME_INPUT," +
                                    "  notes = :MESSAGE_INPUT, " +
                                    "  emp_name = :REQUESTOR_INPUT " +
                                    "  WHERE order_id = :ORDER_ID_INPUT ";

                OracleParameter[] parameters =
                    {
                        new OracleParameter("ORDER_ID_INPUT", orderId.ToString()),
                        new OracleParameter("TYPE_NAME_INPUT", logLevel.ToString()),
                        new OracleParameter("MESSAGE_INPUT", notes.ToString()),
                        new OracleParameter("REQUESTOR_INPUT", requester)
                    };

                DatabaseAccessUtility.SqlDo(Sql, parameters);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Logs processes to the database.  If the database is unavailable then
        /// it will fail over to the EventLog.
        /// </summary>
        /// <param name="logLevel">The <see cref="LogLevel"/>.</param>
        /// <param name="eventName">The type of log; Debug, Info, Warning, Error</param>
        /// <param name="componentName">The Component Name; Transport, GetCertificate, ProcessEdlivery, etc.</param>
        /// <param name="notes">Specific information on the transaction or stack trace.  Field length is 1024</param>
        /// <param name="requester">If you have the user name pass it in</param>
        public static void LogProcess(LogLevel logLevel, string eventName, string componentName, string notes, string requester)
        {
            try
            {
                eventName = eventName.Length > 30 ? eventName.Substring(0, 30) : eventName;
                notes = notes.Length > 970 ? notes.Substring(0, 970) : notes;
                requester = string.IsNullOrEmpty(requester) ? "Unknown" : requester;
                requester = requester.Length > 40 ? requester.Substring(0, 40) : requester;

                StringBuilder message = new StringBuilder();
                message.Append(logLevel.ToString());
                message.Append(" [");
                message.Append(componentName);
                message.Append("] ");
                message.Append(notes);

                const string Sql = "insert into license.process_log pl " +
                                   "    (PROGRAM_NAME, NOTES) " +
                                   "values " +
                                   "    (substr(:PROGRAM_NAME_INPUT, 1, 256), substr(:NOTES_INPUT, 1, 1024) ) ";

                OracleParameter[] parameters =
                    {
                        new OracleParameter("PROGRAM_NAME_INPUT", eventName),
                        new OracleParameter("NOTES_INPUT", message.ToString())
                    };

                DatabaseAccessUtility.SqlDo(Sql, parameters);

                if (logLevel == LogLevel.Error)
                {
                    Exception exception =
                        new Exception(logLevel.ToString() + " " + eventName + " " + notes + " " + requester);
                    //AslEventLog.LogError(exception);
                }
            }
            catch
            {
                switch (logLevel)
                {
                    case LogLevel.Debug:
                    case LogLevel.Information:
                      //  AslEventLog.LogInfo(eventName, notes);
                        break;
                    case LogLevel.Warning:
                       // AslEventLog.LogWarning(eventName, notes);
                        break;
                    case LogLevel.Error:
                        Exception exception =
                            new Exception(logLevel.ToString() + " " + eventName + " " + notes + " " + requester);
                        //AslEventLog.LogError(exception);
                        break;
                }
            }
        }

        /// <summary>
        /// Logs information to Table Execution Trace
        /// </summary>
        /// <param name="processName">The name of the process</param>
        /// <param name="notes">Any notes needed to be captured</param>
        public static void LogTrace(string processName, string notes)
        {
            const string Sql = "LICENSE.LOGGING.LOG_TRACE";

            OracleParameter[] parameters =
            {
                new OracleParameter("in_component_name", processName),
                new OracleParameter("in_message_string", notes)
            };

            DatabaseAccessUtility.ExecuteStoredProcedure(Sql, parameters);
        }

        /// <summary>
        /// Logs to login as customer log.
        /// </summary>
        /// <param name="requestId">The request id.</param>
        /// <param name="empNum">The emp num.</param>
        /// <param name="profileId">The profile id.</param>
        /// <param name="action">The action.</param>
        /// <param name="notes">The notes.</param>
        public static void LogToLoginAsCustomerLog(string requestId, string empNum, string profileId, string action, string notes)
        {
            const string Sql =
                "insert into asl_schema.login_as_customer_log " +
                "(request_id, emp_num, profile_id, event_type, notes, inserted_at) " +
                " values (:REQUEST_ID_INPUT, :EMP_NUM_INPUT, :PROFILE_ID_INPUT, " +
                " :EVENT_TYPE_INPUT, :NOTES_INPUT, SYSDATE )";

            OracleParameter[] parameters =
                {
                    new OracleParameter("REQUEST_ID_INPUT", requestId),
                    new OracleParameter("EMP_NUM_INPUT", empNum),
                    new OracleParameter("PROFILE_ID_INPUT", profileId),
                    new OracleParameter("EVENT_TYPE_INPUT", action),
                    new OracleParameter("NOTES_INPUT", notes)
                };
            DatabaseAccessUtility.SqlDo(Sql, parameters);
        }

        /// <summary>
        /// Logs to agreement log for agreements from siebel.
        /// </summary>
        /// <param name="assetId">The asset Id.</param>
        /// <param name="assetNumber">The asset Number.</param>
        /// <param name="agreementNumber">The agreement Number.</param>
        /// <param name="lineId">The line Id.</param>
        /// <param name="subscriptionId">The subscription Id.</param>
        /// <param name="notes">The notes.</param>
        /// <param name="eventType">The event Type.</param>
        public static void LogToKsmAgreementLog(string assetId, string assetNumber, string agreementNumber, string lineId, long subscriptionId, string notes, string eventType)
        {
            object subsId;

            if (subscriptionId == 0)
            {
                subsId = System.DBNull.Value;
            }
            else
            {
                subsId = subscriptionId;
            }

            // Generate a new ksm_agreement_log_id
            long ksmAgreementLogId = GetNewKsmAgreementLogId();

            const string Sql =
                "insert into asl_schema.ksm_agreement_log " +
                "(ksm_agreement_log_id, change_date, asset_id, asset_number, agreement_number, line_id, subscription_id, notes, event) " +
                " values (:KSM_AGREEMENT_LOG_ID_INPUT, SYSDATE, :ASSET_ID_INPUT, :ASSET_NUMBER_INPUT, :AGREEMENT_NUMBER_INPUT, :LINE_ID_INPUT, " +
                " :SUBSCRIPTION_ID_INPUT, :NOTES_INPUT, :EVENT_INPUT )";

            OracleParameter[] parameters =
                {
                    new OracleParameter("KSM_AGREEMENT_LOG_ID_INPUT", ksmAgreementLogId.ToString()),
                    new OracleParameter("ASSET_ID_INPUT", assetId),
                    new OracleParameter("ASSET_NUMBER_INPUT", assetNumber),
                    new OracleParameter("AGREEMENT_NUMBER_INPUT", agreementNumber),
                    new OracleParameter("LINE_ID_INPUT", lineId),
                    new OracleParameter("SUBSCRIPTION_ID_INPUT", subsId),
                    new OracleParameter("NOTES_INPUT", notes),
                    new OracleParameter("EVENT_INPUT", eventType)
                };
            DatabaseAccessUtility.SqlDo(Sql, parameters);
        }

        /// <summary>
        /// Acquire a new ksm_agreement_log_id, next in sequence
        /// </summary>
        /// <returns>The next RPL id.</returns>
        private static long GetNewKsmAgreementLogId()
        {
            long retVal = 0;
            retVal = DatabaseAccessUtility.SqlGetSeq("asl_schema.ksm_agreement_log_id_seq");
            return retVal;
        }
    }
}
