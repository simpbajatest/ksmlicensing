﻿//-----------------------------------------------------------------------
// <copyright file="DataIntegrityException.cs" company="Keysight Technologies, Inc.">
//      Copyright (C) Keysight Technologies, Inc.
// </copyright>
//-----------------------------------------------------------------------
namespace DatabaseLayer
{
    using System;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    /// <summary>
    /// Exception thrown when there is data or missing data that is not expected
    /// in the database.
    /// </summary>
    [Serializable]
    public class DataIntegrityException : Exception
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the DataIntegrityException class.
        /// </summary>
        /// <param name="message">The message.</param>
        public DataIntegrityException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the DataIntegrityException class.
        /// </summary>
        /// <param name="innerException">The inner exception.</param>
        public DataIntegrityException(Exception innerException) :
            base(null, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the DataIntegrityException class.
        /// </summary>
        /// <param name="serializationInfo">Serialization info.</param>
        /// <param name="streamingContext">Streaming context.</param>
        protected DataIntegrityException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// To support serialization.  Populates a SerializationInfo with the data needed to 
        /// serialize the target object. 
        /// </summary>
        /// <param name="info">Serialization info object.</param>
        /// <param name="context">Streaming context.</param>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        #endregion
    }
}
