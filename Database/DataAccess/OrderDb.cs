﻿using Lambda.Entities;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace DatabaseLayer
{
    public static class OrderDb
    {
        #region Fields

        /// <summary>
        /// A list of columns in the order table we retrieve from queries in this class
        /// All are aliased with the prefix "o_"
        /// </summary>
        internal const string OrderTableColumns =
            "o.order_id                    as o_order_id, " +
            "o.vendor_name                 as o_vendor_name, " +
            "o.vendor_order_num            as o_vendor_order_num, " +
            "o.certificate_id              as o_certificate_id, " +
            "o.certificate_type            as o_certificate_type, " +
            "o.order_type                  as o_order_type, " +
            "o.order_source                as o_order_source, " +
            "o.company_name                as o_company_name, " +
            "o.last_name                   as o_last_name, " +
            "o.first_name                  as o_first_name, " +
            "o.street_address              as o_street_address, " +
            "o.room_floor                  as o_room_floor, " +
            "o.department                  as o_department, " +
            "o.city                        as o_city, " +
            "o.state                       as o_state, " +
            "o.postal_code                 as o_postal_code, " +
            "o.country_code                as o_country_code, " +
            "o.email                       as o_email, " +
            "o.phone                       as o_phone, " +
            "o.approval_code               as o_approval_code, " +
            "o.create_date                 as o_create_date, " +
            "o.current_status              as o_current_status, " +
            "o.email_date                  as o_email_date, " +
            "o.print_status                as o_print_status, " +
            "o.prev_order_id               as o_prev_order_id, " +
            "o.account_id                  as o_account_id, " +
            "o.notes                       as o_notes, " +
            "o.erp_status                  as o_erp_status, " +
            "o.po_number                   as o_po_number, " +
            "o.job_title                   as o_job_title, " +
            "o.street_address_2            as o_street_address_2, " +
            "o.followup_required           as o_followup_required, " +
            "o.last_modified               as o_last_modified, " +
            "o.oracle_customer_number      as o_oracle_customer_number, " +
            "o.sales_channel_code          as o_sales_channel_code, " +
             "o.parent_order_number         as o_parent_order_number, " +
            "o.temporary_license_reason    as o_temporary_license_reason, " +
             "o.end_customer_company         as o_end_customer_company, " +
             "o.end_customer_first_name         as o_end_customer_first_name, " +
             "o.end_customer_last_name         as o_end_customer_last_name, " +
             "o.end_customer_address1         as o_end_customer_address1, " +
             "o.end_customer_address2         as o_end_customer_address2, " +
             "o.end_customer_city         as o_end_customer_city, " +
             "o.end_customer_state         as o_end_customer_state, " +
             "o.end_customer_country         as o_end_customer_country, " +
             "o.end_customer_zipcode         as o_end_customer_zipcode, " +
             "o.entitlement_code         as o_entitlement_code, " +
            "o.parent_account_id         as o_parent_account_id, " +
            "o.creator_email        as  o_creator_email ";

        #endregion

        #region Methods

        /// <summary>
        /// Find one Oracle order from the order id
        /// </summary>
        /// <param name="orderNum">Id of the order</param>
        /// <returns>Requested order object</returns>
        /// <exception cref="DataAccessException">Thrown if request order is not found.</exception>
        public static Order FindOrderByOrderId(string orderNum)
        {
            const string Sql =
                "select " + OrderDb.OrderTableColumns +
                "from  license.orders o " +
                "where o.vendor_order_num = :ORDER_NUM_INPUT and o.current_status not in ('CANCELLED','SNAPSHOT') ";

            OracleParameter[] parameters =
                {
                    new OracleParameter("ORDER_NUM_INPUT", orderNum)
                };

            OrderCollection orderCollection = PopulateOrders(Sql, parameters);

            if (orderCollection.Count == 0)
            {
                // order not found
                return null;
            }

            return orderCollection[0];
        }

        /// <summary>
        /// Find an order given an activation code
        /// </summary>
        /// <param name="activationCode">Id of the order line</param>
        /// <returns>Order line record</returns>
        public static Order FindOrderByActivationCode(string activationCode)
        {
            const string Sql =
                "select " + OrderDb.OrderTableColumns +
                "from license.orders o join license.order_line ol on o.order_id=ol.order_id " +
                "where upper(ol.activation_code) = :ACTIVATION_CODE_INPUT and o.current_status not in ('CANCELLED', 'SNAPSHOT', 'RPL_NEW', 'RPL_DENIED') ";

            OracleParameter[] parameters =
            {
                new OracleParameter("ACTIVATION_CODE_INPUT", activationCode.ToUpperInvariant())
            };

            OrderCollection orderCollection = PopulateOrders(Sql, parameters);

            return orderCollection.Count == 0 ? null : orderCollection[0];
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Populates the collection of Orders using the sql specified.
        /// Sql must get all of the fields needed to populate an Order.
        /// </summary>
        /// <param name="sql">Sql used for the query.</param>
        /// <param name="parameters">Any parameters to use in the query.</param>
        /// <returns>Collection of orders from results of the query.</returns>
        private static OrderCollection PopulateOrders(string sql, params OracleParameter[] parameters)
        {
            OrderCollection retrievedOrders = new OrderCollection();

            using (OracleConnection connection = new OracleConnection(ConfigSettings.Connection))
            {
                using (OracleDataReader dataReader = DatabaseAccessUtility.CreateDataReader(connection, sql, parameters))
                {
                    while (dataReader.Read())
                    {
                        Order order = PopulateOrder(dataReader);
                        retrievedOrders.Add(order);
                    }
                }

                return retrievedOrders;
            }
        }

        /// <summary>
        /// Method that uses data in a data reader to create a fully populated order object
        /// </summary>
        /// <param name="dataReader">DataReader loaded with all columns in Order table</param>
        /// <returns>A populated order object</returns>
        internal static Order PopulateOrder(OracleDataReader dataReader)
        {
            DateTime insertedAt = DateTime.MinValue;
            string createDateField = dataReader["O_CREATE_DATE"].ToString();
            if (!string.IsNullOrEmpty(createDateField))
            {
                insertedAt = Convert.ToDateTime(createDateField, CultureInfo.InvariantCulture);
            }

            DateTime lastModifiedDate = DateTime.MinValue;
            string lastModifiedDateField = dataReader["O_LAST_MODIFIED"].ToString();
            if (!string.IsNullOrEmpty(lastModifiedDateField))
            {
                lastModifiedDate = Convert.ToDateTime(lastModifiedDateField, CultureInfo.InvariantCulture);
            }

            string certificateId = dataReader["O_CERTIFICATE_ID"].ToString();
            string certificateType = dataReader["O_CERTIFICATE_TYPE"].ToString();

            string errormessage = string.Empty;
            if (dataReader.HasColumn("O_ERROR_MESSAGE"))
            {
                errormessage = Convert.ToString(dataReader["O_ERROR_MESSAGE"]);
            }

            Order order = new Order(
            dataReader["O_ORDER_ID"].ToString(),
            certificateId,
            dataReader["O_VENDOR_NAME"].ToString(),
            dataReader["O_VENDOR_ORDER_NUM"].ToString(),
            dataReader["O_CURRENT_STATUS"].ToString(),
            lastModifiedDate /* lastModified (DateTime.MinValue represents an uninitialized LAST_MODIFIED field in the database) */,
            insertedAt /* insertedAt (DateTime.MinValue represents an uninitialized CREATE_DATE field in the database) */,
            dataReader["O_COMPANY_NAME"].ToString(),
            dataReader["O_LAST_NAME"].ToString(),
            dataReader["O_FIRST_NAME"].ToString(),
            dataReader["O_JOB_TITLE"].ToString(),
            dataReader["O_PARENT_ORDER_NUMBER"].ToString(),
            dataReader["O_ROOM_FLOOR"].ToString(),
            dataReader["O_DEPARTMENT"].ToString(),
            dataReader["O_STREET_ADDRESS"].ToString(),
            dataReader["O_STREET_ADDRESS_2"].ToString(),
            dataReader["O_CITY"].ToString(),
            dataReader["O_STATE"].ToString(),
            dataReader["O_POSTAL_CODE"].ToString(),
            dataReader["O_COUNTRY_CODE"].ToString(),
            dataReader["O_PHONE"].ToString(),
            dataReader["O_EMAIL"].ToString(),
            dataReader["O_PO_NUMBER"].ToString(),
            certificateType,
            dataReader["O_ORDER_TYPE"].ToString(),
            dataReader["O_ORDER_SOURCE"].ToString(),
            dataReader.HasColumn("O_END_CUSTOMER_COMPANY") ? Convert.ToString(dataReader["O_END_CUSTOMER_COMPANY"]) : string.Empty,
            dataReader.HasColumn("O_END_CUSTOMER_FIRST_NAME") ? Convert.ToString(dataReader["O_END_CUSTOMER_FIRST_NAME"]) : string.Empty,
            dataReader.HasColumn("O_END_CUSTOMER_LAST_NAME") ? Convert.ToString(dataReader["O_END_CUSTOMER_LAST_NAME"]) : string.Empty,
            dataReader.HasColumn("O_END_CUSTOMER_ADDRESS1") ? Convert.ToString(dataReader["O_END_CUSTOMER_ADDRESS1"]) : string.Empty,
            dataReader.HasColumn("O_END_CUSTOMER_ADDRESS2") ? Convert.ToString(dataReader["O_END_CUSTOMER_ADDRESS2"]) : string.Empty,
            dataReader.HasColumn("O_END_CUSTOMER_CITY") ? Convert.ToString(dataReader["O_END_CUSTOMER_CITY"]) : string.Empty,
            dataReader.HasColumn("O_END_CUSTOMER_STATE") ? Convert.ToString(dataReader["O_END_CUSTOMER_STATE"]) : string.Empty,
            dataReader.HasColumn("O_END_CUSTOMER_COUNTRY") ? Convert.ToString(dataReader["O_END_CUSTOMER_COUNTRY"]) : string.Empty,
            dataReader.HasColumn("O_END_CUSTOMER_ZIPCODE") ? Convert.ToString(dataReader["O_END_CUSTOMER_ZIPCODE"]) : string.Empty,
            dataReader.HasColumn("O_ENTITLEMENT_CODE") ? Convert.ToString(dataReader["O_ENTITLEMENT_CODE"]) : string.Empty,           
            dataReader.HasColumn("O_PARENT_ACCOUNT_ID") ? Convert.ToString(dataReader["O_PARENT_ACCOUNT_ID"]) : string.Empty,
            dataReader.HasColumn("O_CREATOR_EMAIL") ? Convert.ToString(dataReader["O_CREATOR_EMAIL"]) : string.Empty);
            return order;
        }
        #endregion
    }
}
