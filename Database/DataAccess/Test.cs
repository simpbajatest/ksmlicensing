﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseLayer
{
    public static class Test
    {
        public static string checkConnection(string connectionString)
        {
            try
            {
                string s = SqlGet("select vendor_order_num from orders where rownum=1", connectionString);
                return "Result " + s;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public static string SqlGet(string sqlCommand, string conn) // returns a string (scalar)
        {
            if (string.IsNullOrEmpty(sqlCommand))
            {
                throw new ArgumentException("sql command cannot be null nor empty");
            }

            try
            {
                using (OracleConnection cn = new OracleConnection(conn))
                {
                    cn.Open();
                    using (OracleCommand mycommand = new OracleCommand(sqlCommand, cn))
                    {
                        object result = mycommand.ExecuteScalar();
                        mycommand.Parameters.Clear();
                        return result == null ? string.Empty : result.ToString();
                    }
                }
            }
            catch (Exception e)
            {
                return e.Message;              
            }
        }
    }
}
