﻿using Lambda.Entities;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Globalization;

namespace DatabaseLayer
{
    public class OrderLineDb
    {

        #region Fields

        /// <summary>
        /// A list of columns in the order line table we retrieve from queries in this class
        /// All are aliased with the prefix "ol_"
        /// </summary>
        internal const string OrderLineTableColumns =
               "ol.order_id                    as ol_order_id, " +
               "ol.prod_num                    as ol_prod_num, " +
               "ol.qty_ordered                 as ol_qty_ordered, " +
               "ol.qty_redeemed                as ol_qty_redeemed, " +
               "ol.description                 as ol_description, " +
               "ol.end_date                    as ol_end_date, " +
               "ol.line_id                     as ol_line_id, " +
               "ol.notes                       as ol_notes, " +
               "ol.start_date                  as ol_start_date, " +
               "ol.status                      as ol_status, " +
               "ol.qty_redeemable              as ol_qty_redeemable, " +
               "ol.erp_scheduled_ship_date     as ol_erp_scheduled_ship_date, " +
               "ol.erp_line_number             as ol_erp_line_number, " +
               "ol.erp_status                  as ol_erp_status, " +
               "ol.asm_notify_customer_date    as ol_asm_notify_customer_date, " +
               "ol.extended_base_line_id       as ol_extended_base_line_id, " +
               "ol.ext_as_base                 as ol_ext_as_base, " +
               "ol.print_status                as ol_print_status, " +
               "ol.external_line_id            as ol_external_line_id, " +
               "ol.delivery_method             as ol_delivery_method, " +
               "ol.customer_email              as ol_customer_email, " +
               "ol.secondary_email             as ol_secondary_email, " +
               "ol.csr_email                   as ol_csr_email, " +
               "ol.inserted_at                 as ol_inserted_at, " +
               "ol.emailed_at                  as ol_emailed_at, " +
               "ol.customer_request_date       as ol_customer_request_date, " +
               "ol.product_id                  as ol_product_id, " +
               "ol.split_qty_flag              as ol_split_qty_flag, " +
               "ol.master_erp_line_num         as ol_master_erp_line_num, " +
               "ol.activation_code              as ol_activation_code, " +
                "ol.link_id                      as ol_link_id, " +
                "ol.item_type                  as ol_item_type ";

        /// <summary>
        /// A list of columns in the order line table we retrieve from queries in this class
        /// All are aliased with the prefix "ol_"
        /// </summary>
        internal const string FnoodErroredOrderLineTableColumns =
               "ol.order_id                    as ol_order_id, " +
               "ol.prod_num                    as ol_prod_num, " +
               "ol.qty_ordered                 as ol_qty_ordered, " +
               "ol.qty_redeemed                as ol_qty_redeemed, " +
               "ol.description                 as ol_description, " +
               "ol.end_date                    as ol_end_date, " +
               "ol.line_id                     as ol_line_id, " +
               "ol.notes                       as ol_notes, " +
               "ol.start_date                  as ol_start_date, " +
               "ol.status                      as ol_status, " +
               "ol.qty_redeemable              as ol_qty_redeemable, " +
               "ol.erp_scheduled_ship_date     as ol_erp_scheduled_ship_date, " +
               "ol.erp_line_number             as ol_erp_line_number, " +
               "ol.erp_status                  as ol_erp_status, " +
               "ol.asm_notify_customer_date    as ol_asm_notify_customer_date, " +
               "ol.extended_base_line_id       as ol_extended_base_line_id, " +
               "ol.ext_as_base                 as ol_ext_as_base, " +
               "ol.print_status                as ol_print_status, " +
               "ol.external_line_id            as ol_external_line_id, " +
               "ol.delivery_method             as ol_delivery_method, " +
               "ol.customer_email              as ol_customer_email, " +
               "ol.secondary_email             as ol_secondary_email, " +
               "ol.csr_email                   as ol_csr_email, " +
               "ol.inserted_at                 as ol_inserted_at, " +
               "ol.emailed_at                  as ol_emailed_at, " +
               "ol.customer_request_date       as ol_customer_request_date, " +
               "ol.product_id                  as ol_product_id, " +
               "ol.split_qty_flag              as ol_split_qty_flag, " +
               "ol.master_erp_line_num         as ol_master_erp_line_num, " +
               "ol.activation_code             as ol_activation_code, " +
               "ol.item_type                   as ol_item_type, " +
               "nvl(ol.error_message, 'Not yet entitled')  as ol_error_message ";

        #endregion

        #region Methods

        /// <summary>
        /// Find an order line given an order line Id
        /// </summary>
        /// <param name="orderLineId">Id of the order line</param>
        /// <returns>Order line record</returns>
        public static OrderLine FindOrderLine(long orderLineId)
        {
            const string Sql =
                "select " + OrderLineDb.OrderLineTableColumns +
                "from license.order_line ol " +
                "where ol.line_id = :ORDER_LINE_ID_INPUT ";

            OracleParameter[] parameters =
            {
                new OracleParameter("ORDER_LINE_ID_INPUT", orderLineId.ToString())
            };

            OrderLineCollection orderLineCollection = PopulateOrderLines(Sql, parameters);

            return orderLineCollection.Count == 0 ? null : orderLineCollection[0];
        }

        public static long UpdateOrderLineQty(string qtyAdjusted, long orderlineId)
        {
            try
            {
                string Sql =
                    "update  license.order_line ol set" +                                     
                    "    ol.qty_redeemed = " + qtyAdjusted + " where ol.line_id = :LINE_ID_INPUT ";

                OracleParameter[] parameters =
                    {                      
                        new OracleParameter("LINE_ID_INPUT", orderlineId.ToString())
                   };

                DatabaseAccessUtility.SqlDo(Sql, parameters);

                string Sql1 =
                   "update  license.order_line ol set" +
                   "    ol.qty_redeemed = 0 where ol.line_id = :LINE_ID_INPUT and ol.qty_redeemed< 0 ";

                OracleParameter[] parameters1 =
                    {
                        new OracleParameter("LINE_ID_INPUT", orderlineId.ToString())
                   };

                DatabaseAccessUtility.SqlDo(Sql1, parameters1);
            }
            catch (OracleException e)
            {
                throw new DataException("Orders update failed", e);
            }

            return orderlineId;
        }

        /// <summary>
        /// Find an order line given an order line Id
        /// </summary>
        /// <param name="activationCode">Id of the order line</param>
        /// <returns>Order line record</returns>
        public static ReadEntitlement FindOrderInfoByActivationCode(string activationCode)
        {
            const string Sql =
                "select nvl(o.entitlement_code, '') as entitlement_code,  ol.prod_num, ol.activation_code, " +
                " nvl(o.parent_account_id, '') as oracle_cust_id,  nvl(o.company_name,'') as company_name, nvl(ol.customer_email,'') as customer_email, " +
                " nvl(ol.end_date,'') as end_date, ol.qty_redeemable, ol.qty_redeemed, ol.line_id, ol.order_id " +
                " from license.order_line ol join license.orders o on o.order_id=ol.order_id " +
                " where o.current_status not in ('SNAPSHOT','CANCELLED') " +
                " and ol.activation_code = :ACTIVATION_CODE_INPUT ";

            OracleParameter[] parameters =
            {
                new OracleParameter("ACTIVATION_CODE_INPUT", activationCode.ToUpperInvariant())
            };

            ReadEntitlementCollection orderLineCollection = PopulateReadEntitlements(Sql, parameters);

            return orderLineCollection.Count == 0 ? null : orderLineCollection[0];
        }

        /// <summary>
        /// Find an order line given an order line Id
        /// </summary>
        /// <param name="activationCode">Id of the order line</param>
        /// <returns>Order line record</returns>
        public static OrderLine FindOrderLineByActivationCode(string activationCode)
        {
            const string Sql =
                "select " + OrderLineDb.OrderLineTableColumns +
                " from license.order_line ol join PRODUCT_ENTITLEMENTS pe on ol.prod_num = pe.prod_num " +
                " where upper(ol.activation_code) = :ACTIVATION_CODE_INPUT and nvl(ol.print_status,'X') not in ('SNAPSHOT') and nvl(ol.erp_status,'X') not in ('SNAPSHOT') order by pe.entitlement_type ";

            OracleParameter[] parameters =
            {
                new OracleParameter("ACTIVATION_CODE_INPUT", activationCode.ToUpperInvariant())
            };

            OrderLineCollection orderLineCollection = PopulateOrderLines(Sql, parameters);

            return orderLineCollection.Count == 0 ? null : orderLineCollection[0];
        }

        /// <summary>
        /// Find one Oracle order from the order id
        /// </summary>
        /// <param name="orderNum">Id of the order</param>
        /// <returns>Requested order object</returns>
        /// <exception cref="DataAccessException">Thrown if request order is not found.</exception>
        public static ReadEntitlement FindOrderLineByOrderNumber(string orderNum)
        {
            const string Sql =
                "select nvl(o.entitlement_code, '') as entitlement_code,  ol.prod_num, ol.activation_code, " +
                " nvl(o.parent_account_id, '') as oracle_cust_id,  nvl(o.company_name,'') as company_name, nvl(ol.customer_email,'') as customer_email, " +
                " nvl(ol.end_date,'') as end_date, ol.qty_redeemable, ol.qty_redeemed, ol.line_id, ol.order_id " +
                " from license.order_line ol join license.orders o on o.order_id=ol.order_id " +               
                " where o.vendor_order_num = :ORDER_NUM_INPUT and o.current_status not in ('SNAPSHOT','CANCELLED')";

            OracleParameter[] parameters =
                {
                    new OracleParameter("ORDER_NUM_INPUT", orderNum)
                };

            ReadEntitlementCollection orderLineCollection = PopulateReadEntitlements(Sql, parameters);

            if (orderLineCollection.Count == 0)
            {
                // order not found
                return null;
            }

            return orderLineCollection[0];
        }

        /// <summary>
        /// Find one Oracle order from the order id
        /// </summary>
        /// <param name="orderId">Id of the order</param>
        /// <returns>Requested order object</returns>
        /// <exception cref="DataAccessException">Thrown if request order is not found.</exception>
        public static OrderLineCollection FindOrderLinesByEntitlementCode(string entitlementCode)
        {
            const string Sql =
                "select " + OrderLineDb.OrderLineTableColumns +
                " from license.order_line ol join license.orders o on ol.order_id=o.order_id JOIN ENTITIES e on e.E_NAME=o.VENDOR_NAME " +
                " where upper(o.entitlement_code) = :ENTITLEMENT_CODE_INPUT  and e.USE_ACTIVATION_CODE='Y' and o.current_status not in ('SNAPSHOT', 'CANCELLED', 'RPL_NEW', 'RPL_DENIED') and ol.ACTIVATION_CODE is not null ";

            OracleParameter[] parameters =
                {
                    new OracleParameter("ENTITLEMENT_CODE_INPUT", entitlementCode.ToUpperInvariant())
                };

            OrderLineCollection orderLineCollection = PopulateOrderLines(Sql, parameters);

            if (orderLineCollection.Count == 0)
            {
                // order not found
                return null;
            }

            return orderLineCollection;
        }

        /// <summary>
        /// Method that uses sql to fill a data reader with order line records and fully populates a collection of order lines
        /// </summary>
        /// <param name="sql">SQL to retrieve a set of order line records</param>
        /// <param name="parameters">Optional OracleParameters needed by the query.</param>
        /// <returns>Collection of order line</returns>
        private static OrderLineCollection PopulateOrderLines(string sql, params OracleParameter[] parameters)
        {
            OrderLineCollection retrievedOrderLines = new OrderLineCollection();

            using (OracleConnection connection = new OracleConnection(ConfigSettings.Connection))
            {
                using (OracleDataReader dataReader = DatabaseAccessUtility.CreateDataReader(connection, sql, parameters))
                {
                    while (dataReader.Read())
                    {
                        OrderLine orderLine = PopulateOrderLine(dataReader);

                        retrievedOrderLines.Add(orderLine);
                    }
                }

                return retrievedOrderLines;
            }
        }

        /// <summary>
        /// Method that uses sql to fill a data reader with order line records and fully populates a collection of order lines
        /// </summary>
        /// <param name="sql">SQL to retrieve a set of order line records</param>
        /// <param name="parameters">Optional OracleParameters needed by the query.</param>
        /// <returns>Collection of order line</returns>
        private static ReadEntitlementCollection PopulateReadEntitlements(string sql, params OracleParameter[] parameters)
        {
            ReadEntitlementCollection retrievedOrderLines = new ReadEntitlementCollection();

            using (OracleConnection connection = new OracleConnection(ConfigSettings.Connection))
            {
                using (OracleDataReader dataReader = DatabaseAccessUtility.CreateDataReader(connection, sql, parameters))
                {
                    while (dataReader.Read())
                    {
                        ReadEntitlement orderLine = PopulateReadEntitlement(dataReader);

                        retrievedOrderLines.Add(orderLine);
                    }
                }

                return retrievedOrderLines;
            }
        }

        /// <summary>
        /// Method that uses data in a data reader to create a fully populated order line object
        /// </summary>
        /// <param name="dataReader">DataReader loaded with all columns in OrderLine table</param>        
        /// <returns>
        /// A populated order line object
        /// </returns>
        internal static ReadEntitlement PopulateReadEntitlement(OracleDataReader dataReader)
        {
            string entitlementCode = dataReader["ENTITLEMENT_CODE"].ToString();
            string prodNum = dataReader["PROD_NUM"].ToString();
            string companyName = dataReader["COMPANY_NAME"].ToString();
            string oracleCustId = dataReader["ORACLE_CUST_ID"].ToString();
            string activationCode = dataReader["ACTIVATION_CODE"].ToString();
            string customerEmail = dataReader["CUSTOMER_EMAIL"].ToString();
            string orderId = dataReader["ORDER_ID"].ToString();

            long qtyRedeemable = 0;
            string value = dataReader["QTY_REDEEMABLE"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                qtyRedeemable = DataConversionUtility.StringToInt64(value);
            }

            long qtyRedeemed = 0;
            value = dataReader["QTY_REDEEMED"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                qtyRedeemed = DataConversionUtility.StringToInt64(value);
            }

            string endDate = string.Empty;
            value = dataReader["END_DATE"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                endDate = Convert.ToDateTime(value).ToString("yyyy-MM-dd");
            }

            long lineId = 0;
            value = dataReader["LINE_ID"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                lineId = DataConversionUtility.StringToInt64(value);
            }

            return new ReadEntitlement(entitlementCode,prodNum, Convert.ToString(endDate),activationCode, qtyRedeemable, 
                qtyRedeemed, lineId, orderId, companyName, customerEmail, oracleCustId);
        }

        /// <summary>
        /// Method that uses data in a data reader to create a fully populated order line object
        /// </summary>
        /// <param name="dataReader">DataReader loaded with all columns in OrderLine table</param>
        /// <param name="isStrict">if set to <c>true</c> the order line is required to have a product id.</param>
        /// <returns>
        /// A populated order line object
        /// </returns>
        internal static OrderLine PopulateOrderLine(OracleDataReader dataReader)
        {
            string orderId = dataReader["OL_ORDER_ID"].ToString();

            string prodNum = dataReader["OL_PROD_NUM"].ToString();

            long qtyOrdered = 0;
            string value = dataReader["OL_QTY_ORDERED"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                qtyOrdered = DataConversionUtility.StringToInt64(value);
            }

            long qtyRedeemed = 0;
            value = dataReader["OL_QTY_REDEEMED"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                qtyRedeemed = DataConversionUtility.StringToInt64(value);
            }

            long orderLineId = Convert.ToInt64(dataReader["OL_LINE_ID"].ToString());

            DateTime startDate = new DateTime();
            value = dataReader["OL_START_DATE"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                startDate = Convert.ToDateTime(value, CultureInfo.InvariantCulture);
            }

            DateTime endDate = new DateTime();
            value = dataReader["OL_END_DATE"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                endDate = Convert.ToDateTime(value, CultureInfo.InvariantCulture);
            }

            DateTime? customerRequestDate = null;
            value = dataReader["OL_CUSTOMER_REQUEST_DATE"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                customerRequestDate = Convert.ToDateTime(value, CultureInfo.InvariantCulture);
            }

            string status = dataReader["OL_STATUS"].ToString().ToUpperInvariant();

            string notes = dataReader["OL_NOTES"].ToString();

            // If we don't have an order_line description, and we had joined to the product table, then
            // return the product table's description instead.
            string description = dataReader["OL_DESCRIPTION"].ToString();
            if (string.IsNullOrEmpty(description) && dataReader.HasColumn("P_DESCRIPTION"))
            {
                description = dataReader["P_DESCRIPTION"].ToString();
            }

            long qtyRedeemable = 0;
            value = dataReader["OL_QTY_REDEEMABLE"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                qtyRedeemable = DataConversionUtility.StringToInt64(value);
            }

            DateTime erpScheduledShipDate = new DateTime();
            value = dataReader["OL_ERP_SCHEDULED_SHIP_DATE"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                erpScheduledShipDate = Convert.ToDateTime(value, CultureInfo.InvariantCulture);
            }

            string erpLineNumber = dataReader["OL_ERP_LINE_NUMBER"].ToString();
            string erpStatus = dataReader["OL_ERP_STATUS"].ToString().ToUpperInvariant();
            string printStatus = dataReader["OL_PRINT_STATUS"].ToString().ToUpperInvariant();

            long extendedBaseLineId = 0;
            value = dataReader["OL_EXTENDED_BASE_LINE_ID"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                extendedBaseLineId = Convert.ToInt64(value);
            }

            DateTime asmNotifyCustomerDate = new DateTime();
            value = dataReader["OL_ASM_NOTIFY_CUSTOMER_DATE"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                asmNotifyCustomerDate = Convert.ToDateTime(value, CultureInfo.InvariantCulture);
            }

            bool extAsBase = false;
            value = dataReader["OL_EXT_AS_BASE"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                extAsBase = value.ToUpperInvariant() == "Y";
            }

            long externalLineId = 0;
            value = dataReader["OL_EXTERNAL_LINE_ID"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                externalLineId = Convert.ToInt64(value);
            }

            string deliveryMethod = dataReader["OL_DELIVERY_METHOD"].ToString();

            string customerEmail = dataReader["OL_CUSTOMER_EMAIL"].ToString();
            string secondaryEmail = dataReader["OL_SECONDARY_EMAIL"].ToString();
            string csrEmail = dataReader["OL_CSR_EMAIL"].ToString();

            DateTime insertedAt = new DateTime();
            value = dataReader["OL_INSERTED_AT"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                insertedAt = Convert.ToDateTime(value, CultureInfo.InvariantCulture);
            }

            DateTime emailedAtDate = new DateTime();
            value = dataReader["OL_EMAILED_AT"].ToString();
            if (!string.IsNullOrEmpty(value))
            {
                emailedAtDate = Convert.ToDateTime(value, CultureInfo.InvariantCulture);
            }

            value = dataReader["OL_PRODUCT_ID"].ToString();         

            long productId = 0;
            if (!string.IsNullOrEmpty(value))
            {
                productId = Convert.ToInt64(value);
            }

            value = dataReader["OL_MASTER_ERP_LINE_NUM"].ToString();
            string masterErpLineNumber = string.IsNullOrEmpty(value) ? string.Empty : value;

            value = dataReader["OL_SPLIT_QTY_FLAG"].ToString();
            string splitQtyFlag = string.IsNullOrEmpty(value) ? "No" : value;
            //// value = dataReader["AL_AGREEMENT_ID"].ToString();
            string agreementnumber = string.Empty;
            ////  value = dataReader["AL_ASSET_NUMBER"].ToString();
            string assetnumber = string.Empty;
            string activationcode = dataReader.HasColumn("OL_ACTIVATION_CODE") ? Convert.ToString(dataReader["OL_ACTIVATION_CODE"]) : string.Empty;
            string itemtype = dataReader.HasColumn("ol_item_type") ? Convert.ToString(dataReader["ol_item_type"]) : string.Empty;
            string errorMessage = dataReader.HasColumn("ol_error_message") ? Convert.ToString(dataReader["ol_error_message"]) : string.Empty;
            string linkId = dataReader.HasColumn("ol_link_id") ? Convert.ToString(dataReader["ol_link_id"]) : string.Empty;
            return new OrderLine(
                masterErpLineNumber,
                splitQtyFlag,
                asmNotifyCustomerDate,
                csrEmail,
                customerEmail,
                deliveryMethod,
                description,
                emailedAtDate,
                endDate,
                erpLineNumber,
                erpScheduledShipDate,
                erpStatus,
                extAsBase,
                extendedBaseLineId,
                externalLineId,
                insertedAt,
                notes,
                orderId,
                orderLineId,
                printStatus,
                productId,
                prodNum,
                qtyOrdered,
                qtyRedeemable,
                qtyRedeemed,
                secondaryEmail,
                startDate,
                customerRequestDate,
                status,
                agreementnumber,
                assetnumber,
                activationcode,
                errorMessage,
                itemtype,
                linkId);
        }
        #endregion
    }
}
