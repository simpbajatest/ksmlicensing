﻿using System;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Net;

namespace DatabaseLayer
{

    /// <summary>
    /// Gets values from appsettings.config and web.config (including database connection strings)
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Using Win32 naming for consistency.")]
    public static class ConfigSettings
    {
        #region Fields

        /// <summary>
        /// Dummy password which need to replace by database Password fetched from CyberArk
        /// </summary>
        private const string DummyPassword = "XXXXXX";

        /// <summary>
        /// License user which need to replace by database Password fetched from CyberArk
        /// </summary>
        private const string LicenseUser = "license";     

        /// <summary>
        /// A config setting.
        /// </summary>
        [ThreadStatic]
        private static TimeSpan databaseTimeout;

        /// <summary>
        /// A config setting.
        /// </summary>
        [ThreadStatic]
        private static bool databaseTimeoutValid;


        #endregion

        #region Properties       

        /// <summary>
        /// Gets or sets the database connection information
        /// </summary>
        public static string Connection{ get; set; }       

        /// <summary>
        /// Gets a value indicating whether to show a warning dialog when IE10 is detected.
        /// This exists as a workaround to buttons that don't function when IE 10 is not in compatibility view mode.
        /// (See AppSettings.PROD.config for more details.)
        /// </summary>
        public static bool ShowWarningDialogWhenIE10Detected
        {
            get
            {
                string value = GetAppSetting("ShowWarningDialogWhenIE10Detected");

                if (string.IsNullOrEmpty(value) ||
                    value.ToUpperInvariant() == "FALSE")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        #endregion      

        #region Methods

        #region Public Methods

        /// <summary>
        /// Be sure that the configuration file is properly protected.
        /// </summary>
        /// <param name="programName">Name of the program.</param>
        public static void ConfigEncryption(string programName)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(programName);
            ConnectionStringsSection section = config.GetSection("connectionStrings") as ConnectionStringsSection;
            if (section == null)
            {
                throw new InvalidOperationException("connectionStrings not found.");
            }

            if (!section.SectionInformation.IsProtected)
            {
                section.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
                config.Save();
            }
        }

        /// <summary>
        /// Gets the common database value
        /// </summary>
        [SuppressMessage(
            "Microsoft.Design", "CA1031",
            Justification = "Critical point failure catch")]
        public static TimeSpan TransactionLimit
        {
            get
            {
                if (databaseTimeoutValid)
                {
                    return databaseTimeout;
                }

                try
                {
                    string limitAsString = GetAppSetting("TransactionLimit");
                    int limitAsInt32 = int.Parse(limitAsString, System.Globalization.CultureInfo.InvariantCulture);
                    databaseTimeout = TimeSpan.FromSeconds(limitAsInt32);
                }
                catch (Exception e)
                {
                   // AslEventLog.LogWarning("Using default TransactionLimit.  Exception: " + e);
                    databaseTimeout = TimeSpan.FromSeconds(300); // too critical to not recover!
                }

                databaseTimeoutValid = true;
                return databaseTimeout;
            }
        }

        #endregion

        #region Private Methods       

        /// <summary>
        /// Gets the connection string information from web.config
        /// </summary>
        /// <param name="name">The name of the connection string.</param>
        /// <returns>The current applications connection string for theis key.</returns>
        private static string GetConnectionString(string name)
        {
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[name];
            if (settings == null ||
                string.IsNullOrEmpty(settings.ConnectionString))
            {
                throw new InvalidOperationException(name + " does not exist in the web.config's <connectionStrings>");
            }

            return settings.ConnectionString;
        }

        /// <summary>
        /// Gets the app settings from any app.config file
        /// </summary>
        /// <param name="name">The name of the key.</param>
        /// <returns>The current applications value for this key. </returns>
        private static string GetAppSetting(string name)
        {
            string value = ConfigurationManager.AppSettings[name];
            if (string.IsNullOrEmpty(value))
            {
                throw new InvalidOperationException(name + " is not set to a value in the app.config");
            }

            return value;
        }

        #endregion

        #endregion
    }
}
