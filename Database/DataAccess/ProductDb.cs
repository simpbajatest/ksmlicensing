﻿using Lambda.Entities;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace DatabaseLayer
{
    public class ProductDb
    {
        #region Properties

        /// <summary>
        /// Gets the product table columns.
        /// </summary>
        public static string ProductTableColumns
        {
            get { return RetrieveProductTableColumns("p"); }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Find all Products that match the given ProductID.  Of course,
        /// since product_id is the unique identifier for products, this
        /// is going to always return a collection containing the one
        /// matching product.
        /// </summary>
        /// <param name="productId">ProdID product id of the Product you want to find</param>
        /// <returns>A ProductCollection containing fully loaded Product objects that match given productId</returns>
        public static Product FindProductByProdId(long productId)
        {
            string sql =
                                       "SELECT " + ProductTableColumns +
                                       " FROM license.product p " +
                " WHERE p.product_id = :PRODUCT_ID_INPUT ";

            OracleParameter[] parameters =
                {
                    new OracleParameter("PRODUCT_ID_INPUT", productId.ToString())
                };

            ProductCollection prodCol = PopulateProducts(sql, parameters);

            if (prodCol.Count > 0)
            {
                return PopulateProducts(sql, parameters)[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Find all Products that match the given ProductID.  Of course,
        /// since product_id is the unique identifier for products, this
        /// is going to always return a collection containing the one
        /// matching product.
        /// </summary>
        /// <param name="productId">ProdID product id of the Product you want to find</param>
        /// <returns>A ProductCollection containing fully loaded Product objects that match given productId</returns>
        public static ProductFeaturesCollection FindProductFeaturesByProdNumber(string prodNum)
        {
            string sql =
                                       "SELECT nvl(p.qty,0) as qty, nvl(p.feature,'') as feature " +
                                       " FROM license.TEMP_PROD_FEATURE p " +
                " WHERE p.prod_num = :PRODUCT_NUM_INPUT ";

            OracleParameter[] parameters =
                {
                    new OracleParameter("PRODUCT_NUM_INPUT", prodNum.ToString())
                };

            ProductFeaturesCollection prodCol = PopulateProductFeatures(sql, parameters);

            if (prodCol.Count > 0)
            {
                return prodCol;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Map from a product id to a product number
        /// </summary>
        /// <param name="prodId">The product id</param>
        /// <returns>The product number for this id</returns>
        public static string FindProductNumber(long prodId)
        {
            const string Sql =
                "select p.prod_num as product_num " +
                "from license.product p " +
                "where p.product_id = :PRODUCT_ID_INPUT ";

            OracleParameter[] parameters =
                {
                    new OracleParameter("PRODUCT_ID_INPUT", prodId.ToString())
                };

            return DatabaseAccessUtility.SqlGet(Sql, parameters);
        }

        /// <summary>
        /// Find Product by product id
        /// </summary>
        /// <param name="productId">Id of the product to be found</param>
        /// <returns>A product object, not valid if nothing found</returns>
        public static Product FindProduct(long productId)
        {
            string sql =
                "select " + ProductTableColumns +
                         "from license.product p " +
                         "where p.product_id = :PRODUCT_ID_INPUT ";

            OracleParameter[] parameters =
                {
                    new OracleParameter("PRODUCT_ID_INPUT", productId.ToString())
                };

            ProductCollection products = PopulateProducts(sql, parameters);

            if (products.Count == 0)
            {
                return new Product();
            }

            return products[0];
        }

        /// <summary>
        /// Find Product by product id
        /// </summary>
        /// <param name="productId">Id of the product to be found</param>
        /// <returns>A product object, not valid if nothing found</returns>
        public static Product FindProduct(string productNumber)
        {
            string sql =
                "select " + ProductTableColumns +
                         "from license.product p " +
                         "where p.prod_num = :PRODUCT_NUM_INPUT ";

            OracleParameter[] parameters =
                {
                    new OracleParameter("PRODUCT_NUM_INPUT", productNumber.ToString())
                };

            ProductCollection products = PopulateProducts(sql, parameters);

            if (products.Count == 0)
            {
                return new Product();
            }

            return products[0];
        }

        /// <summary>
        /// Populates a collection of Products using the SQL query passed in.
        /// SQL query must get all fields needed to populate a Product.
        /// </summary>
        /// <param name="sql">SQL used to query product data.</param>
        /// <param name="parameters">The parameters for this SQL</param>
        /// <returns>Collection of products from result of the SQL query.</returns>
        internal static ProductCollection PopulateProducts(string sql, params OracleParameter[] parameters)
        {
            ProductCollection products = new ProductCollection();

            using (OracleConnection connection = new OracleConnection(ConfigSettings.Connection))
            {
                using (OracleDataReader dataReader = DatabaseAccessUtility.CreateDataReader(connection, sql, parameters))
                {
                    while (dataReader.Read())
                    {
                        Product product = PopulateProduct(dataReader, "p");

                        products.Add(product);
                    }
                }
            }

            return products;
        }

        /// <summary>
        /// Populates a collection of Products using the SQL query passed in.
        /// SQL query must get all fields needed to populate a Product.
        /// </summary>
        /// <param name="sql">SQL used to query product data.</param>
        /// <param name="parameters">The parameters for this SQL</param>
        /// <returns>Collection of products from result of the SQL query.</returns>
        internal static ProductFeaturesCollection PopulateProductFeatures(string sql, params OracleParameter[] parameters)
        {
            ProductFeaturesCollection productFeatures = new ProductFeaturesCollection();

            using (OracleConnection connection = new OracleConnection(ConfigSettings.Connection))
            {
                using (OracleDataReader dataReader = DatabaseAccessUtility.CreateDataReader(connection, sql, parameters))
                {
                    while (dataReader.Read())
                    {
                        ProductFeatures productFeature = PopulateProductFeatures(dataReader, "p");

                        productFeatures.Add(productFeature);
                    }
                }
            }

            return productFeatures;
        }

        /// <summary>
        /// Populate a single product from a data reader.  The data reader
        /// must contain all of the fields necessary to populate the 
        /// product.
        /// </summary>
        /// <param name="dataReader">The data reader to populate product from.</param>
        /// <param name="aliasPrefix">The prefix for the table column names</param>
        /// <returns>A product with data from the data reader.</returns>
        internal static Product PopulateProduct(OracleDataReader dataReader, string aliasPrefix)
        {
            string fullAliasPrefix = aliasPrefix + "_";

            // convert data to correct types to keep this method independent
            // of column order
            int productId = Convert.ToInt32(dataReader[fullAliasPrefix + "PRODUCT_ID"].ToString());
            string entity = dataReader[fullAliasPrefix + "ENTITY_NAME"].ToString();
            string productNum = dataReader[fullAliasPrefix + "PRODUCT_NUM"].ToString().ToUpper();

            string description = dataReader[fullAliasPrefix + "DESCRIPTION"].ToString();
            //string status = dataReader[fullAliasPrefix + "STATUS"].ToString();
            string status = string.Empty;

            string revision = dataReader[fullAliasPrefix + "REVISION"].ToString();

            // product_version_number is no longer nullable, so this should always succeed:
            uint productVerNum = Convert.ToUInt32(dataReader[fullAliasPrefix + "PRODUCT_VERSION_NUMBER"].ToString().Trim());

            string productVerDesc = dataReader[fullAliasPrefix + "PRODUCT_VERSION_DESCRIPTION"].ToString();
            string redeemOnePerHost = dataReader[fullAliasPrefix + "REDEEM_ONE_PER_HOST_FLAG"].ToString();
            string orderable = dataReader[fullAliasPrefix + "ORDERABLE_FLAG"].ToString();
            string allowRehosting = dataReader[fullAliasPrefix + "ALLOW_REHOSTING_FLAG"].ToString();
            string hostIdHelpUrl = dataReader[fullAliasPrefix + "HOSTID_HELP_URL"].ToString();
            string renewableLicense = dataReader[fullAliasPrefix + "RENEWABLE_LICENSE_FLAG"].ToString();
            string rmu = dataReader[fullAliasPrefix + "RMU"].ToString();
            string prodLine = dataReader[fullAliasPrefix + "PROD_LINE"].ToString();
            //// string quantity = dataReader["ol_" + "QTY_ORDERED"].ToString();

            return new Product(
                               productId,
                               entity,
                               productNum,
                               description,
                               status,
                               revision,
                               productVerNum,
                               productVerDesc,
                               redeemOnePerHost,
                               orderable,
                               allowRehosting,
                               hostIdHelpUrl,
                               renewableLicense,
                               rmu,
                               prodLine);
            //// lineNumber,
            //// quantity);
        }

        /// <summary>
        /// Populate a single product from a data reader.  The data reader
        /// must contain all of the fields necessary to populate the 
        /// product.
        /// </summary>
        /// <param name="dataReader">The data reader to populate product from.</param>
        /// <param name="aliasPrefix">The prefix for the table column names</param>
        /// <returns>A product with data from the data reader.</returns>
        internal static ProductFeatures PopulateProductFeatures(OracleDataReader dataReader, string aliasPrefix)
        {
            long quantity = Convert.ToInt64(dataReader["Qty"].ToString());
            string features = dataReader["FEATURE"].ToString();


            return new ProductFeatures(
                              quantity,
                              features
                              );
        }

        /// <summary>
        /// Retrieves the product table columns.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>The product table columns.</returns>
        private static string RetrieveProductTableColumns(string prefix)
        {
            if (string.IsNullOrEmpty(prefix))
            {
                throw new ArgumentNullException("prefix");
            }

            string select =
                prefix + ".product_id as " + prefix + "_product_id, " +
                prefix + ".vendor_name as " + prefix + "_entity_name, " +
                prefix + ".prod_num as " + prefix + "_product_num, " +
                prefix + ".description as " + prefix + "_description, " +
                prefix + ".revision as " + prefix + "_revision, " +
                prefix + ".product_version_number as " + prefix + "_product_version_number, " +
                prefix + ".product_version_description as " + prefix + "_product_version_description, " +
                prefix + ".redeem_one_per_host_flag as " + prefix + "_redeem_one_per_host_flag, " +
                prefix + ".orderable_flag as " + prefix + "_orderable_flag, " +
                prefix + ".allow_rehosting_flag as " + prefix + "_allow_rehosting_flag, " +
                prefix + ".hostid_help_url as " + prefix + "_hostid_help_url, " +
                prefix + ".rmu as " + prefix + "_rmu, " +
                prefix + ".seed4 as " + prefix + "_prod_line, " +
                prefix + ".renewable_license_flag as " + prefix + "_renewable_license_flag ";

            return @select;
        }

        #endregion
    }
}
